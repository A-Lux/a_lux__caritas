(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
        /***/ "./$$_lazy_route_resource lazy recursive": 
        /*!******************************************************!*\
          !*** ./$$_lazy_route_resource lazy namespace object ***!
          \******************************************************/
        /*! no static exports found */
        /***/ (function (module, exports) {
            function webpackEmptyAsyncContext(req) {
                // Here Promise.resolve().then() is used instead of new Promise() to prevent
                // uncaught exception popping up in devtools
                return Promise.resolve().then(function () {
                    var e = new Error("Cannot find module '" + req + "'");
                    e.code = 'MODULE_NOT_FOUND';
                    throw e;
                });
            }
            webpackEmptyAsyncContext.keys = function () { return []; };
            webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
            module.exports = webpackEmptyAsyncContext;
            webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html": 
        /*!**************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"global__container\">\n  <div class=\"container-fluid\" id=\"scroll\">\n    <div class=\"row\">\n      <div class=\"global__container__menu\">\n        <app-navigation></app-navigation>\n      </div>\n      <div class=\"global__container__content\" [@routeAnimations]=\"prepareRoute(outlet)\">\n          <router-outlet #outlet=\"outlet\"></router-outlet>\n      </div>\n    </div>\n  </div>\n</div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about-inner/about-inner.component.html": 
        /*!****************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about-inner/about-inner.component.html ***!
          \****************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"about-container\">\n    <div class=\"container-fluid\">\n        <div class=\"row\">\n            <div class=\"about-container__title\">\n                <img src=\"\" alt=\"\">\n                <a routerLink=\"home/about/about-inner\">\n                <h3>О нас</h3>\n                </a>\n                <p>Our mission is based on nine core principles of Catholic Social Teaching</p>\n            </div>\n        </div>\n        <div class=\"row\">\n            <h2 class=\"title__header\">Наша миссия</h2>\n        </div>\n        <div class=\"row\">\n            <p class=\"title__text\">\n                Мы вдохновлены Евангелием учением католической церкви и надеждами людей, живущих в нищете. Мы призываем\n                каждого отвечать на гуманитарные потребности, продвигать целостному развитию человечества и выступаем за\n                причины возникновения бедноты и насилия.\n\n                Мы вдохновляем католические общины и всех людей с благими побуждениями к солидарности ко страданию их\n                братьев и сестер по всему миру.\n\n                Мы работаем над тем, чтобы убедить что наша естественная среда управлялась ответственно и бережно в\n                интересах всей человеческой семьи.\n            </p>\n        </div>\n        <div class=\"row about-container__gallery\">\n            <div class=\"col-sm-5\">\n                <img src=\"{{ aboutLeft }}\" alt=\"\">\n            </div>\n            <div class=\"col-sm-7\">\n                <img src=\"{{ aboutRight }}\" alt=\"\">\n            </div>\n        </div>\n        <div class=\"row\">\n            <p class=\"body__text\">\n                Благодаря успешному партнерству в гуманитарной помощи и для развития и преображения сердец и мыслей в\n                обществе, Каритас оказывает помощь в борьбе с бедностью, побуждает справедливость, и исповедует\n                благородство. Мы пытаемся отвечать потребностям тех, кто страдает старыми и новыми видами бедности. В то\n                же время мы мобилизуем наши человеческие и материальные ресурсы и ресурсы тех кто разделяет с нами наши\n                идеи, чтобы сделать наш мир «общим домом» лучшим местом.\n            </p>\n        </div>\n        <div class=\"row\">\n            <div class=\"facts\">\n                <p>At some point, western civilization made an emotional\n                    choice to treat dogs like our best friends</p>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"wrapper-mission\">\n                <img src=\"{{ ballons  }}\" alt=\"\">\n                <img src=\"{{ ballons }}\" alt=\"\">\n                <h2>Наша миссия</h2>\n                <p>Мы вдохновлены Евангелием учением католической церкви и надеждами людей, живущих в нищете. Мы\n                    призываем каждого отвечать на гуманитарные потребности, продвигать целостному развитию человечества\n                    и выступаем за причины возникновения бедноты и насилия.\n                    Мы вдохновляем католические общины и всех людей с благими\n                </p>\n                <a href=\"\"> написать нам </a>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <h2 class=\"body__title\">Наше видение</h2>\n                <p>\n                    Знак любви Бога к человечеству.\n\n                    Мы работаем с людьми всех вероисповеданий и даже теми кто не имеет никакой веры.\n\n                    Каритас стремится к миру, где голоса бедных услышаны и приняты во внимание, где каждый человек может\n                    свободно процветать и жить в мире и достоинстве, и где наша естественная среда, данная Богом,\n                    управляется ответственно и устойчиво в интересах всего человечества. семьи.\n                    «Развитие народов зависит от признания того, что человеческая раса - это одна семья, работающая\n                    вместе…» (Папа Иоанн Павел II).\n                </p>\n            </div>\n            <div class=\"col-sm-12\">\n                <div class=\"wrapper-vision\">\n                    <div class=\"absolute-wrapper__note\">\n                        <h2>С любовью</h2>\n                        <p>Благодаря успешному партнерству в гуманитарной помощи и для развития и преображения сердец и\n                            мыслей в обществе, Каритас оказывает помощь в борьбе с бедностью </p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"charity-container\">\n            <div class=\"row\">\n                <div class=\"col-sm-1\"></div>\n                <div class=\"col-sm-5\">\n                    <div class=\"charity-container__option\">\n\n                    </div>\n                    <div class=\"charity-container__option\">\n                        <h2>Пожертовать средства</h2>\n                        <p>Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как в\n                            Казахстане,\n                            так и во всем мире. Это означает защиту права детей для .</p>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"charity-container__wrapper\">\n                <div class=\"container-phone\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 1 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 5 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 10 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 25 000 тенге</a></div>\n                        <div class=\"col-sm-12\">\n                            <input type=\"text\">\n                        </div>\n                        <div class=\"col-sm-12\">\n                            <a href=\"\" class=\"btn-give\">Пожертвовать</a>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n\n        </div>\n        <div class=\"row\">\n            <div class=\"facts\">\n                <p>At some point, western civilization made an emotional\n                    choice to treat dogs like our best friends</p>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about.component.html": 
        /*!**********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about.component.html ***!
          \**********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"about-container\" wheelOn=\"integer\">\n    <a id=\"classLink\" routerLink=\"/home/about/about-inner\"></a>\n    <div class=\"fotorama\" data-nav=\"thumbs\" data-width=\"100%\" data-height=\"100%\" data-arrows=\"false\"\n        data-thumbwidth=\"160\" data-thumbheight=\"160\">\n        <div class=\"slide1\" data-thumb=\"/assets/images/about/about-slide1.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n            <!-- <img src=\"{{ slide1 }}\"> -->\n        </div>\n        <div class=\"slide2\" data-thumb=\"/assets/images/about/about-slide2.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n\n            <!-- <img src=\"{{ slide2 }}\"> -->\n        </div>\n        <div class=\"slide3\" data-thumb=\"/assets/images/about/about-slide3.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n\n\n            <!-- <img src=\"{{ slide3 }}\"> -->\n        </div>\n        <div class=\"slide4\" data-thumb=\"/assets/images/about/about-slide4.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n            <!-- <img src=\"{{ slide4 }}\"> -->\n        </div>\n        <div class=\"slide5\" data-thumb=\"/assets/images/about/about-slide5.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n            <!-- <img src=\"{{ slide5 }}\"> -->\n        </div>\n        <div class=\"slide6\" data-thumb=\"/assets/images/about/about-slide6.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n            <!-- <img src=\"{{ slide6 }}\"> -->\n        </div>\n        <div class=\"slide7\" data-thumb=\"/assets/images/about/about-slide7.png\">\n            <a routerLink=\"/home/about/about-inner\">\n                <h5>О нас</h5>\n            </a>\n            <span>The people, the rainforest, the Church</span>\n            <p class=\"about__region\">Деятельность Caritas в Казахстане</p>\n            <p class=\"about__desc\">Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как\n                в Казахстане, так и\n                во всем мире. Это означает защиту права детей для выживания, развития и полной реализации потенциала в\n            </p>\n            <!-- <img src=\"{{ slide7 }}\"> -->\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/archive/archive.component.html": 
        /*!**************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/archive/archive.component.html ***!
          \**************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\r\n\r\n<div class=\"archive-container\"  wheelOn=\"integer\">\r\n    <div class=\"archive-header\">\r\n        <span>\r\n            <h2>\r\n                Архив </h2>\r\n        </span>\r\n    </div>\r\n    <div class=\"archive-content\">\r\n        <img src=\"{{ bear }}\" alt=\"\" class=\"archive__bear\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12 col-md-6\">\r\n                <a >\r\n                    <div class=\"archive-container-article\">\r\n                        <p>Истории <span>пятница - 10:14</span></p>\r\n                        <h3>Сила девочек: яркая и неудержимая</h3>\r\n                        <p> История алматинской школьницы, покоряющей Кремниевую долину и выступающей за предоставление\r\n                            возможностей для молодежи</p>\r\n                    </div>\r\n                </a>\r\n                <a  >\r\n                    <div class=\"archive-container-article\">\r\n                        <p>Истории <span>пятница - 10:14</span></p>\r\n                        <h3>Сила девочек: яркая и неудержимая</h3>\r\n                        <p> История алматинской школьницы, покоряющей Кремниевую долину и выступающей за предоставление\r\n                            возможностей для молодежи</p>\r\n                    </div>\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-6 col-md-6 archive-mobile\">\r\n                <img src=\"{{ archivePhoto }}\" alt=\"\" srcset=\"\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity-inner/charity-inner.component.html": 
        /*!**********************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity-inner/charity-inner.component.html ***!
          \**********************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"charity-inner-container\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"charity-container__title\">\n                <a routerLink=\"home/charity/charity-inner\">\n                    <h3>Как помочь</h3>\n                </a>\n                <p>Our mission is based on nine core principles of Catholic Social Teaching</p>\n            </div>\n        </div>\n        <div class=\"row\">\n            <h5 class=\"charity-container__title\">Заголовок1</h5>\n            <p class=\"charity-container__text\">Мы вдохновлены Евангелием учением католической церкви и надеждами людей,\n                живущих в нищете. Мы призываем каждого отвечать на гуманитарные потребности, продвигать целостному\n                развитию человечества и выступаем за причины возникновения бедноты и насилия.\n\n                Мы вдохновляем католические общины и всех людей с благими побуждениями к солидарности ко страданию их\n                братьев и сестер по всему миру.\n\n                Мы работаем над тем, чтобы убедить что наша естественная среда управлялась ответственно и бережно в\n                интересах всей человеческой семьи.\n            </p>\n        </div>\n        <div class=\"row\">\n            <h5 class=\"charity-container__title\">Волонтерство</h5>\n            <p class=\"charity-container__text\">\n                Благодаря успешному партнерству в гуманитарной помощи и для развития и преображения сердец и мыслей в\n                обществе, Каритас оказывает помощь в борьбе с бедностью, побуждает справедливость, и исповедует\n                благородство. Мы пытаемся отвечать потребностям тех, кто страдает старыми и новыми видами бедности. В то\n                же время мы мобилизуем наши человеческие и материальные ресурсы и ресурсы тех кто разделяет с нами наши\n                идеи, чтобы сделать наш мир «общим домом» лучшим местом.\n            </p>\n        </div>\n\n        <ngu-carousel id=\"ngCarousel\" #myCarousel [inputs]=\"carouselTile\" [dataSource]=\"carouselTileItems\">\n            <ngu-tile *nguCarouselDef=\"let item; let i = index\">\n\n                <ngu-carousel id=\"ngCarousel-content\" #myCarousel [inputs]=\"carouselTile\"\n                    (carouselLoad)=\"carouselTileLoad(i)\" [dataSource]=\"carouselTiles[i]\">\n                    <ngu-tile *nguCarouselDef=\"let item; let j = index\">\n                        <div class=\"tile\" [style.background]=\"'url(' + item + ')'\" style=\"min-height: 200px;\">\n\n                        </div>\n                    </ngu-tile>\n                    <button NguCarouselPrev class=\"leftRs\" [style.opacity]=\"myCarousel.isFirst ? 0.5:1\">&lt;</button>\n                    <button NguCarouselNext class=\"rightRs\" [style.opacity]=\"myCarousel.isLast ? 0.5:1\">&gt;</button>\n\n                </ngu-carousel>\n\n            </ngu-tile>\n\n        </ngu-carousel>\n\n\n        <div class=\"row\">\n            <a href=\"#\" class=\"btn_be-part-of\">Вступить в ряды</a>\n        </div>\n        <div class=\"row\">\n            <h5 class=\"charity-container__title\">Волонтерство</h5>\n            <p class=\"charity-container__text\">\n                Знак любви Бога к человечеству.\n\n                Мы работаем с людьми всех вероисповеданий и даже теми кто не имеет никакой веры.\n\n                Каритас стремится к миру, где голоса бедных услышаны и приняты во внимание, где каждый человек может\n                свободно процветать и жить в мире и достоинстве, и где наша естественная среда, данная Богом,\n                управляется ответственно и устойчиво в интересах всего человечества. семьи.\n                «Развитие народов зависит от признания того, что человеческая раса - это одна семья, работающая вместе…»\n                (Папа Иоанн Павел II).\n\n            </p>\n        </div>\n        <div class=\"achivment-wrapper\">\n            <div class=\"row\">\n                <div class=\"absolute__container-achivment\">\n                    <p><span>10 235 485 тенге</span></p>\n                    <p>Собрали за все время</p>\n                </div>\n                <img class=\"image-family\" src=\"{{ family }}\" alt=\"\">\n            </div>\n        </div>\n        <div class=\"charity-container\">\n            <div class=\"row\">\n                <div class=\"col-sm-1\"></div>\n                <div class=\"col-sm-5\">\n                    <div class=\"charity-container__option\">\n\n                    </div>\n                    <div class=\"charity-container__option\">\n                        <h2>Пожертовать средства</h2>\n                        <p>Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как в\n                            Казахстане,\n                            так и во всем мире. Это означает защиту права детей для .</p>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"charity-container__wrapper\">\n                <div class=\"container-phone\">\n                    <div class=\"row\">\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 1 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 5 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 10 000 тенге</a></div>\n                        <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 25 000 тенге</a></div>\n                        <div class=\"col-sm-12\">\n                            <input type=\"text\">\n                        </div>\n                        <div class=\"col-sm-12\">\n                            <a href=\"\" class=\"btn-give\">Пожертвовать</a>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n\n        </div>\n        <div class=\"row\">\n            <div class=\"facts\">\n                <p>At some point, western civilization made an emotional\n                    choice to treat dogs like our best friends</p>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity.component.html": 
        /*!**************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity.component.html ***!
          \**************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"charity-container\"  wheelOn=\"integer\">\n    <div class=\"charity-header\">\n        <span>\n            <h2>\n                Как помочь </h2>\n        </span>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-1\"></div>\n        <div class=\"col-sm-5\">\n            <div class=\"charity-container__option\">\n                <h2>Волонтерство</h2>\n                <p>Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как в Казахстане,\n                    так и во всем мире. Это означает защиту права детей для .</p>\n                <a href=\"\">Вступить в ряды</a>\n            </div>\n            <div class=\"charity-container__option\">\n                <h2>Пожертовать средства</h2>\n                <p>Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ как в Казахстане,\n                    так и во всем мире. Это означает защиту права детей для .</p>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"charity-container__wrapper\">\n        <div class=\"container-phone\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 1 000 тенге</a></div>\n                <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 5 000 тенге</a></div>\n                <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 10 000 тенге</a></div>\n                <div class=\"col-sm-12 col-md-6\"><a href=\"\"> 25 000 тенге</a></div>\n                <div class=\"col-sm-12\">\n                    <input type=\"text\">\n                </div>\n                <div class=\"col-sm-12\">\n                    <a href=\"\" class=\"btn-give\">Пожертвовать</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.html": 
        /*!*************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.html ***!
          \*************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"contacts-inner-container\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"contacts-container__title\">\n                \n                <h3>Контакты</h3>\n                \n            </div>\n        </div>\n        <div class=\"row contacts-container__address--padding\">\n            <div class=\"col-sm-4\">\n                <div class=\"contacts-container__title\">\n                    <p><span> Адресс </span></p>\n                    <p class=\"contacts-container__title--redline\">г. Нур-Султан, ул. Кенесары 72</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <div class=\"contacts-container__title\">\n                    <p><span> Телефон </span></p>\n                    <p class=\"contacts-container__title--redline\">+7 900 231 00 90</p>\n                </div>\n            </div>\n            <div class=\"col-sm-4\">\n                <div class=\"contacts-container__title\">\n                    <p><span> E-mail </span></p>\n                    <p class=\"contacts-container__title--redline\">info@mail.ru</p>\n                </div>\n            </div>\n            <div class=\"col-sm-12\">\n                <iframe\n                    src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19552.209420897885!2d76.9334021957793!3d43.24192026022259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836ee31c97e51f%3A0xfecb868752f516a9!2sCentral%20State%20Museum%20of%20the%20Republic%20of%20Kazakhstan!5e0!3m2!1sen!2skz!4v1574332240730!5m2!1sen!2skz\"\n                    width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-8\">\n                <div class=\"legal-info\">\n                    <h2>Реквизиты компании:</h2>\n                    <p>ОГРН 1089848059366</p>\n                    <p> ОКПО 89041828</p>\n                    <p> ОКАТО 40278562000</p>\n                    <p>ИНН 7842399004</p>\n                    <p>КПП 780601001</p>\n                    <p>Р/С 40702810124000011658</p>\n                    <p>БИК 044525976</p>\n                    <p>К/С 30101810500000000976</p>\n\n                    <p>Банк: АКБ \"АБСОЛЮТ БАНК\" (ПАО)</p>\n                    <p>(127051, Г. МОСКВА, ЦВЕТНОЙ Б-Р,18, ИНН 7736046991, КПП 770201001)</p>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <form action=\"\">\n                    <div class=\"contacts-inner-container__submitQ\">\n\n                        <h2>Есть вопросы? Пишите нам</h2>\n                        <p>Ответим в ближайшее время на все ваши вопросы</p>\n                        <div class=\"input-wrapper\">\n                            <input class=\"input-info1\" type=\"text\" placeholder=\"Ваше имя\"> <input class=\"input-info2\"\n                                type=\"text\" placeholder=\"Ваш e-mail\">\n                        </div>\n                        <textarea placeholder=\"Введите ваш вопрос\" name=\"\" id=\"\" cols=\"30\" rows=\"1\"></textarea>\n                        <a href=\"\">отправить</a>\n\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts.component.html": 
        /*!****************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts.component.html ***!
          \****************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"contacts-container\" wheelOn=\"integer\">\n    <div class=\"contacts-header\">\n        <span>\n            <a routerLink=\"home/contacts/contacts-inner\">\n                <h2> контакты </h2>\n            </a>\n        </span>\n    </div>\n    <div class=\"contacts-content\">\n        <div class=\"row\">\n            <div class=\"col-sm-1\"></div>\n            <div class=\"col-sm-8\" style=\"z-index: 1\">\n                <h2>+44 (0)7867 532021</h2>\n                <p class=\"contacts-content__email\">support@apidura.com </p>\n                <p class=\"contacts-content__work-time\"><span>9am-6pm UK Time Monday-Friday</span></p>\n                <p class=\"contacts-content__sub-comment\"><span>Подпишитесь на рассылку</span></p>\n                <div class=\"input-container\">\n                    <input type=\"text\">\n                    <a class=\"input-container__btn\">Подписаться</a>\n                </div>\n            </div>\n        </div>\n        <!-- <div class=\"row contacts-content__footer\">\n            <div class=\"col-sm-1\"></div>\n            <div class=\"col-sm-2\">\n                <h2>о нас</h2>\n                <a href=\"\">О нас</a>\n                <a href=\"\">Конференция</a>\n                <a href=\"\">В рк</a>\n            </div>\n            <div class=\"col-sm-2\">\n                <h2>проекты</h2>\n                <a href=\"\">ЭКО</a>\n                <a href=\"\">ЭКО</a>\n                <a href=\"\">Уход</a>\n            </div>\n            <div class=\"col-sm-2\">\n                <h2>как помочь</h2>\n                <a href=\"\">Счета</a>\n                <a href=\"\">Волонтерство</a>\n            </div>\n            <div class=\"col-sm-2\">\n                <h2>архив</h2>\n                <a href=\"\">Видео</a>\n                <a href=\"\">Документы</a>\n            </div>\n        </div> -->\n    </div>\n    <img src=\"{{ ball1 }}\" alt=\"\" id=\"ball1\">\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/map/map.component.html": 
        /*!******************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/map/map.component.html ***!
          \******************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"map-container\"  wheelOn=\"integer\">\n<div class=\"map-container__absolute-title\">\n    <h2>\n        Где мы\n    </h2>\n</div>\n\n<img src=\"{{ mapPng }}\" alt=\"\">\n\n<div class=\"map-container__absolute-sub\">\n\n    <h2>\n            Первым узнавайте самые важные новости \n            о наших подопечных\n    </h2>\n    <p>Подпишитесь на рассылку фонда и получайте отчеты о переводах средств детям</p>\n\n    <a >подписаться</a>\n</div>\n</div>\n\n\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-event/news-event.component.html": 
        /*!************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-event/news-event.component.html ***!
          \************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"news-event-container\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 px-0\">\n                    <div class=\"news-event-container__title\" [ngStyle]=\"styles\">\n                        <h3>Синдром дауна</h3>\n                        <p>The people, the rainforest, the Church</p>\n                        <div class=\"news-event-container__title__body\">\n                            <h3>Немного о патологии Синдром Дауна.</h3>\n                            <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром\n                                Дауна\n                                впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959\n                                году,\n                                французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                                определяется наличием в клетках человека дополнительной хромосомы</p>\n                        </div>\n                    </div>\n    \n    \n                </div>\n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <div class=\"news-event-container__content\">\n    \n                        <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                            впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                            французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                            определяется наличием в клетках человека дополнительной хромосомы.\n    \n                            Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                            ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                            этапы развития.\n    \n                            Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                            умеют делать другие дети.</p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row\">\n    \n                <ngu-carousel id=\"ngCarousel\" #myCarousel [inputs]=\"carouselTile\" [dataSource]=\"carouselTileItems\">\n                    <ngu-tile *nguCarouselDef=\"let item; let i = index\">\n    \n                        <ngu-carousel id=\"ngCarousel-content\" #myCarousel [inputs]=\"carouselTile\"\n                            (carouselLoad)=\"carouselTileLoad(i)\" [dataSource]=\"carouselTiles[i]\">\n                            <ngu-tile *nguCarouselDef=\"let item; let j = index\">\n                                <div class=\"tile\" [style.background]=\"'url(' + item + ')'\" style=\"min-height: 200px\">\n                                    <!-- <h1>{{j}}</h1> -->\n                                </div>\n                            </ngu-tile>\n                            <button NguCarouselPrev class=\"leftRs\"\n                                [style.opacity]=\"myCarousel.isFirst ? 0.5:1\">&lt;</button>\n                            <button NguCarouselNext class=\"rightRs\"\n                                [style.opacity]=\"myCarousel.isLast ? 0.5:1\">&gt;</button>\n                            <!-- <ul class=\"myPoint\" NguCarouselPoint>\n                                          <li *ngFor=\"let j of myCarousel.pointNumbers; let j = index\" [class.active]=\"j==myCarousel.activePoint\" (click)=\"myCarousel.moveTo(j)\"\n                                            [style.background]=\"'url(' + carouselTileItems[j] + ')'\"></li>\n                                        </ul> -->\n                        </ngu-carousel>\n    \n                    </ngu-tile>\n                    <!-- <button NguCarouselPrev class=\"leftRs\" [style.opacity]=\"myCarousel.isFirst ? 0.5:1\">&lt;</button>\n                                    <button NguCarouselNext class=\"rightRs\" [style.opacity]=\"myCarousel.isLast ? 0.5:1\">&gt;</button> -->\n                    <!-- <ul class=\"myPoint\" NguCarouselPoint>\n                                      <li *ngFor=\"let i of myCarousel.pointNumbers; let i = index\" [class.active]=\"i==myCarousel.activePoint\" (click)=\"myCarousel.moveTo(i)\"\n                                        [style.background]=\"'url(' + carouselTileItems[i] + ')'\"></li>\n                                    </ul> -->\n                </ngu-carousel>\n    \n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <div class=\"news-event-container__content\">\n                        <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                            впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                            французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                            определяется наличием в клетках человека дополнительной хромосомы.\n    \n                            Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                            ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                            этапы развития.\n    \n                            Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                            умеют делать другие дети.</p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"row \">\n    \n                <div class=\"col-sm-6 pl-4\">\n    \n                </div>\n                <div class=\"col-sm-6 pr-4\">\n                    <img src=\"{{  imageRight }}\" alt=\"imageRight\" srcset=\"\" class=\"image-left\">\n                </div>\n    \n                <div class=\"col-sm-12\">\n                    <p class=\"info--grey-color\">Слово «синдром» подразумевает наличие определенных признаков или характерных\n                        черт. Синдром Дауна впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет\n                        спустя, в 1959 году, французский ученый Жером Лежен обосновал генетическое происхождение этого\n                        синдрома, которое определяется наличием в клетках человека дополнительной хромосомы.\n    \n                        Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                        ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей этапы\n                        развития.\n    \n                        Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                        умеют делать другие дети.</p>\n                </div>\n    \n            </div>\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <div class=\"news-event-container__content\">\n                        <h3>Целью проекта является оказание\n                            профессиональной психолого-педагогической\n                            помощи семьям в которых есть ребенок\n                            с синдромом Дауна.</h3>\n                        <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                            впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                            французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                            определяется наличием в клетках человека дополнительной хромосомы.\n    \n                            Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                            ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                            этапы развития.\n    \n                            Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                            умеют делать другие дети.</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-inner.component.html": 
        /*!*************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-inner.component.html ***!
          \*************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"news-inner-container\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"news-inner-container__title\">\n                <h3>Новости</h3>\n                <p>Our mission is based on nine core principles of Catholic Social Teaching</p>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"news-inner-container__tabs\">\n                <ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\"\n                            aria-controls=\"home\" aria-selected=\"true\">Казахстан</a>\n                    </li>\n                    <li class=\"nav-item\">\n                        <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\"\n                            aria-controls=\"profile\" aria-selected=\"false\">Мир</a>\n                    </li>\n                </ul>\n                <div class=\"tab-content\" id=\"myTabContent\">\n                    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-6 waterfall-first\">\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                            <div class=\"col-sm-3 waterfall-second\">\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                            <div class=\"col-sm-3 waterfall-second\">\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard1 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\n                        <div class=\"row\">\n                            <div class=\"col-sm-6 waterfall-first\">\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n                                            <p><span>Истории</span><span>Пятница - 10:14</span> </p>\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                            <div class=\"col-sm-3 waterfall-second\">\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                            <div class=\"col-sm-3 waterfall-second\">\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                                <div class=\"news-card-other\">\n                                    <a routerLink=\"./news-event\">\n                                        <img src=\"{{ newsCard2 }}\" alt=\"\">\n                                        <div class=\"news-info\">\n\n                                            <p>Поколение безграничных\n                                                возможностей в Казахстане</p>\n                                            <p><a href=\"#\"><span>читать подробнее</span></a> </p>\n                                        </div>\n                                    </a>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news.component.html": 
        /*!********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news.component.html ***!
          \********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"news-container\" wheelOn=\"integer\">\r\n    <div class=\"news-header\">\r\n        <span>\r\n            <a [routerLink]=\"['/home/news/news-inner']\" >\r\n                <h2>\r\n                    Новости\r\n                </h2>\r\n            </a>\r\n        </span>\r\n    </div>\r\n    <div class=\"news-header-btn\">\r\n        <div class=\"news-header-btn__wrapper\">\r\n            <ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">\r\n                <li class=\"nav-item\">\r\n                    <a class=\"news-header-btn__btn-tabs nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\"\r\n                        role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Казахстан</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\" news-header-btn__btn-tabs nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\"\r\n                        role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">Мир</a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"news-content\">\r\n        <div class=\"tab-content\" id=\"myTabContent\">\r\n            <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-1\"></div>\r\n                    <div class=\"col-sm-3 news-img\">\r\n                        <div class=\"news-container__article-wrapper\">\r\n                            <img src=\"{{ babyImg }}\" alt=\"\" class=\"news-img_target-1\">\r\n                            <div class=\"first-column__article\">\r\n                                <p>Обеспечение равных возможностей для детей – </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4 news-img\">\r\n                        <div class=\"news-container__article-wrapper \">\r\n                            <div class=\"second-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n\r\n                            </div>\r\n                            <img src=\"{{ boyImg }}\" alt=\"\" class=\"news-img_target-2\">\r\n                            <div class=\"second-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-3 news-img\">\r\n                        <div class=\"news-container__article-wrapper news-container__article-wrapper--box-shadow\">\r\n                            <img src=\"{{ manImg }}\" alt=\"\" class=\"news-img_target-3\">\r\n                            <div class=\"third-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-1\"></div>\r\n                </div>\r\n            </div>\r\n            <div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-1\"></div>\r\n                    <div class=\"col-sm-3 news-img\">\r\n                        <div class=\"news-container__article-wrapper\">\r\n                            <img src=\"{{ babyImg }}\" alt=\"\" class=\"news-img_target-1\">\r\n                            <div class=\"first-column__article\">\r\n                                <p>Обеспечение равных возможностей для детей – </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>ng\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-4 news-img\">\r\n                        <div class=\"news-container__article-wrapper \">\r\n                            <div class=\"second-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n\r\n                            </div>\r\n                            <img src=\"{{ boyImg }}\" alt=\"\" class=\"news-img_target-2\">\r\n                            <div class=\"second-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-3 news-img\">\r\n                        <div class=\"news-container__article-wrapper news-container__article-wrapper--box-shadow\">\r\n                            <img src=\"{{ manImg }}\" alt=\"\" class=\"news-img_target-3\">\r\n                            <div class=\"third-column__article\">\r\n                                <p>\r\n                                    Обеспечение равных возможностей для детей – одна из самых важных задач для ЮНИСЕФ\r\n                                    как в\r\n                                    Казахстане, так и во всем мире. Это означает\r\n                                </p>\r\n                                <div class=\"news-content__read--wrapper\">\r\n                                    <a routerLink='/home/news/news-inner/news-event' class=\"news-content__read\">Читать далее</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"col-sm-1\"></div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <img src=\"{{ ball1 }}\" alt=\"\" id=\"ball1\">\r\n    <img src=\"{{ ball2 }}\" alt=\"\" id=\"ball2\">\r\n    <img src=\"{{ ball3 }}\" alt=\"\" id=\"ball3\">\r\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/project/project.component.html": 
        /*!**************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/project/project.component.html ***!
          \**************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"project-container\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-12 px-0\">\n                <div class=\"projects-container__title\" [ngStyle]=\"styles\">\n                    <h3>Синдром дауна</h3>\n                    <p>The people, the rainforest, the Church</p>\n                    <div class=\"projects-container__title__body\">\n                        <h3>Немного о патологии Синдром Дауна.</h3>\n                        <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром\n                            Дауна\n                            впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959\n                            году,\n                            французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                            определяется наличием в клетках человека дополнительной хромосомы</p>\n                    </div>\n                </div>\n\n\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <div class=\"project-container__content\">\n\n                    <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                        впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                        французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                        определяется наличием в клетках человека дополнительной хромосомы.\n\n                        Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                        ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                        этапы развития.\n\n                        Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                        умеют делать другие дети.</p>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n\n            <ngu-carousel id=\"ngCarousel\" #myCarousel [inputs]=\"carouselTile\" [dataSource]=\"carouselTileItems\">\n                <ngu-tile *nguCarouselDef=\"let item; let i = index\">\n\n                    <ngu-carousel id=\"ngCarousel-content\" #myCarousel [inputs]=\"carouselTile\"\n                        (carouselLoad)=\"carouselTileLoad(i)\" [dataSource]=\"carouselTiles[i]\">\n                        <ngu-tile *nguCarouselDef=\"let item; let j = index\">\n                            <div class=\"tile\" [style.background]=\"'url(' + item + ')'\" style=\"min-height: 200px\">\n                                <!-- <h1>{{j}}</h1> -->\n                            </div>\n                        </ngu-tile>\n                        <button NguCarouselPrev class=\"leftRs\"\n                            [style.opacity]=\"myCarousel.isFirst ? 0.5:1\">&lt;</button>\n                        <button NguCarouselNext class=\"rightRs\"\n                            [style.opacity]=\"myCarousel.isLast ? 0.5:1\">&gt;</button>\n                        <!-- <ul class=\"myPoint\" NguCarouselPoint>\n                                      <li *ngFor=\"let j of myCarousel.pointNumbers; let j = index\" [class.active]=\"j==myCarousel.activePoint\" (click)=\"myCarousel.moveTo(j)\"\n                                        [style.background]=\"'url(' + carouselTileItems[j] + ')'\"></li>\n                                    </ul> -->\n                    </ngu-carousel>\n\n                </ngu-tile>\n                <!-- <button NguCarouselPrev class=\"leftRs\" [style.opacity]=\"myCarousel.isFirst ? 0.5:1\">&lt;</button>\n                                <button NguCarouselNext class=\"rightRs\" [style.opacity]=\"myCarousel.isLast ? 0.5:1\">&gt;</button> -->\n                <!-- <ul class=\"myPoint\" NguCarouselPoint>\n                                  <li *ngFor=\"let i of myCarousel.pointNumbers; let i = index\" [class.active]=\"i==myCarousel.activePoint\" (click)=\"myCarousel.moveTo(i)\"\n                                    [style.background]=\"'url(' + carouselTileItems[i] + ')'\"></li>\n                                </ul> -->\n            </ngu-carousel>\n\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <div class=\"project-container__content\">\n                    <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                        впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                        французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                        определяется наличием в клетках человека дополнительной хромосомы.\n\n                        Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                        ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                        этапы развития.\n\n                        Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                        умеют делать другие дети.</p>\n                </div>\n            </div>\n        </div>\n        <div class=\"row \">\n\n            <div class=\"col-sm-6 pl-4\">\n\n            </div>\n            <div class=\"col-sm-6 pr-4\">\n                <img src=\"{{  imageRight }}\" alt=\"imageRight\" srcset=\"\" class=\"image-left\">\n            </div>\n\n            <div class=\"col-sm-12\">\n                <p class=\"info--grey-color\">Слово «синдром» подразумевает наличие определенных признаков или характерных\n                    черт. Синдром Дауна впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет\n                    спустя, в 1959 году, французский ученый Жером Лежен обосновал генетическое происхождение этого\n                    синдрома, которое определяется наличием в клетках человека дополнительной хромосомы.\n\n                    Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                    ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей этапы\n                    развития.\n\n                    Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                    умеют делать другие дети.</p>\n            </div>\n\n        </div>\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <div class=\"project-container__content\">\n                    <h3>Целью проекта является оказание\n                        профессиональной психолого-педагогической\n                        помощи семьям в которых есть ребенок\n                        с синдромом Дауна.</h3>\n                    <p>Слово «синдром» подразумевает наличие определенных признаков или характерных черт. Синдром Дауна\n                        впервые описал в 1866 году британский врач Джон Лэнгдон Даун. Почти сто лет спустя, в 1959 году,\n                        французский ученый Жером Лежен обосновал генетическое происхождение этого синдрома, которое\n                        определяется наличием в клетках человека дополнительной хромосомы.\n\n                        Наличие этой генетической аномалии обуславливает появление ряда особенностей, вследствие которых\n                        ребенок будет медленнее развиваться и позже своих ровесников проходить общие для всех детей\n                        этапы развития.\n\n                        Таким малышам труднее учиться. И все же большинство детей с синдромом Дауна может делать то, что\n                        умеют делать другие дети.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/projects-inner.component.html": 
        /*!*************************************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/projects-inner.component.html ***!
          \*************************************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"projects-inner-container\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-sm-12\">\n                <div class=\"projects-inner-container__title\">\n                    <h3>Проекты</h3>\n                    <p>Our mission is based on nine core principles of Catholic Social Teaching</p>\n                </div>\n            </div>\n        </div>\n        <div class=\"projects-inner-container__body\">\n            <div class=\"row\">\n                <div class=\"col-sm-12\">\n                    <p class=\"projects-inner-container__body__title\">Основные проекты</p>\n                </div>\n                <div class=\"col-sm-3\">\n                    <a  routerLink=\"./project\">\n                        <img src=\"{{ proj1 }}\" alt=\"proj1\">\n                        <p>{{  proj1Title | uppercase }}</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-3\">\n                    <a  routerLink=\"./project\">\n                        <img src=\"{{ proj2 }}\" alt=\"proj1\">\n                        <p>{{  proj1Title | uppercase }}</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-3\">\n                    <a  routerLink=\"./project\">\n                        <img src=\"{{ proj3 }}\" alt=\"proj1\">\n                        <p>{{  proj1Title | uppercase }}</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-3\">\n                    <a  routerLink=\"./project\">\n                        <img src=\"{{ proj4 }}\" alt=\"proj1\">\n                        <p>{{  proj1Title | uppercase }}</p>\n                    </a>\n                </div>\n                <div class=\"col-sm-3\">\n                    <a  routerLink=\"./project\">\n                        <img src=\"{{ proj5 }}\" alt=\"proj1\">\n                        <p>{{  proj1Title | uppercase }}</p>\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects.component.html": 
        /*!****************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects.component.html ***!
          \****************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"project-container\"  wheelOn=\"integer\">\r\n    <div class=\"projects-header\">\r\n        <span>\r\n            <a [routerLink]=\"['/home/projects/projects-inner']\">\r\n            <h2 >\r\n                Наши проекты\r\n            </h2>\r\n        </a>\r\n        </span>\r\n\r\n        <p>\r\n            Our mission is based on nine core principles of Catholic Social Teaching\r\n        </p>\r\n    </div>\r\n\r\n\r\n    <div class=\"glide-projects\">\r\n        <div class=\"glide__track\" data-glide-el=\"track\">\r\n            <ul class=\"glide__slides\">\r\n                <li class=\"glide__slide\">\r\n                    <a routerLink=\"./projects-inner/project\">\r\n                        <img src=\"{{ proj1 }}\" alt=\"proj1\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"glide__slide\">\r\n                    <a routerLink=\"./projects-inner/project\">\r\n                        <img src=\" {{ proj2 }}\" alt=\"proj2\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"glide__slide\">\r\n                    <a routerLink=\"./projects-inner/project\">\r\n                        <img src=\" {{ proj3 }}\" alt=\"proj3\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"glide__slide\">\r\n                    <a routerLink=\"./projects-inner/project\">\r\n                        <img src=\" {{ proj4 }}\" alt=\"proj4\">\r\n                    </a>\r\n                </li>\r\n                <li class=\"glide__slide\">\r\n                    <a routerLink=\"./projects-inner/project\">\r\n                        <img src=\" {{ proj5 }}\" alt=\"proj5\">\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"projects-footer\">\r\n        <a routerLink=\"./projects-inner\">перейти к проектам </a>\r\n    </div>\r\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/welcome/welcome.component.html": 
        /*!**************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/welcome/welcome.component.html ***!
          \**************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"welcome__wrapper\" wheelOn=\"integer\">\n    <div class=\"glide\">\n        <div class=\"glide__track\" data-glide-el=\"track\">\n            <ul class=\"glide__slides\">\n                <li class=\"glide__slide\">\n                    <img src=\"{{ slide1 }}\" alt=\"slide1\">\n                </li>\n                <li class=\"glide__slide\">\n                    <img src=\"{{ slide2 }}\" alt=\"slide2\">\n                </li>\n                <li class=\"glide__slide\">\n                    <img src=\"{{ slide3 }}\" alt=\"slide3\">\n                </li>\n                <li class=\"glide__slide\">\n                    <img src=\"{{ slide4 }}\" alt=\"slide4\">\n                </li>\n            </ul>\n        </div>\n\n    </div>\n\n    <div class=\"logo-welcome--absolute\">\n        <img src=\"{{ logoWelcome }}\" alt=\"logoWelcome\">\n    </div>\n\n    <h2 class=\"welcome__h2\">\n        Caritas: ending poverty,\n        promoting justice\n        and restoring dignity\n    </h2>\n\n    <div class=\"button-container\">\n        <div class=\"button-container__a\"></div>\n        <div class=\"button-container__a\"></div>\n    </div>\n</div>");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/error404/error404.component.html": 
        /*!**********************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/error404/error404.component.html ***!
          \**********************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<p>error404 works!</p>\n");
            /***/ 
        }),
        /***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html": 
        /*!**************************************************************************************************************!*\
          !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html ***!
          \**************************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("<div class=\"menuLang-absolute\">\n    <a >RU</a>\n    <a >KZ</a>\n    <a >EN</a>\n    <!-- <a  id=\"menu-toggle\" routerLinkActive=\"active\">\n        <div></div>\n    </a> -->\n</div>\n<div class=\"navigation-wrapper\">\n\n    <div class=\"container-fluid\">\n        <div class=\"navigation-wrapper__logo\">\n            <a routerLink=\"\" routerLinkActive=\"active\">\n                <img src=\"{{ logoSrc }}\" alt=\"caritas-logo.png\">\n            </a>\n        </div>\n        <div class=\"navigation-wrapper__list\">\n\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"about/about-inner\" routerLinkActive=\"active\" class=\"text1\">О нас</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"map\" routerLinkActive=\"active\" class=\"text2\">Где мы</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"projects/projects-inner\" routerLinkActive=\"active\" class=\"text3\">Наши проекты</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"news/news-inner\" routerLinkActive=\"active\" class=\"text4\">Новости</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"charity/charity-inner\" routerLinkActive=\"active\" class=\"text5\">Как помочь</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"archive\" routerLinkActive=\"active\" class=\"text6\">Архив</a>\n            </div>\n            <div class=\"col-sm-12 px-0\">\n                <a routerLink=\"contacts/contacts-inner\" routerLinkActive=\"active\" class=\"text7\">Контакты</a>\n            </div>\n\n        </div>\n        <div class=\"navigation-wrapper__socials-container\">\n            <img src=\"{{ instagram }}\" alt=\"instagram\">\n            <img src=\"{{ facebook }}\" alt=\"facebook\">\n            <img src=\"{{ twitter }}\" alt=\"twitter\">\n            <img src=\"{{ youtube }}\" alt=\"youtube\">\n        </div>\n    </div>\n</div>\n<!-- <div id=\"hidden-target\">\n        <a routerLink=\"home/welcome\"></a>\n        <a routerLink=\"home/about\"></a>\n        <a routerLink=\"home/map\"></a>\n        <a routerLink=\"home/projects\"></a>\n        <a routerLink=\"home/news\"></a>\n        <a routerLink=\"home/charity\"></a>\n        <a routerLink=\"home/archive\"></a>\n        <a routerLink=\"home/contacts\"></a>\n    </div> -->");
            /***/ 
        }),
        /***/ "./node_modules/tslib/tslib.es6.js": 
        /*!*****************************************!*\
          !*** ./node_modules/tslib/tslib.es6.js ***!
          \*****************************************/
        /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function () { return __extends; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function () { return __assign; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function () { return __rest; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function () { return __decorate; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function () { return __param; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function () { return __metadata; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function () { return __awaiter; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function () { return __generator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function () { return __exportStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function () { return __values; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function () { return __read; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function () { return __spread; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () { return __spreadArrays; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function () { return __await; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () { return __asyncGenerator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () { return __asyncDelegator; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function () { return __asyncValues; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () { return __makeTemplateObject; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function () { return __importStar; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function () { return __importDefault; });
            /*! *****************************************************************************
            Copyright (c) Microsoft Corporation. All rights reserved.
            Licensed under the Apache License, Version 2.0 (the "License"); you may not use
            this file except in compliance with the License. You may obtain a copy of the
            License at http://www.apache.org/licenses/LICENSE-2.0
            
            THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
            KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
            WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
            MERCHANTABLITY OR NON-INFRINGEMENT.
            
            See the Apache Version 2.0 License for specific language governing permissions
            and limitations under the License.
            ***************************************************************************** */
            /* global Reflect, Promise */
            var extendStatics = function (d, b) {
                extendStatics = Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
                    function (d, b) { for (var p in b)
                        if (b.hasOwnProperty(p))
                            d[p] = b[p]; };
                return extendStatics(d, b);
            };
            function __extends(d, b) {
                extendStatics(d, b);
                function __() { this.constructor = d; }
                d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
            }
            var __assign = function () {
                __assign = Object.assign || function __assign(t) {
                    for (var s, i = 1, n = arguments.length; i < n; i++) {
                        s = arguments[i];
                        for (var p in s)
                            if (Object.prototype.hasOwnProperty.call(s, p))
                                t[p] = s[p];
                    }
                    return t;
                };
                return __assign.apply(this, arguments);
            };
            function __rest(s, e) {
                var t = {};
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                        t[p] = s[p];
                if (s != null && typeof Object.getOwnPropertySymbols === "function")
                    for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                            t[p[i]] = s[p[i]];
                    }
                return t;
            }
            function __decorate(decorators, target, key, desc) {
                var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
                if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
                    r = Reflect.decorate(decorators, target, key, desc);
                else
                    for (var i = decorators.length - 1; i >= 0; i--)
                        if (d = decorators[i])
                            r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
                return c > 3 && r && Object.defineProperty(target, key, r), r;
            }
            function __param(paramIndex, decorator) {
                return function (target, key) { decorator(target, key, paramIndex); };
            }
            function __metadata(metadataKey, metadataValue) {
                if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
                    return Reflect.metadata(metadataKey, metadataValue);
            }
            function __awaiter(thisArg, _arguments, P, generator) {
                return new (P || (P = Promise))(function (resolve, reject) {
                    function fulfilled(value) { try {
                        step(generator.next(value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function rejected(value) { try {
                        step(generator["throw"](value));
                    }
                    catch (e) {
                        reject(e);
                    } }
                    function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
                    step((generator = generator.apply(thisArg, _arguments || [])).next());
                });
            }
            function __generator(thisArg, body) {
                var _ = { label: 0, sent: function () { if (t[0] & 1)
                        throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
                return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
                function verb(n) { return function (v) { return step([n, v]); }; }
                function step(op) {
                    if (f)
                        throw new TypeError("Generator is already executing.");
                    while (_)
                        try {
                            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                                return t;
                            if (y = 0, t)
                                op = [op[0] & 2, t.value];
                            switch (op[0]) {
                                case 0:
                                case 1:
                                    t = op;
                                    break;
                                case 4:
                                    _.label++;
                                    return { value: op[1], done: false };
                                case 5:
                                    _.label++;
                                    y = op[1];
                                    op = [0];
                                    continue;
                                case 7:
                                    op = _.ops.pop();
                                    _.trys.pop();
                                    continue;
                                default:
                                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                        _ = 0;
                                        continue;
                                    }
                                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                        _.label = op[1];
                                        break;
                                    }
                                    if (op[0] === 6 && _.label < t[1]) {
                                        _.label = t[1];
                                        t = op;
                                        break;
                                    }
                                    if (t && _.label < t[2]) {
                                        _.label = t[2];
                                        _.ops.push(op);
                                        break;
                                    }
                                    if (t[2])
                                        _.ops.pop();
                                    _.trys.pop();
                                    continue;
                            }
                            op = body.call(thisArg, _);
                        }
                        catch (e) {
                            op = [6, e];
                            y = 0;
                        }
                        finally {
                            f = t = 0;
                        }
                    if (op[0] & 5)
                        throw op[1];
                    return { value: op[0] ? op[1] : void 0, done: true };
                }
            }
            function __exportStar(m, exports) {
                for (var p in m)
                    if (!exports.hasOwnProperty(p))
                        exports[p] = m[p];
            }
            function __values(o) {
                var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
                if (m)
                    return m.call(o);
                return {
                    next: function () {
                        if (o && i >= o.length)
                            o = void 0;
                        return { value: o && o[i++], done: !o };
                    }
                };
            }
            function __read(o, n) {
                var m = typeof Symbol === "function" && o[Symbol.iterator];
                if (!m)
                    return o;
                var i = m.call(o), r, ar = [], e;
                try {
                    while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                        ar.push(r.value);
                }
                catch (error) {
                    e = { error: error };
                }
                finally {
                    try {
                        if (r && !r.done && (m = i["return"]))
                            m.call(i);
                    }
                    finally {
                        if (e)
                            throw e.error;
                    }
                }
                return ar;
            }
            function __spread() {
                for (var ar = [], i = 0; i < arguments.length; i++)
                    ar = ar.concat(__read(arguments[i]));
                return ar;
            }
            function __spreadArrays() {
                for (var s = 0, i = 0, il = arguments.length; i < il; i++)
                    s += arguments[i].length;
                for (var r = Array(s), k = 0, i = 0; i < il; i++)
                    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                        r[k] = a[j];
                return r;
            }
            ;
            function __await(v) {
                return this instanceof __await ? (this.v = v, this) : new __await(v);
            }
            function __asyncGenerator(thisArg, _arguments, generator) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var g = generator.apply(thisArg, _arguments || []), i, q = [];
                return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
                function verb(n) { if (g[n])
                    i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
                function resume(n, v) { try {
                    step(g[n](v));
                }
                catch (e) {
                    settle(q[0][3], e);
                } }
                function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
                function fulfill(value) { resume("next", value); }
                function reject(value) { resume("throw", value); }
                function settle(f, v) { if (f(v), q.shift(), q.length)
                    resume(q[0][0], q[0][1]); }
            }
            function __asyncDelegator(o) {
                var i, p;
                return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
                function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
            }
            function __asyncValues(o) {
                if (!Symbol.asyncIterator)
                    throw new TypeError("Symbol.asyncIterator is not defined.");
                var m = o[Symbol.asyncIterator], i;
                return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
                function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
                function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
            }
            function __makeTemplateObject(cooked, raw) {
                if (Object.defineProperty) {
                    Object.defineProperty(cooked, "raw", { value: raw });
                }
                else {
                    cooked.raw = raw;
                }
                return cooked;
            }
            ;
            function __importStar(mod) {
                if (mod && mod.__esModule)
                    return mod;
                var result = {};
                if (mod != null)
                    for (var k in mod)
                        if (Object.hasOwnProperty.call(mod, k))
                            result[k] = mod[k];
                result.default = mod;
                return result;
            }
            function __importDefault(mod) {
                return (mod && mod.__esModule) ? mod : { default: mod };
            }
            /***/ 
        }),
        /***/ "./src/app/animations.ts": 
        /*!*******************************!*\
          !*** ./src/app/animations.ts ***!
          \*******************************/
        /*! exports provided: slideInAnimation */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInAnimation", function () { return slideInAnimation; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
            var slideInAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('routeAnimations', [
                /**
                 * HOME ANIMATION
                 */
                //  1-2 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ContactsPage => WelcomePage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('WelcomePage => AboutPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('AboutPage => WelcomePage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  2-3 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('AboutPage => MapPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('MapPage => AboutPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  3-4 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('MapPage => ProjectsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ProjectsPage => MapPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  4-5 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ProjectsPage => NewsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('NewsPage => ProjectsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  5-6 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('NewsPage => CharityrPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('CharityrPage => NewsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  6-7 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('CharityrPage => ArchiverPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ArchiverPage => CharityrPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //  7-8 block
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ArchiverPage => ContactsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('ContactsPage => ArchiverPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                /// 1-8 connection
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('WelcomePage =>  ContactsPage', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ top: '-100%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //About-inner
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageAbout', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageAbout => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //project-inner
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageProject', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageProject => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //charity-inner
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageCharity', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageCharity => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //contacts-inner
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageContacts', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageContacts => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //contacts-inner
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageNews', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageNews => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ left: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                ///////////////////////////
                //news-event
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageNews-event', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageNews-event => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                //news-event
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* => innerPageProject-id', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('innerPageProject-id => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ position: 'relative' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter, :leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            width: '100%',
                            height: "100vh",
                            zIndex: 10,
                        })
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '-100%', zIndex: 11 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '0%' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ right: '100%' })),
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animateChild"])()),
                ]),
            ]);
            /***/ 
        }),
        /***/ "./src/app/app-routing.module.ts": 
        /*!***************************************!*\
          !*** ./src/app/app-routing.module.ts ***!
          \***************************************/
        /*! exports provided: AppRoutingModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () { return AppRoutingModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            var routes = [];
            var AppRoutingModule = /** @class */ (function () {
                function AppRoutingModule() {
                }
                return AppRoutingModule;
            }());
            AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
                    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
                })
            ], AppRoutingModule);
            /***/ 
        }),
        /***/ "./src/app/app.component.scss": 
        /*!************************************!*\
          !*** ./src/app/app.component.scss ***!
          \************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("p {\n  font-size: 18px;\n}\n\nhtml {\n  overflow: hidden;\n}\n\n.global__container {\n  display: block;\n  height: 100vh;\n}\n\n.global__container .global__container__menu {\n  display: inline-block;\n  text-align: left;\n  width: 15%;\n  height: 100%;\n  transition: all 0.3s;\n}\n\n.global__container .global__container__content {\n  transition: all 0.3s;\n  position: relative;\n  display: inline-block;\n  width: 85%;\n  overflow: hidden;\n}\n\n.fotorama__thumb-border {\n  border-width: 0px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNBLGVBQUE7QUNBQTs7QURHQTtFQUNDLGdCQUFBO0FDQUQ7O0FER0E7RUFDSSxjQUFBO0VBRUEsYUFBQTtBQ0RKOztBREVJO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUNBUjs7QURJSTtFQUNJLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQ0ZSOztBRFdDO0VBQ0csNEJBQUE7QUNSSiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5we1xyXG5mb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmdsb2JhbF9fY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICAuZ2xvYmFsX19jb250YWluZXJfX21lbnUge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgLy8gYm94LXNoYWRvdzogMCAwIDhweCByZ2JhKDAsIDAsIDAsIDEpO1xyXG4gICAgICAgIC8vIGZpbHRlcjogZHJvcC1zaGFkb3coMHB4IDBweCA0cHggcmdiYSgwLCAwLCAwLCAwLjM1KSk7XHJcbiAgICB9XHJcbiAgICAuZ2xvYmFsX19jb250YWluZXJfX2NvbnRlbnQge1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICB3aWR0aDogODUlO1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB9XHJcblxyXG4gICBcclxuICAgXHJcblxyXG4gICBcclxuIH1cclxuXHJcbiAuZm90b3JhbWFfX3RodW1iLWJvcmRlcntcclxuICAgIGJvcmRlci13aWR0aDogMHB4ICFpbXBvcnRhbnQ7XHJcbiB9XHJcblxyXG4gXHJcbiIsInAge1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbmh0bWwge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZ2xvYmFsX19jb250YWluZXIge1xuICBkaXNwbGF5OiBibG9jaztcbiAgaGVpZ2h0OiAxMDB2aDtcbn1cbi5nbG9iYWxfX2NvbnRhaW5lciAuZ2xvYmFsX19jb250YWluZXJfX21lbnUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHdpZHRoOiAxNSU7XG4gIGhlaWdodDogMTAwJTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG59XG4uZ2xvYmFsX19jb250YWluZXIgLmdsb2JhbF9fY29udGFpbmVyX19jb250ZW50IHtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogODUlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZm90b3JhbWFfX3RodW1iLWJvcmRlciB7XG4gIGJvcmRlci13aWR0aDogMHB4ICFpbXBvcnRhbnQ7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/app.component.ts": 
        /*!**********************************!*\
          !*** ./src/app/app.component.ts ***!
          \**********************************/
        /*! exports provided: AppComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function () { return AppComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var src_app_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/animations */ "./src/app/animations.ts");
            var AppComponent = /** @class */ (function () {
                function AppComponent() {
                    this.title = 'caritas';
                }
                AppComponent.prototype.ngOnInit = function () {
                };
                AppComponent.prototype.ngAfterContentInit = function () {
                    $('.fotorama').fotorama();
                };
                AppComponent.prototype.prepareRoute = function (outlet) {
                    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
                };
                return AppComponent;
            }());
            AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-root',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
                    animations: [
                        src_app_animations__WEBPACK_IMPORTED_MODULE_2__["slideInAnimation"],
                    ],
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
                })
            ], AppComponent);
            var CountClicks = /** @class */ (function () {
                function CountClicks() {
                    this.numberOfClicks = 0;
                }
                CountClicks.prototype.onClick = function (btn) {
                    console.log('button', btn, 'number of clicks:', this.numberOfClicks++);
                };
                return CountClicks;
            }());
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('wheel', ['$event.target'])
            ], CountClicks.prototype, "onClick", null);
            CountClicks = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({ selector: 'button[counting]' })
            ], CountClicks);
            /***/ 
        }),
        /***/ "./src/app/app.module.ts": 
        /*!*******************************!*\
          !*** ./src/app/app.module.ts ***!
          \*******************************/
        /*! exports provided: CustomRouterStateSerializer, AppModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomRouterStateSerializer", function () { return CustomRouterStateSerializer; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function () { return AppModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
            /* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
            /* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
            /* harmony import */ var _modules_home_home_modules__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/home/home.modules */ "./src/app/modules/home/home.modules.ts");
            /* harmony import */ var _shared_shared_modules__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/shared.modules */ "./src/app/shared/shared.modules.ts");
            /* harmony import */ var _modules_home_components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modules/home/components/welcome/welcome.component */ "./src/app/modules/home/components/welcome/welcome.component.ts");
            /* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
            /* harmony import */ var _modules_home_components_about_about_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./modules/home/components/about/about.component */ "./src/app/modules/home/components/about/about.component.ts");
            /* harmony import */ var _modules_home_components_about_about_inner_about_inner_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./modules/home/components/about/about-inner/about-inner.component */ "./src/app/modules/home/components/about/about-inner/about-inner.component.ts");
            /* harmony import */ var _modules_home_components_map_map_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./modules/home/components/map/map.component */ "./src/app/modules/home/components/map/map.component.ts");
            /* harmony import */ var _modules_home_components_projects_projects_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./modules/home/components/projects/projects.component */ "./src/app/modules/home/components/projects/projects.component.ts");
            /* harmony import */ var _modules_home_components_news_news_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./modules/home/components/news/news.component */ "./src/app/modules/home/components/news/news.component.ts");
            /* harmony import */ var _modules_home_components_charity_charity_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./modules/home/components/charity/charity.component */ "./src/app/modules/home/components/charity/charity.component.ts");
            /* harmony import */ var _modules_home_components_archive_archive_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./modules/home/components/archive/archive.component */ "./src/app/modules/home/components/archive/archive.component.ts");
            /* harmony import */ var _modules_home_components_contacts_contacts_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./modules/home/components/contacts/contacts.component */ "./src/app/modules/home/components/contacts/contacts.component.ts");
            /* harmony import */ var _shared_components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./shared/components/navigation/navigation.component */ "./src/app/shared/components/navigation/navigation.component.ts");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _shared_components_error404_error404_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/components/error404/error404.component */ "./src/app/shared/components/error404/error404.component.ts");
            /* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
            /* harmony import */ var ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-slick-carousel */ "./node_modules/ngx-slick-carousel/fesm2015/ngx-slick-carousel.js");
            /* harmony import */ var _modules_home_components_projects_projects_inner_projects_inner_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./modules/home/components/projects/projects-inner/projects-inner.component */ "./src/app/modules/home/components/projects/projects-inner/projects-inner.component.ts");
            /* harmony import */ var _modules_home_components_charity_charity_inner_charity_inner_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./modules/home/components/charity/charity-inner/charity-inner.component */ "./src/app/modules/home/components/charity/charity-inner/charity-inner.component.ts");
            /* harmony import */ var _modules_home_components_news_news_inner_news_inner_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./modules/home/components/news/news-inner/news-inner.component */ "./src/app/modules/home/components/news/news-inner/news-inner.component.ts");
            /* harmony import */ var _modules_home_components_contacts_contacts_inner_contacts_inner_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./modules/home/components/contacts/contacts-inner/contacts-inner.component */ "./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.ts");
            /* harmony import */ var _ngu_carousel__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @ngu/carousel */ "./node_modules/@ngu/carousel/fesm2015/ngu-carousel.js");
            /* harmony import */ var _modules_home_components_projects_projects_inner_project_project_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./modules/home/components/projects/projects-inner/project/project.component */ "./src/app/modules/home/components/projects/projects-inner/project/project.component.ts");
            /* harmony import */ var _modules_home_components_news_news_inner_news_event_news_event_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./modules/home/components/news/news-inner/news-event/news-event.component */ "./src/app/modules/home/components/news/news-inner/news-event/news-event.component.ts");
            /* harmony import */ var _wheel_directive__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./wheel.directive */ "./src/app/wheel.directive.ts");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            /* harmony import */ var _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @ngxs/router-plugin */ "./node_modules/@ngxs/router-plugin/fesm2015/ngxs-router-plugin.js");
            //NGXS 
            var appRoutes = [
                {
                    path: '',
                    component: _modules_home_components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_8__["WelcomeComponent"],
                    data: { animation: 'WelcomePage' }
                },
                {
                    path: 'about',
                    component: _modules_home_components_about_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"],
                    data: { animation: 'AboutPage' }
                },
                {
                    path: 'map',
                    component: _modules_home_components_map_map_component__WEBPACK_IMPORTED_MODULE_12__["MapComponent"],
                    data: { animation: 'MapPage' }
                },
                {
                    path: 'projects',
                    component: _modules_home_components_projects_projects_component__WEBPACK_IMPORTED_MODULE_13__["ProjectsComponent"],
                    data: { animation: 'ProjectsPage' }
                },
                {
                    path: 'news',
                    component: _modules_home_components_news_news_component__WEBPACK_IMPORTED_MODULE_14__["NewsComponent"],
                    data: { animation: 'NewsPage' }
                },
                {
                    path: 'charity',
                    component: _modules_home_components_charity_charity_component__WEBPACK_IMPORTED_MODULE_15__["CharityComponent"],
                    data: { animation: 'CharityrPage' }
                },
                {
                    path: 'archive',
                    component: _modules_home_components_archive_archive_component__WEBPACK_IMPORTED_MODULE_16__["ArchiveComponent"],
                    data: { animation: 'ArchiverPage' }
                },
                {
                    path: 'contacts',
                    component: _modules_home_components_contacts_contacts_component__WEBPACK_IMPORTED_MODULE_17__["ContactsComponent"],
                    data: { animation: 'ContactsPage' }
                },
                //inner pages
                {
                    path: 'about/about-inner',
                    component: _modules_home_components_about_about_inner_about_inner_component__WEBPACK_IMPORTED_MODULE_11__["AboutInnerComponent"],
                    data: { animation: 'innerPageAbout' }
                },
                {
                    path: 'projects/projects-inner',
                    component: _modules_home_components_projects_projects_inner_projects_inner_component__WEBPACK_IMPORTED_MODULE_23__["ProjectsInnerComponent"],
                    data: { animation: 'innerPageProject' }
                },
                {
                    path: 'projects/projects-inner/project',
                    component: _modules_home_components_projects_projects_inner_project_project_component__WEBPACK_IMPORTED_MODULE_28__["ProjectComponent"],
                    data: { animation: 'innerPageProject-id' }
                },
                {
                    path: 'charity/charity-inner',
                    component: _modules_home_components_charity_charity_inner_charity_inner_component__WEBPACK_IMPORTED_MODULE_24__["CharityInnerComponent"],
                    data: { animation: 'innerPageCharity' }
                },
                {
                    path: 'contacts/contacts-inner',
                    component: _modules_home_components_contacts_contacts_inner_contacts_inner_component__WEBPACK_IMPORTED_MODULE_26__["ContactsInnerComponent"],
                    data: { animation: 'innerPageContacts' }
                },
                {
                    path: 'news/news-inner',
                    component: _modules_home_components_news_news_inner_news_inner_component__WEBPACK_IMPORTED_MODULE_25__["NewsInnerComponent"],
                    data: { animation: 'innerPageNews' }
                },
                {
                    path: 'news/news-inner/news-event',
                    component: _modules_home_components_news_news_inner_news_event_news_event_component__WEBPACK_IMPORTED_MODULE_29__["NewsEventComponent"],
                    data: { animation: 'innerPageNews-event' }
                },
                { path: '404', component: _shared_components_error404_error404_component__WEBPACK_IMPORTED_MODULE_20__["Error404Component"], data: { animation: 'ContactsPage' } },
                { path: '**', redirectTo: "404", data: { animation: 'FilterPage' } }
            ];
            // Map the router snapshot to { url, params, queryParams }
            var CustomRouterStateSerializer = /** @class */ (function () {
                function CustomRouterStateSerializer() {
                }
                CustomRouterStateSerializer.prototype.serialize = function (routerState) {
                    var url = routerState.url, queryParams = routerState.root.queryParams;
                    var route = routerState.root;
                    while (route.firstChild) {
                        route = route.firstChild;
                    }
                    var params = route.params;
                    return { url: url, params: params, queryParams: queryParams };
                };
                return CustomRouterStateSerializer;
            }());
            var AppModule = /** @class */ (function () {
                function AppModule() {
                }
                return AppModule;
            }());
            AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
                    declarations: [
                        _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                        _modules_home_components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_8__["WelcomeComponent"],
                        _modules_home_components_about_about_component__WEBPACK_IMPORTED_MODULE_10__["AboutComponent"],
                        _modules_home_components_map_map_component__WEBPACK_IMPORTED_MODULE_12__["MapComponent"],
                        _modules_home_components_projects_projects_component__WEBPACK_IMPORTED_MODULE_13__["ProjectsComponent"],
                        _modules_home_components_news_news_component__WEBPACK_IMPORTED_MODULE_14__["NewsComponent"],
                        _modules_home_components_charity_charity_component__WEBPACK_IMPORTED_MODULE_15__["CharityComponent"],
                        _modules_home_components_archive_archive_component__WEBPACK_IMPORTED_MODULE_16__["ArchiveComponent"],
                        _modules_home_components_contacts_contacts_component__WEBPACK_IMPORTED_MODULE_17__["ContactsComponent"],
                        _shared_components_navigation_navigation_component__WEBPACK_IMPORTED_MODULE_18__["NavigationComponent"],
                        _shared_components_error404_error404_component__WEBPACK_IMPORTED_MODULE_20__["Error404Component"],
                        _modules_home_components_about_about_inner_about_inner_component__WEBPACK_IMPORTED_MODULE_11__["AboutInnerComponent"],
                        _modules_home_components_projects_projects_inner_projects_inner_component__WEBPACK_IMPORTED_MODULE_23__["ProjectsInnerComponent"],
                        _modules_home_components_charity_charity_inner_charity_inner_component__WEBPACK_IMPORTED_MODULE_24__["CharityInnerComponent"],
                        _modules_home_components_news_news_inner_news_inner_component__WEBPACK_IMPORTED_MODULE_25__["NewsInnerComponent"],
                        _modules_home_components_contacts_contacts_inner_contacts_inner_component__WEBPACK_IMPORTED_MODULE_26__["ContactsInnerComponent"],
                        _modules_home_components_projects_projects_inner_project_project_component__WEBPACK_IMPORTED_MODULE_28__["ProjectComponent"],
                        _modules_home_components_news_news_inner_news_event_news_event_component__WEBPACK_IMPORTED_MODULE_29__["NewsEventComponent"],
                        _wheel_directive__WEBPACK_IMPORTED_MODULE_30__["InputMagicWheelDirective"]
                    ],
                    imports: [
                        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                        _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                        _modules_home_home_modules__WEBPACK_IMPORTED_MODULE_6__["HomeModule"],
                        _shared_shared_modules__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__["BrowserAnimationsModule"],
                        ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_22__["SlickCarouselModule"],
                        _ngu_carousel__WEBPACK_IMPORTED_MODULE_27__["NguCarouselModule"],
                        _angular_router__WEBPACK_IMPORTED_MODULE_19__["RouterModule"].forRoot(appRoutes),
                        _ngxs_store__WEBPACK_IMPORTED_MODULE_31__["NgxsModule"].forRoot([]),
                        _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_32__["NgxsRouterPluginModule"].forRoot()
                    ],
                    providers: [{ provide: _ngxs_router_plugin__WEBPACK_IMPORTED_MODULE_32__["RouterStateSerializer"], useClass: CustomRouterStateSerializer }],
                    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
                })
            ], AppModule);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/about/about-inner/about-inner.component.scss": 
        /*!**************************************************************************************!*\
          !*** ./src/app/modules/home/components/about/about-inner/about-inner.component.scss ***!
          \**************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".about-container {\n  padding: 0 5%;\n  background: #fff;\n}\n\n.about-container__title {\n  margin-left: 1rem;\n}\n\n.about-container__title a {\n  color: inherit;\n}\n\n.about-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 2rem;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 42px;\n  margin-bottom: 0;\n  color: inherit;\n}\n\n.about-container__title p {\n  color: #bfbfbf;\n  padding-left: 1rem;\n}\n\n.about-container__title img {\n  width: auto;\n}\n\n.about-container__gallery img {\n  width: 100%;\n}\n\n.title__header {\n  margin-left: 1rem;\n  margin-right: 2rem;\n}\n\n.title__text {\n  margin-left: 1rem;\n  margin-right: 2rem;\n  margin-bottom: 2rem;\n}\n\n.body__text {\n  margin-left: 0rem;\n  margin-right: 2rem;\n  margin-top: 2rem;\n}\n\n.body__title {\n  margin-bottom: 3rem;\n}\n\n.vision-image {\n  position: absolute;\n  display: inline-block;\n  width: 100%;\n  height: 528px;\n}\n\n.container-fluid {\n  padding: 0 1rem;\n}\n\n.charity-container {\n  color: #252539;\n  background: linear-gradient(0deg, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0.3) 100%), url(\"/assets/images/charity/charity-bg.png\"), #fff;\n  background-size: cover;\n  position: relative;\n  overflow: hidden;\n  padding: 7% 0;\n}\n\n.charity-container .charity-header {\n  width: 100%;\n  height: 10%;\n  text-transform: uppercase;\n  text-align: center;\n  position: relative;\n}\n\n.charity-container .charity-header span {\n  position: relative;\n}\n\n.charity-container .charity-header h2 {\n  display: inline-block;\n  z-index: 1;\n}\n\n.charity-container .charity-header h2::after {\n  content: \"\";\n  display: inline-block;\n  position: absolute;\n  width: 96%;\n  height: 10px;\n  background: red;\n  right: -17px;\n  bottom: 2px;\n  z-index: 0;\n}\n\n.charity-container .charity-container__option {\n  position: relative;\n  border-radius: 7px;\n  padding: 1rem;\n  margin: 10% 0;\n}\n\n.charity-container .charity-container__option:first-child {\n  background: transparent;\n  margin: 0;\n}\n\n.charity-container .charity-container__option:last-child {\n  background: #000;\n  color: #fff;\n}\n\n.charity-container .charity-container__option:last-child::after {\n  content: \"\";\n  display: inline-block;\n  background: #000;\n  width: 15px;\n  height: 15px;\n  position: absolute;\n  right: -15px;\n  top: 20px;\n  -webkit-clip-path: polygon(0 100%, 0 0, 100% 0);\n          clip-path: polygon(0 100%, 0 0, 100% 0);\n}\n\n.charity-container .charity-container__wrapper {\n  position: absolute;\n  height: 420px;\n  bottom: 0;\n  right: 2%;\n  background: url(\"/assets/images/charity/charity-phone.png\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-position-y: 0;\n  background-size: cover;\n  padding-top: 11%;\n  max-width: 430px;\n}\n\n.charity-container .charity-container__wrapper .container-phone {\n  width: 85%;\n  margin: auto;\n  text-align: center;\n}\n\n.charity-container .charity-container__wrapper .container-phone a {\n  display: inline-block;\n  color: #252539;\n  padding: 1rem 1.5rem;\n  width: 100%;\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);\n  border-radius: 9px;\n  margin: 0.5rem 0;\n  transition: all 0.3s;\n}\n\n.charity-container .charity-container__wrapper .container-phone a:hover {\n  background: red;\n  color: #fff;\n  text-decoration: none;\n}\n\n.charity-container .charity-container__wrapper .container-phone input {\n  width: 100%;\n  border: none;\n  background: #f4f4f7;\n  height: 3rem;\n  padding: 0 1rem;\n  border: none;\n}\n\n.charity-container .charity-container__wrapper .container-phone input:focus {\n  outline: none;\n}\n\n.charity-container .charity-container__charity-phone {\n  position: absolute;\n  height: 60%;\n  bottom: 0;\n  right: 10%;\n}\n\n.wrapper-mission {\n  background: url(\"/assets/images/about/about-inner/mission-bg.png\") no-repeat, #f8f8f8;\n  background-position-x: 80%;\n  background-position-y: 0rem;\n  background-size: content;\n  padding: 8% 5%;\n  position: relative;\n  overflow: hidden;\n  z-index: 1;\n}\n\n.wrapper-mission a {\n  text-transform: uppercase;\n  padding: 0.5rem 2.5rem;\n  border: 1px solid #000000;\n  border-radius: 5px;\n  display: inline-block;\n  color: inherit;\n}\n\n.wrapper-mission p {\n  margin-bottom: 5%;\n}\n\n.wrapper-mission img {\n  position: absolute;\n}\n\n.wrapper-mission img:nth-child(1) {\n  left: -70px;\n  -webkit-animation: ballon1 infinite 9s 1s ease-in-out;\n          animation: ballon1 infinite 9s 1s ease-in-out;\n  z-index: -1;\n  top: 20%;\n}\n\n.wrapper-mission img:nth-child(2) {\n  top: 20%;\n  right: -70px;\n  -webkit-animation: ballon2 infinite 9s 2s ease-in-out;\n          animation: ballon2 infinite 9s 2s ease-in-out;\n  z-index: -1;\n}\n\n@-webkit-keyframes ballon1 {\n  0% {\n    top: 20%;\n  }\n  25% {\n    top: 0%;\n  }\n  50% {\n    top: 20%;\n  }\n  75% {\n    top: 0%;\n  }\n  100% {\n    top: 20%;\n  }\n}\n\n@keyframes ballon1 {\n  0% {\n    top: 20%;\n  }\n  25% {\n    top: 0%;\n  }\n  50% {\n    top: 20%;\n  }\n  75% {\n    top: 0%;\n  }\n  100% {\n    top: 20%;\n  }\n}\n\n@-webkit-keyframes ballon2 {\n  0% {\n    top: 20%;\n  }\n  25% {\n    top: 0%;\n  }\n  50% {\n    top: 20%;\n  }\n  75% {\n    top: 0%;\n  }\n  100% {\n    top: 20%;\n  }\n}\n\n@keyframes ballon2 {\n  0% {\n    top: 20%;\n  }\n  25% {\n    top: 0%;\n  }\n  50% {\n    top: 20%;\n  }\n  75% {\n    top: 0%;\n  }\n  100% {\n    top: 20%;\n  }\n}\n\n.wrapper-vision {\n  position: relative;\n  display: block;\n  height: 528px;\n  background: url(\"/assets/images/about/about-inner/vision.png\") no-repeat;\n  margin-bottom: 10%;\n}\n\n.wrapper-vision .absolute-wrapper__note {\n  width: 31%;\n  background: #fff;\n  position: absolute;\n  bottom: -11%;\n  left: -5%;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);\n  padding: 3%;\n}\n\n.wrapper-vision .absolute-wrapper__note h2 {\n  color: #d4002d;\n}\n\n.facts {\n  width: 100%;\n  padding: 2% 0;\n  position: relative;\n}\n\n.facts p {\n  margin-bottom: 0;\n  padding: 0 10%;\n  text-align: center;\n  font-size: 22px;\n  font-weight: bold;\n}\n\n.facts p::after {\n  content: \"\";\n  display: inline-block;\n  width: 35px;\n  height: 35px;\n  background: url(\"/assets/images/quoutes.svg\");\n  background-size: contain;\n  transform: rotate(180deg);\n  position: absolute;\n  top: 10px;\n  left: 40px;\n}\n\n.facts p::before {\n  content: \"\";\n  display: inline-block;\n  width: 35px;\n  height: 35px;\n  background: url(\"/assets/images/quoutes.svg\");\n  background-size: contain;\n  position: absolute;\n  bottom: 10px;\n  right: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvYWJvdXQvYWJvdXQtaW5uZXIvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxhYm91dFxcYWJvdXQtaW5uZXJcXGFib3V0LWlubmVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9hYm91dC9hYm91dC1pbm5lci9hYm91dC1pbm5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFVQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtBQ1RKOztBRFlBO0VBQ0ksaUJBQUE7QUNUSjs7QURXSTtFQUNJLGNBQUE7QUNUUjs7QURZSTtFQUNJLGlCQUFBO0VBQ0EseURBQUE7RUFDQSwyQkFBQTtFQUNBLDBCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDVlI7O0FEYUk7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUNYUjs7QURjSTtFQUNJLFdBQUE7QUNaUjs7QURrQkk7RUFDSSxXQUFBO0FDZlI7O0FEbUJBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ2hCSjs7QURtQkE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNoQko7O0FEbUJBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDaEJKOztBRG9CQTtFQUNJLG1CQUFBO0FDakJKOztBRG9CQTtFQUNJLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQ2pCSjs7QURvQkE7RUFDSSxlQUFBO0FDakJKOztBRG9CQTtFQUNJLGNBQUE7RUFDQSxxSUFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QUNqQko7O0FEbUJJO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUNqQlI7O0FEbUJRO0VBQ0ksa0JBQUE7QUNqQlo7O0FEc0JRO0VBQ0kscUJBQUE7RUFDQSxVQUFBO0FDcEJaOztBRHNCWTtFQUNJLFdBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDcEJoQjs7QUR5Qkk7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7QUN2QlI7O0FEeUJRO0VBQ0ksdUJBQUE7RUFDQSxTQUFBO0FDdkJaOztBRDBCUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtBQ3hCWjs7QUQwQlk7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLCtDQUFBO1VBQUEsdUNBQUE7QUN4QmhCOztBRDZCSTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsMkRBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esd0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUMzQlI7O0FENkJRO0VBQ0ksVUFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQzVCWjs7QUQ4Qlk7RUFDSSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFFQSx1Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQzVCaEI7O0FEOEJnQjtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7QUM1QnBCOztBRGdDWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUM5QmhCOztBRGdDZ0I7RUFDSSxhQUFBO0FDOUJwQjs7QURvQ0k7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQ2xDUjs7QUR3Q0E7RUFDSSxxRkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtBQ3JDSjs7QUR1Q0k7RUFDSSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQ3JDUjs7QUR3Q0k7RUFDSSxpQkFBQTtBQ3RDUjs7QUR5Q0k7RUFDSSxrQkFBQTtBQ3ZDUjs7QUR5Q1E7RUFDSSxXQUFBO0VBQ0EscURBQUE7VUFBQSw2Q0FBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDdkNaOztBRDBDUTtFQUNJLFFBQUE7RUFDQSxZQUFBO0VBQ0EscURBQUE7VUFBQSw2Q0FBQTtFQUNBLFdBQUE7QUN4Q1o7O0FENkNBO0VBQ0k7SUFDSSxRQUFBO0VDMUNOO0VENkNFO0lBQ0ksT0FBQTtFQzNDTjtFRDhDRTtJQUNJLFFBQUE7RUM1Q047RUQrQ0U7SUFDSSxPQUFBO0VDN0NOO0VEZ0RFO0lBQ0ksUUFBQTtFQzlDTjtBQUNGOztBRDJCQTtFQUNJO0lBQ0ksUUFBQTtFQzFDTjtFRDZDRTtJQUNJLE9BQUE7RUMzQ047RUQ4Q0U7SUFDSSxRQUFBO0VDNUNOO0VEK0NFO0lBQ0ksT0FBQTtFQzdDTjtFRGdERTtJQUNJLFFBQUE7RUM5Q047QUFDRjs7QURpREE7RUFDSTtJQUNJLFFBQUE7RUMvQ047RURrREU7SUFDSSxPQUFBO0VDaEROO0VEbURFO0lBQ0ksUUFBQTtFQ2pETjtFRG9ERTtJQUNJLE9BQUE7RUNsRE47RURxREU7SUFDSSxRQUFBO0VDbkROO0FBQ0Y7O0FEZ0NBO0VBQ0k7SUFDSSxRQUFBO0VDL0NOO0VEa0RFO0lBQ0ksT0FBQTtFQ2hETjtFRG1ERTtJQUNJLFFBQUE7RUNqRE47RURvREU7SUFDSSxPQUFBO0VDbEROO0VEcURFO0lBQ0ksUUFBQTtFQ25ETjtBQUNGOztBRHNEQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7RUFDQSx3RUFBQTtFQUNBLGtCQUFBO0FDcERKOztBRHNESTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSx1Q0FBQTtFQUNBLFdBQUE7QUNwRFI7O0FEc0RRO0VBQ0ksY0FBQTtBQ3BEWjs7QUR5REE7RUFFSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FDdkRKOztBRHlESTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDdkRSOztBRHlEUTtFQUNJLFdBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsNkNBQUE7RUFDQSx3QkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQ3ZEWjs7QUQwRFE7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDZDQUFBO0VBQ0Esd0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FDeERaIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvYWJvdXQvYWJvdXQtaW5uZXIvYWJvdXQtaW5uZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkY2hhcml0eS1waG9uZTogXCIvYXNzZXRzL2ltYWdlcy9jaGFyaXR5L2NoYXJpdHktcGhvbmUucG5nXCI7XHJcbiRjaGFyaXR5LWJnOiAnL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LWJnLnBuZyc7XHJcbiR1bmRlcmxpbmU6ICcvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUucG5nJztcclxuXHJcbiRxdW91dGVzOiAnL2Fzc2V0cy9pbWFnZXMvcXVvdXRlcy5zdmcnO1xyXG4kdmlzaW9uOiAnL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtaW5uZXIvdmlzaW9uLnBuZyc7XHJcblxyXG4kZ2lybHM6IFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtaW5uZXIvbWlzc2lvbi1iZy5wbmdcIjtcclxuJGJhbGw6ICcvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1pbm5lci9iYWxsb25zLnBuZyc7XHJcblxyXG4uYWJvdXQtY29udGFpbmVyIHtcclxuICAgIHBhZGRpbmc6IDAgNSU7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG59XHJcblxyXG4uYWJvdXQtY29udGFpbmVyX190aXRsZSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuXHJcbiAgICBhe1xyXG4gICAgICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgfVxyXG5cclxuICAgIGgzIHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMnJlbTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgZm9udC1zaXplOiA0MnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgY29sb3I6IGluaGVyaXQ7XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgICAgY29sb3I6ICNiZmJmYmY7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4uYWJvdXQtY29udGFpbmVyX19nYWxsZXJ5IHtcclxuICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50aXRsZV9faGVhZGVyIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAycmVtO1xyXG59XHJcblxyXG4udGl0bGVfX3RleHQge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDJyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG59XHJcblxyXG4uYm9keV9fdGV4dCB7XHJcbiAgICBtYXJnaW4tbGVmdDogMHJlbTtcclxuICAgIG1hcmdpbi1yaWdodDogMnJlbTtcclxuICAgIG1hcmdpbi10b3A6IDJyZW07XHJcblxyXG59XHJcblxyXG4uYm9keV9fdGl0bGUge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbVxyXG59XHJcblxyXG4udmlzaW9uLWltYWdlIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA1MjhweDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1mbHVpZCB7XHJcbiAgICBwYWRkaW5nOiAwIDFyZW07XHJcbn1cclxuXHJcbi5jaGFyaXR5LWNvbnRhaW5lciB7XHJcbiAgICBjb2xvcjogIzI1MjUzOTtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCByZ2JhKDAsIDAsIDAsIC4zKSAwJSwgcmdiYSgwLCAwLCAwLCAuMykgMTAwJSksIHVybCgkY2hhcml0eS1iZyksICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBhZGRpbmc6IDclIDA7XHJcblxyXG4gICAgLmNoYXJpdHktaGVhZGVyIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwJTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICBcclxuXHJcbiAgICAgICAgaDIge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIHotaW5kZXg6IDE7XHJcblxyXG4gICAgICAgICAgICAmOjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDk2JTtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHJlZDtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAtMTdweDtcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogMnB4O1xyXG4gICAgICAgICAgICAgICAgei1pbmRleDogMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2hhcml0eS1jb250YWluZXJfX29wdGlvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICBwYWRkaW5nOiAxcmVtO1xyXG4gICAgICAgIG1hcmdpbjogMTAlIDA7XHJcblxyXG4gICAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcblxyXG4gICAgICAgICAgICAmOjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IC0xNXB4O1xyXG4gICAgICAgICAgICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgY2xpcC1wYXRoOiBwb2x5Z29uKDAgMTAwJSwgMCAwLCAxMDAlIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGhlaWdodDogNDIwcHg7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OiAyJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJGNoYXJpdHktcGhvbmUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMSU7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA0MzBweDtcclxuXHJcbiAgICAgICAgLmNvbnRhaW5lci1waG9uZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4NSU7XHJcblxyXG4gICAgICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6ICMyNTI1Mzk7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxcmVtIDEuNXJlbTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbiAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA5cHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IC41cmVtIDA7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG5cclxuICAgICAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IHJlZDtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlucHV0IHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2Y0ZjRmNztcclxuICAgICAgICAgICAgICAgIGhlaWdodDogM3JlbTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAgMXJlbTtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuXHJcbiAgICAgICAgICAgICAgICAmOmZvY3VzIHtcclxuICAgICAgICAgICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jaGFyaXR5LWNvbnRhaW5lcl9fY2hhcml0eS1waG9uZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGhlaWdodDogNjAlO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICByaWdodDogMTAlO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcbi53cmFwcGVyLW1pc3Npb24ge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCRnaXJscykgbm8tcmVwZWF0LCAjZjhmOGY4O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiA4MCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDByZW07XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRlbnQ7XHJcbiAgICBwYWRkaW5nOiA4JSA1JTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB6LWluZGV4OiAxO1xyXG5cclxuICAgIGEge1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgcGFkZGluZzogLjVyZW0gMi41cmVtO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBjb2xvcjogaW5oZXJpdDtcclxuICAgIH1cclxuXHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICAgIH1cclxuXHJcbiAgICBpbWcge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHJcbiAgICAgICAgJjpudGgtY2hpbGQoMSkge1xyXG4gICAgICAgICAgICBsZWZ0OiAtNzBweDtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBiYWxsb24xIGluZmluaXRlIDlzIDFzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgICAgICB6LWluZGV4OiAtMTtcclxuICAgICAgICAgICAgdG9wOiAyMCU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAmOm50aC1jaGlsZCgyKSB7XHJcbiAgICAgICAgICAgIHRvcDogMjAlO1xyXG4gICAgICAgICAgICByaWdodDogLTcwcHg7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogYmFsbG9uMiBpbmZpbml0ZSA5cyAycyBlYXNlLWluLW91dDtcclxuICAgICAgICAgICAgei1pbmRleDogLTE7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJhbGxvbjEge1xyXG4gICAgMCUge1xyXG4gICAgICAgIHRvcDogMjAlO1xyXG4gICAgfVxyXG5cclxuICAgIDI1JSB7XHJcbiAgICAgICAgdG9wOiAwJTtcclxuICAgIH1cclxuXHJcbiAgICA1MCUge1xyXG4gICAgICAgIHRvcDogMjAlO1xyXG4gICAgfVxyXG5cclxuICAgIDc1JSB7XHJcbiAgICAgICAgdG9wOiAwJTtcclxuICAgIH1cclxuXHJcbiAgICAxMDAlIHtcclxuICAgICAgICB0b3A6IDIwJTtcclxuICAgIH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBiYWxsb24yIHtcclxuICAgIDAlIHtcclxuICAgICAgICB0b3A6IDIwJTtcclxuICAgIH1cclxuXHJcbiAgICAyNSUge1xyXG4gICAgICAgIHRvcDogMCU7XHJcbiAgICB9XHJcblxyXG4gICAgNTAlIHtcclxuICAgICAgICB0b3A6IDIwJTtcclxuICAgIH1cclxuXHJcbiAgICA3NSUge1xyXG4gICAgICAgIHRvcDogMCU7XHJcbiAgICB9XHJcblxyXG4gICAgMTAwJSB7XHJcbiAgICAgICAgdG9wOiAyMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi53cmFwcGVyLXZpc2lvbiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNTI4cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJHZpc2lvbikgbm8tcmVwZWF0O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTAlO1xyXG5cclxuICAgIC5hYnNvbHV0ZS13cmFwcGVyX19ub3RlIHtcclxuICAgICAgICB3aWR0aDogMzElO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvdHRvbTogLTExJTtcclxuICAgICAgICBsZWZ0OiAtNSU7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDEwcHggcmdiKDAsIDAsIDAsIDAuMyk7XHJcbiAgICAgICAgcGFkZGluZzogMyU7XHJcblxyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgICAgY29sb3I6ICNkNDAwMmQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uZmFjdHMge1xyXG4gICAgLy8gYmFja2dyb3VuZDogdXJsKCRxdW91dGVzKSAwJSAwJSBuby1yZXBlYXQsIHVybCgkcXVvdXRlcykgMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMiUgMDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICBwIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTAlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcblxyXG4gICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMzVweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHF1b3V0ZXMpO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBsZWZ0OiA0MHB4O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICB3aWR0aDogMzVweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHF1b3V0ZXMpO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgYm90dG9tOiAxMHB4O1xyXG4gICAgICAgICAgICByaWdodDogNDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCIuYWJvdXQtY29udGFpbmVyIHtcbiAgcGFkZGluZzogMCA1JTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLmFib3V0LWNvbnRhaW5lcl9fdGl0bGUge1xuICBtYXJnaW4tbGVmdDogMXJlbTtcbn1cbi5hYm91dC1jb250YWluZXJfX3RpdGxlIGEge1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbi5hYm91dC1jb250YWluZXJfX3RpdGxlIGgzIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDJyZW07XG4gIGJhY2tncm91bmQtcG9zaXRpb24teTogODglO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogNDJweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgY29sb3I6IGluaGVyaXQ7XG59XG4uYWJvdXQtY29udGFpbmVyX190aXRsZSBwIHtcbiAgY29sb3I6ICNiZmJmYmY7XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbn1cbi5hYm91dC1jb250YWluZXJfX3RpdGxlIGltZyB7XG4gIHdpZHRoOiBhdXRvO1xufVxuXG4uYWJvdXQtY29udGFpbmVyX19nYWxsZXJ5IGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udGl0bGVfX2hlYWRlciB7XG4gIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDJyZW07XG59XG5cbi50aXRsZV9fdGV4dCB7XG4gIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDJyZW07XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG59XG5cbi5ib2R5X190ZXh0IHtcbiAgbWFyZ2luLWxlZnQ6IDByZW07XG4gIG1hcmdpbi1yaWdodDogMnJlbTtcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuLmJvZHlfX3RpdGxlIHtcbiAgbWFyZ2luLWJvdHRvbTogM3JlbTtcbn1cblxuLnZpc2lvbi1pbWFnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA1MjhweDtcbn1cblxuLmNvbnRhaW5lci1mbHVpZCB7XG4gIHBhZGRpbmc6IDAgMXJlbTtcbn1cblxuLmNoYXJpdHktY29udGFpbmVyIHtcbiAgY29sb3I6ICMyNTI1Mzk7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCByZ2JhKDAsIDAsIDAsIDAuMykgMCUsIHJnYmEoMCwgMCwgMCwgMC4zKSAxMDAlKSwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LWJnLnBuZ1wiKSwgI2ZmZjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwYWRkaW5nOiA3JSAwO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWhlYWRlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwJTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktaGVhZGVyIHNwYW4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktaGVhZGVyIGgyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB6LWluZGV4OiAxO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWhlYWRlciBoMjo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDk2JTtcbiAgaGVpZ2h0OiAxMHB4O1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIHJpZ2h0OiAtMTdweDtcbiAgYm90dG9tOiAycHg7XG4gIHotaW5kZXg6IDA7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgcGFkZGluZzogMXJlbTtcbiAgbWFyZ2luOiAxMCUgMDtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX29wdGlvbjpmaXJzdC1jaGlsZCB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBtYXJnaW46IDA7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb246bGFzdC1jaGlsZCB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIGNvbG9yOiAjZmZmO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fb3B0aW9uOmxhc3QtY2hpbGQ6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtMTVweDtcbiAgdG9wOiAyMHB4O1xuICBjbGlwLXBhdGg6IHBvbHlnb24oMCAxMDAlLCAwIDAsIDEwMCUgMCk7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDQyMHB4O1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAyJTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LXBob25lLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDA7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBhZGRpbmctdG9wOiAxMSU7XG4gIG1heC13aWR0aDogNDMwcHg7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUge1xuICB3aWR0aDogODUlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIgLmNvbnRhaW5lci1waG9uZSBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogIzI1MjUzOTtcbiAgcGFkZGluZzogMXJlbSAxLjVyZW07XG4gIHdpZHRoOiAxMDAlO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIG1hcmdpbjogMC41cmVtIDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUgaW5wdXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjZjRmNGY3O1xuICBoZWlnaHQ6IDNyZW07XG4gIHBhZGRpbmc6IDAgMXJlbTtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX2NoYXJpdHktcGhvbmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNjAlO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAxMCU7XG59XG5cbi53cmFwcGVyLW1pc3Npb24ge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1pbm5lci9taXNzaW9uLWJnLnBuZ1wiKSBuby1yZXBlYXQsICNmOGY4Zjg7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogODAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDByZW07XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGVudDtcbiAgcGFkZGluZzogOCUgNSU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgei1pbmRleDogMTtcbn1cbi53cmFwcGVyLW1pc3Npb24gYSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmc6IDAuNXJlbSAyLjVyZW07XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDAwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbi53cmFwcGVyLW1pc3Npb24gcCB7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuLndyYXBwZXItbWlzc2lvbiBpbWcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4ud3JhcHBlci1taXNzaW9uIGltZzpudGgtY2hpbGQoMSkge1xuICBsZWZ0OiAtNzBweDtcbiAgYW5pbWF0aW9uOiBiYWxsb24xIGluZmluaXRlIDlzIDFzIGVhc2UtaW4tb3V0O1xuICB6LWluZGV4OiAtMTtcbiAgdG9wOiAyMCU7XG59XG4ud3JhcHBlci1taXNzaW9uIGltZzpudGgtY2hpbGQoMikge1xuICB0b3A6IDIwJTtcbiAgcmlnaHQ6IC03MHB4O1xuICBhbmltYXRpb246IGJhbGxvbjIgaW5maW5pdGUgOXMgMnMgZWFzZS1pbi1vdXQ7XG4gIHotaW5kZXg6IC0xO1xufVxuXG5Aa2V5ZnJhbWVzIGJhbGxvbjEge1xuICAwJSB7XG4gICAgdG9wOiAyMCU7XG4gIH1cbiAgMjUlIHtcbiAgICB0b3A6IDAlO1xuICB9XG4gIDUwJSB7XG4gICAgdG9wOiAyMCU7XG4gIH1cbiAgNzUlIHtcbiAgICB0b3A6IDAlO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogMjAlO1xuICB9XG59XG5Aa2V5ZnJhbWVzIGJhbGxvbjIge1xuICAwJSB7XG4gICAgdG9wOiAyMCU7XG4gIH1cbiAgMjUlIHtcbiAgICB0b3A6IDAlO1xuICB9XG4gIDUwJSB7XG4gICAgdG9wOiAyMCU7XG4gIH1cbiAgNzUlIHtcbiAgICB0b3A6IDAlO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogMjAlO1xuICB9XG59XG4ud3JhcHBlci12aXNpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IDUyOHB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1pbm5lci92aXNpb24ucG5nXCIpIG5vLXJlcGVhdDtcbiAgbWFyZ2luLWJvdHRvbTogMTAlO1xufVxuLndyYXBwZXItdmlzaW9uIC5hYnNvbHV0ZS13cmFwcGVyX19ub3RlIHtcbiAgd2lkdGg6IDMxJTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IC0xMSU7XG4gIGxlZnQ6IC01JTtcbiAgYm94LXNoYWRvdzogMCAwIDEwcHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICBwYWRkaW5nOiAzJTtcbn1cbi53cmFwcGVyLXZpc2lvbiAuYWJzb2x1dGUtd3JhcHBlcl9fbm90ZSBoMiB7XG4gIGNvbG9yOiAjZDQwMDJkO1xufVxuXG4uZmFjdHMge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMiUgMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZhY3RzIHAge1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nOiAwIDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmZhY3RzIHA6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMzVweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9xdW91dGVzLnN2Z1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgbGVmdDogNDBweDtcbn1cbi5mYWN0cyBwOjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3F1b3V0ZXMuc3ZnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxMHB4O1xuICByaWdodDogNDBweDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/about/about-inner/about-inner.component.ts": 
        /*!************************************************************************************!*\
          !*** ./src/app/modules/home/components/about/about-inner/about-inner.component.ts ***!
          \************************************************************************************/
        /*! exports provided: AboutInnerComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutInnerComponent", function () { return AboutInnerComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var AboutInnerComponent = /** @class */ (function () {
                function AboutInnerComponent() {
                    this.aboutLeft = "./assets/images/about/about-inner/about-inner-left.png";
                    this.aboutRight = "./assets/images/about/about-inner/about-inner-right.png";
                    this.ballons = "./assets/images/about/about-inner/ballons.png";
                    this.vision = "./assets/images/about/about-inner/vision.png";
                }
                AboutInnerComponent.prototype.ngOnInit = function () {
                };
                return AboutInnerComponent;
            }());
            AboutInnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-about-inner',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about-inner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about-inner/about-inner.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about-inner.component.scss */ "./src/app/modules/home/components/about/about-inner/about-inner.component.scss")).default]
                })
            ], AboutInnerComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/about/about.component.scss": 
        /*!********************************************************************!*\
          !*** ./src/app/modules/home/components/about/about.component.scss ***!
          \********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".about-container {\n  position: relative;\n  width: 100%;\n  height: 100vh;\n  max-height: 1200px;\n  overflow: hidden;\n  background: #fff;\n  color: #fff;\n}\n.about-container h5 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 52%;\n  background-position-y: 88%;\n  display: inline-block;\n  margin-bottom: 0;\n  z-index: 2;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.about-container .slide1 {\n  height: 105%;\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide2 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide2.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide3 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide3.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide4 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide4.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide5 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide5.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide6 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide6.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container .slide7 {\n  height: 105%;\n  background: url(\"/assets/images/about/about-slide7.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n  margin-top: -2%;\n  padding-top: 10%;\n}\n.about-container h5 {\n  width: 100%;\n  margin: auto;\n  text-align: center;\n  margin-top: 2%;\n  font-size: 30px;\n}\n.about-container span {\n  display: block;\n  width: 50%;\n  margin: auto;\n  text-align: center;\n  margin-bottom: 5%;\n}\n.about-container .about__region {\n  font-size: 27px;\n  width: 50%;\n  margin: auto;\n  text-align: center;\n}\n.about-container .about__desc {\n  width: 60%;\n  margin: auto;\n  text-align: center;\n}\n.fotorama__thumb-border {\n  border-color: transparent !important;\n}\n.fotorama__nav__frame--thumb:nth-child(1) .fotorama__thumb {\n  background-image: url(\"/assets/images/about/about-slide1.png\") !important;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(2) .fotorama__thumb {\n  background-image: url(\"/assets/images/about/about-slide1.png\") !important;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(3) {\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(4) {\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(5) {\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(6) {\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n.fotorama__nav__frame--thumb:nth-child(7) {\n  background-image: url(\"/assets/images/about/about-slide1.png\");\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n#classLink {\n  display: inline-block;\n  width: 90%;\n  height: 64%;\n  position: absolute;\n  top: 10%;\n  left: 5%;\n  z-index: 999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvYWJvdXQvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxhYm91dFxcYWJvdXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNWSjtBRFdJO0VBQ0ksaUJBQUE7RUFDQSx5REFBQTtFQUNBLDBCQUFBO0VBQ0EsMEJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0EsMkJBQUE7VUFBQSxtQkFBQTtBQ1RSO0FEV0k7RUFDRyxZQUFBO0VBQ0EsOERBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDVFA7QURZSTtFQUNJLFlBQUE7RUFDQSx3REFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNWUjtBRGFJO0VBQ0ksWUFBQTtFQUNBLHdEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ1hSO0FEY0k7RUFDSSxZQUFBO0VBQ0Esd0RBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDWlI7QURlSTtFQUNJLFlBQUE7RUFDQSx3REFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNiUjtBRGdCSTtFQUNJLFlBQUE7RUFDQSx3REFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNkUjtBRGlCSTtFQUNJLFlBQUE7RUFDQSx3REFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNmUjtBRG9CSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ2xCUjtBRG9CSTtFQUNJLGNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUNsQlI7QURxQkk7RUFDSSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ25CUjtBRHFCSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNuQlI7QUQyQkE7RUFDSSxvQ0FBQTtBQ3hCSjtBRDZCQTtFQUNJLHlFQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQzFCSjtBRDRCQztFQUNHLHlFQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3pCSjtBRDJCQTtFQUNJLDhEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3hCSjtBRDBCQTtFQUNJLDhEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3ZCSjtBRHlCQTtFQUNJLDhEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3RCSjtBRHdCQTtFQUNJLDhEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3JCSjtBRHVCQTtFQUNJLDhEQUFBO0VBQ0Esc0JBQUE7RUFDQSw0QkFBQTtBQ3BCSjtBRHlCQTtFQUNJLHFCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQ3RCSiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJHNsaWRlMTogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlMS5wbmcnO1xyXG4kc2xpZGUyOiAnL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUyLnBuZyc7XHJcbiRzbGlkZTM6ICcvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1zbGlkZTMucG5nJztcclxuJHNsaWRlNDogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlNC5wbmcnO1xyXG4kc2xpZGU1OiAnL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGU1LnBuZyc7XHJcbiRzbGlkZTY6ICcvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1zbGlkZTYucG5nJztcclxuJHNsaWRlNzogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlNy5wbmcnO1xyXG5cclxuJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmcnO1xyXG4kYmFsbDogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LWlubmVyL2JhbGxvbnMucG5nJztcclxuXHJcbi5hYm91dC1jb250YWluZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgbWF4LWhlaWdodDogMTIwMHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGg1IHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogNTIlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODglOyBcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICB6LWluZGV4OiAyO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgYW5pbWF0aW9uOiB0aXRsZSAycztcclxuICAgIH1cclxuICAgIC5zbGlkZTF7XHJcbiAgICAgICBoZWlnaHQ6IDEwNSU7XHJcbiAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJHNsaWRlMSk7XHJcbiAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgIG1hcmdpbi10b3A6IC0yJTtcclxuICAgICAgIHBhZGRpbmctdG9wOiAxMCU7XHJcblxyXG4gICAgfVxyXG4gICAgLnNsaWRlMntcclxuICAgICAgICBoZWlnaHQ6IDEwNSU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCRzbGlkZTIpO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMiU7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDEwJTtcclxuXHJcbiAgICB9XHJcbiAgICAuc2xpZGUze1xyXG4gICAgICAgIGhlaWdodDogMTA1JTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHNsaWRlMyk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0yJTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG5cclxuICAgIH1cclxuICAgIC5zbGlkZTR7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDUlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgkc2xpZGU0KTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTIlO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMCU7XHJcblxyXG4gICAgfVxyXG4gICAgLnNsaWRlNXtcclxuICAgICAgICBoZWlnaHQ6IDEwNSU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCRzbGlkZTUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMiU7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDEwJTtcclxuXHJcbiAgICB9XHJcbiAgICAuc2xpZGU2e1xyXG4gICAgICAgIGhlaWdodDogMTA1JTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHNsaWRlNik7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0yJTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG5cclxuICAgIH1cclxuICAgIC5zbGlkZTd7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDUlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgkc2xpZGU3KTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTIlO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMCU7XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBoNSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7ICBcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIH1cclxuICAgIHNwYW4ge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiA1MCU7ICBcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gICAgfVxyXG5cclxuICAgIC5hYm91dF9fcmVnaW9uIHtcclxuICAgICAgICBmb250LXNpemU6IDI3cHg7XHJcbiAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLmFib3V0X19kZXNje1xyXG4gICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvOyBcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIFxyXG5cclxufVxyXG5cclxuLmZvdG9yYW1hX190aHVtYi1ib3JkZXJ7XHJcbiAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoMSkgLmZvdG9yYW1hX190aHVtYiB7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJHNsaWRlMSkhaW1wb3J0YW50IDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gfVxyXG4gLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoMikgIC5mb3RvcmFtYV9fdGh1bWJ7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJHNsaWRlMSkhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoMykge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRzbGlkZTEpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoNCkge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRzbGlkZTEpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoNSkge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRzbGlkZTEpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoNikge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRzbGlkZTEpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoNykge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRzbGlkZTEpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuXHJcblxyXG5cclxuI2NsYXNzTGlua3tcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDY0JTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTAlO1xyXG4gICAgbGVmdDogNSU7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbn0iLCIuYWJvdXQtY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgbWF4LWhlaWdodDogMTIwMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBjb2xvcjogI2ZmZjtcbn1cbi5hYm91dC1jb250YWluZXIgaDUge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogNTIlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB6LWluZGV4OiAyO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYW5pbWF0aW9uOiB0aXRsZSAycztcbn1cbi5hYm91dC1jb250YWluZXIgLnNsaWRlMSB7XG4gIGhlaWdodDogMTA1JTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUxLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXRvcDogLTIlO1xuICBwYWRkaW5nLXRvcDogMTAlO1xufVxuLmFib3V0LWNvbnRhaW5lciAuc2xpZGUyIHtcbiAgaGVpZ2h0OiAxMDUlO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1zbGlkZTIucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBtYXJnaW4tdG9wOiAtMiU7XG4gIHBhZGRpbmctdG9wOiAxMCU7XG59XG4uYWJvdXQtY29udGFpbmVyIC5zbGlkZTMge1xuICBoZWlnaHQ6IDEwNSU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlMy5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIG1hcmdpbi10b3A6IC0yJTtcbiAgcGFkZGluZy10b3A6IDEwJTtcbn1cbi5hYm91dC1jb250YWluZXIgLnNsaWRlNCB7XG4gIGhlaWdodDogMTA1JTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGU0LnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXRvcDogLTIlO1xuICBwYWRkaW5nLXRvcDogMTAlO1xufVxuLmFib3V0LWNvbnRhaW5lciAuc2xpZGU1IHtcbiAgaGVpZ2h0OiAxMDUlO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1zbGlkZTUucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBtYXJnaW4tdG9wOiAtMiU7XG4gIHBhZGRpbmctdG9wOiAxMCU7XG59XG4uYWJvdXQtY29udGFpbmVyIC5zbGlkZTYge1xuICBoZWlnaHQ6IDEwNSU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlNi5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIG1hcmdpbi10b3A6IC0yJTtcbiAgcGFkZGluZy10b3A6IDEwJTtcbn1cbi5hYm91dC1jb250YWluZXIgLnNsaWRlNyB7XG4gIGhlaWdodDogMTA1JTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGU3LnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgbWFyZ2luLXRvcDogLTIlO1xuICBwYWRkaW5nLXRvcDogMTAlO1xufVxuLmFib3V0LWNvbnRhaW5lciBoNSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMiU7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5hYm91dC1jb250YWluZXIgc3BhbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG59XG4uYWJvdXQtY29udGFpbmVyIC5hYm91dF9fcmVnaW9uIHtcbiAgZm9udC1zaXplOiAyN3B4O1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5hYm91dC1jb250YWluZXIgLmFib3V0X19kZXNjIHtcbiAgd2lkdGg6IDYwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb3RvcmFtYV9fdGh1bWItYm9yZGVyIHtcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4uZm90b3JhbWFfX25hdl9fZnJhbWUtLXRodW1iOm50aC1jaGlsZCgxKSAuZm90b3JhbWFfX3RodW1iIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUxLnBuZ1wiKSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG4uZm90b3JhbWFfX25hdl9fZnJhbWUtLXRodW1iOm50aC1jaGlsZCgyKSAuZm90b3JhbWFfX3RodW1iIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUxLnBuZ1wiKSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG4uZm90b3JhbWFfX25hdl9fZnJhbWUtLXRodW1iOm50aC1jaGlsZCgzKSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlMS5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG5cbi5mb3RvcmFtYV9fbmF2X19mcmFtZS0tdGh1bWI6bnRoLWNoaWxkKDQpIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUxLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuLmZvdG9yYW1hX19uYXZfX2ZyYW1lLS10aHVtYjpudGgtY2hpbGQoNSkge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1zbGlkZTEucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xufVxuXG4uZm90b3JhbWFfX25hdl9fZnJhbWUtLXRodW1iOm50aC1jaGlsZCg2KSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LXNsaWRlMS5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG5cbi5mb3RvcmFtYV9fbmF2X19mcmFtZS0tdGh1bWI6bnRoLWNoaWxkKDcpIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtc2xpZGUxLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuI2NsYXNzTGluayB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDkwJTtcbiAgaGVpZ2h0OiA2NCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAxMCU7XG4gIGxlZnQ6IDUlO1xuICB6LWluZGV4OiA5OTk7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/about/about.component.ts": 
        /*!******************************************************************!*\
          !*** ./src/app/modules/home/components/about/about.component.ts ***!
          \******************************************************************/
        /*! exports provided: AboutComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function () { return AboutComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var AboutComponent = /** @class */ (function () {
                function AboutComponent(store) {
                    this.store = store;
                    this.slide1 = './assets/images/about/about-slide1.png';
                    this.slide2 = './assets/images/about/about-slide2.png';
                    this.slide3 = './assets/images/about/about-slide3.png';
                    this.slide4 = './assets/images/about/about-slide4.png';
                    this.slide5 = './assets/images/about/about-slide5.png';
                    this.slide6 = './assets/images/about/about-slide6.png';
                    this.slide7 = './assets/images/about/about-slide7.png';
                }
                AboutComponent.prototype.ngOnInit = function () {
                };
                AboutComponent.prototype.ngAfterContentInit = function () {
                    $('.fotorama').fotorama();
                };
                return AboutComponent;
            }());
            AboutComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-about',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/about/about.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about.component.scss */ "./src/app/modules/home/components/about/about.component.scss")).default]
                })
            ], AboutComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/archive/archive.component.scss": 
        /*!************************************************************************!*\
          !*** ./src/app/modules/home/components/archive/archive.component.scss ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".archive-container {\n  height: 100vh;\n  position: relative;\n  overflow: hidden;\n  background: url(), #f7f7fa;\n}\n.archive-container .archive__bear {\n  position: absolute;\n  top: 25%;\n  right: 0;\n  z-index: 10;\n  transition: all 0.3s;\n  -webkit-animation: bear 1s;\n          animation: bear 1s;\n}\n.archive-container .archive-header {\n  width: 100%;\n  height: 10%;\n  text-align: center;\n  position: relative;\n}\n.archive-container span {\n  position: relative;\n}\n.archive-container h2 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 30%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 48px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.archive-container .archive-content {\n  text-align: right;\n}\n.archive-container .archive-content a {\n  color: unset;\n}\n.archive-container .archive-content a:hover {\n  text-decoration: none;\n}\n.archive-container .archive-content .archive-container-article {\n  text-align: left;\n  padding: 4%;\n  margin: auto;\n  margin-right: 0;\n  background: url(\"/assets/images/archive/chevron.png\") 95% 50% no-repeat, #fff;\n  margin-bottom: 6%;\n  width: 80%;\n}\n.archive-container .archive-content .archive-container-article p span {\n  color: #969697;\n}\n.archive-container .archive-content .archive-container-article p:first-child {\n  color: #000;\n}\n.archive-container .archive-content .archive-container-article P {\n  color: #969697;\n}\n.archive-container .archive-content .archive-mobile {\n  text-align: left;\n}\n@-webkit-keyframes bear {\n  from {\n    right: -30%;\n  }\n  to {\n    right: 0%;\n  }\n}\n@keyframes bear {\n  from {\n    right: -30%;\n  }\n  to {\n    right: 0%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvYXJjaGl2ZS9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXG1vZHVsZXNcXGhvbWVcXGNvbXBvbmVudHNcXGFyY2hpdmVcXGFyY2hpdmUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2FyY2hpdmUvYXJjaGl2ZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQVVJLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7QUNYSjtBRERJO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLDBCQUFBO1VBQUEsa0JBQUE7QUNHUjtBRE1JO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDSlI7QURPSTtFQUNJLGtCQUFBO0FDTFI7QURRSTtFQUNJLGlCQUFBO0VBQ0EsNkRBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSwyQkFBQTtVQUFBLG1CQUFBO0FDTlI7QURVSTtFQVNJLGlCQUFBO0FDaEJSO0FEUVE7RUFDSSxZQUFBO0FDTlo7QURRWTtFQUNJLHFCQUFBO0FDTmhCO0FEWVE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLDZFQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0FDVlo7QURhZ0I7RUFDSSxjQUFBO0FDWHBCO0FEY2dCO0VBQ0ksV0FBQTtBQ1pwQjtBRGdCWTtFQUNJLGNBQUE7QUNkaEI7QURrQlE7RUFDSSxnQkFBQTtBQ2hCWjtBRHVCQTtFQUNJO0lBQ0ksV0FBQTtFQ3BCTjtFRHNCRTtJQUNJLFNBQUE7RUNwQk47QUFDRjtBRGNBO0VBQ0k7SUFDSSxXQUFBO0VDcEJOO0VEc0JFO0lBQ0ksU0FBQTtFQ3BCTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvYXJjaGl2ZS9hcmNoaXZlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGNoZXZyb246IFwiL2Fzc2V0cy9pbWFnZXMvYXJjaGl2ZS9jaGV2cm9uLnBuZ1wiO1xyXG4kdW5kZXJsaW5lOiAnL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLWJpZy5wbmcnO1xyXG5cclxuLmFyY2hpdmUtY29udGFpbmVyIHtcclxuICAgIC5hcmNoaXZlX19iZWFyIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAyNSU7XHJcbiAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgei1pbmRleDogMTA7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcztcclxuICAgICAgICBhbmltYXRpb246IGJlYXIgMXMgO1xyXG4gICAgfVxyXG5cclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCksXHJcbiAgICAjZjdmN2ZhO1xyXG5cclxuICAgIC5hcmNoaXZlLWhlYWRlciB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxuXHJcbiAgICBzcGFuIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB9XHJcblxyXG4gICAgaDIge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgkdW5kZXJsaW5lKSBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiA4MCUgMzAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgZm9udC1zaXplOiA0OHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVuc2V0O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgYW5pbWF0aW9uOiB0aXRsZSAycztcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLmFyY2hpdmUtY29udGVudCB7XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiB1bnNldDtcclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuXHJcbiAgICAgICAgLmFyY2hpdmUtY29udGFpbmVyLWFydGljbGUge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0JTtcclxuICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHVybCgkY2hldnJvbikgOTUlIDUwJSBuby1yZXBlYXQsICNmZmY7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDYlO1xyXG4gICAgICAgICAgICB3aWR0aDogODAlO1xyXG5cclxuICAgICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzk2OTY5NztcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgUCB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzk2OTY5NztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLmFyY2hpdmUtbW9iaWxlIHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5cclxuQGtleWZyYW1lcyBiZWFyIHtcclxuICAgIGZyb217XHJcbiAgICAgICAgcmlnaHQ6IC0zMCU7XHJcbiAgICB9XHJcbiAgICB0b3tcclxuICAgICAgICByaWdodDogMCU7XHJcbiAgICB9XHJcbn1cclxuIiwiLmFyY2hpdmUtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiB1cmwoKSwgI2Y3ZjdmYTtcbn1cbi5hcmNoaXZlLWNvbnRhaW5lciAuYXJjaGl2ZV9fYmVhciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAyNSU7XG4gIHJpZ2h0OiAwO1xuICB6LWluZGV4OiAxMDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGFuaW1hdGlvbjogYmVhciAxcztcbn1cbi5hcmNoaXZlLWNvbnRhaW5lciAuYXJjaGl2ZS1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmFyY2hpdmUtY29udGFpbmVyIHNwYW4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uYXJjaGl2ZS1jb250YWluZXIgaDIge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLWJpZy5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAzMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiA0OHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHRpdGxlIDJzO1xufVxuLmFyY2hpdmUtY29udGFpbmVyIC5hcmNoaXZlLWNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5hcmNoaXZlLWNvbnRhaW5lciAuYXJjaGl2ZS1jb250ZW50IGEge1xuICBjb2xvcjogdW5zZXQ7XG59XG4uYXJjaGl2ZS1jb250YWluZXIgLmFyY2hpdmUtY29udGVudCBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLmFyY2hpdmUtY29udGFpbmVyIC5hcmNoaXZlLWNvbnRlbnQgLmFyY2hpdmUtY29udGFpbmVyLWFydGljbGUge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nOiA0JTtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2FyY2hpdmUvY2hldnJvbi5wbmdcIikgOTUlIDUwJSBuby1yZXBlYXQsICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IDYlO1xuICB3aWR0aDogODAlO1xufVxuLmFyY2hpdmUtY29udGFpbmVyIC5hcmNoaXZlLWNvbnRlbnQgLmFyY2hpdmUtY29udGFpbmVyLWFydGljbGUgcCBzcGFuIHtcbiAgY29sb3I6ICM5Njk2OTc7XG59XG4uYXJjaGl2ZS1jb250YWluZXIgLmFyY2hpdmUtY29udGVudCAuYXJjaGl2ZS1jb250YWluZXItYXJ0aWNsZSBwOmZpcnN0LWNoaWxkIHtcbiAgY29sb3I6ICMwMDA7XG59XG4uYXJjaGl2ZS1jb250YWluZXIgLmFyY2hpdmUtY29udGVudCAuYXJjaGl2ZS1jb250YWluZXItYXJ0aWNsZSBQIHtcbiAgY29sb3I6ICM5Njk2OTc7XG59XG4uYXJjaGl2ZS1jb250YWluZXIgLmFyY2hpdmUtY29udGVudCAuYXJjaGl2ZS1tb2JpbGUge1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG5Aa2V5ZnJhbWVzIGJlYXIge1xuICBmcm9tIHtcbiAgICByaWdodDogLTMwJTtcbiAgfVxuICB0byB7XG4gICAgcmlnaHQ6IDAlO1xuICB9XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/archive/archive.component.ts": 
        /*!**********************************************************************!*\
          !*** ./src/app/modules/home/components/archive/archive.component.ts ***!
          \**********************************************************************/
        /*! exports provided: ArchiveComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArchiveComponent", function () { return ArchiveComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var ArchiveComponent = /** @class */ (function () {
                function ArchiveComponent(store) {
                    this.store = store;
                    this.bear = "./assets/images/archive/bear.png";
                    this.archivePhoto = './assets/images/archive/archive-photo.png';
                }
                ArchiveComponent.prototype.ngOnInit = function () {
                };
                return ArchiveComponent;
            }());
            ArchiveComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            ArchiveComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-archive',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./archive.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/archive/archive.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./archive.component.scss */ "./src/app/modules/home/components/archive/archive.component.scss")).default]
                })
            ], ArchiveComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/charity/charity-inner/charity-inner.component.scss": 
        /*!********************************************************************************************!*\
          !*** ./src/app/modules/home/components/charity/charity-inner/charity-inner.component.scss ***!
          \********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".charity-inner-container {\n  background: #fff;\n}\n\n#ngCarousel {\n  width: 100%;\n}\n\n#ngCarousel-conten {\n  box-shadow: none;\n}\n\n.image-family {\n  margin: auto;\n  width: 85%;\n}\n\n.achivment-wrapper {\n  position: relative;\n  padding-bottom: 5%;\n}\n\n.achivment-wrapper .absolute__container-achivment {\n  position: absolute;\n  background: #fff;\n  bottom: 5%;\n  left: 1%;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);\n  padding: 1rem;\n}\n\n.achivment-wrapper .absolute__container-achivment span {\n  color: #d4002d;\n  font-size: 20px;\n}\n\n.charity-container__title {\n  padding: 0 5%;\n}\n\n.charity-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 20%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 48px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s linear;\n          animation: title 2s linear;\n}\n\n.charity-container__text {\n  padding: 0 5%;\n}\n\n.btn_be-part-of {\n  color: inherit;\n  border: 1px solid #000;\n  padding: 0.3rem 2rem;\n  border-radius: 3px;\n  margin: auto;\n  text-transform: uppercase;\n  font-size: 12px;\n}\n\n.btn_be-part-of:hover {\n  text-decoration: none;\n}\n\n.charity-container {\n  color: #252539;\n  background: linear-gradient(0deg, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0.3) 100%), url(\"/assets/images/charity/charity-bg.png\"), #fff;\n  background-size: cover;\n  position: relative;\n  overflow: hidden;\n  padding: 7% 0;\n}\n\n.charity-container .charity-header {\n  width: 100%;\n  height: 10%;\n  text-transform: uppercase;\n  text-align: center;\n  position: relative;\n}\n\n.charity-container .charity-header span {\n  position: relative;\n}\n\n.charity-container .charity-header h2 {\n  display: inline-block;\n  z-index: 1;\n}\n\n.charity-container .charity-header h2::after {\n  content: \"\";\n  display: inline-block;\n  position: absolute;\n  width: 96%;\n  height: 10px;\n  background: red;\n  right: -17px;\n  bottom: 2px;\n  z-index: 0;\n}\n\n.charity-container .charity-container__option {\n  position: relative;\n  border-radius: 7px;\n  padding: 1rem;\n  margin: 10% 0;\n}\n\n.charity-container .charity-container__option:first-child {\n  background: transparent;\n  margin: 0;\n}\n\n.charity-container .charity-container__option:last-child {\n  background: #000;\n  color: #fff;\n}\n\n.charity-container .charity-container__option:last-child::after {\n  content: \"\";\n  display: inline-block;\n  background: #000;\n  width: 15px;\n  height: 15px;\n  position: absolute;\n  right: -15px;\n  top: 20px;\n  -webkit-clip-path: polygon(0 100%, 0 0, 100% 0);\n          clip-path: polygon(0 100%, 0 0, 100% 0);\n}\n\n.charity-container .charity-container__wrapper {\n  position: absolute;\n  height: 420px;\n  bottom: 0;\n  right: 2%;\n  background: url(\"/assets/images/charity/charity-phone.png\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-position-y: 0;\n  background-size: cover;\n  padding-top: 9%;\n  max-width: 430px;\n}\n\n.charity-container .charity-container__wrapper .container-phone {\n  width: 85%;\n  margin: auto;\n  text-align: center;\n  background: none;\n}\n\n.charity-container .charity-container__wrapper .container-phone a {\n  display: inline-block;\n  color: #252539;\n  padding: 1rem 1.5rem;\n  width: 100%;\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);\n  border-radius: 9px;\n  margin: 0.5rem 0;\n  transition: all 0.3s;\n}\n\n.charity-container .charity-container__wrapper .container-phone a:hover {\n  background: red;\n  color: #fff;\n  text-decoration: none;\n}\n\n.charity-container .charity-container__wrapper .container-phone input {\n  width: 100%;\n  border: none;\n  background: #f4f4f7;\n  height: 3rem;\n  padding: 0 1rem;\n  border: none;\n}\n\n.charity-container .charity-container__wrapper .container-phone input:focus {\n  outline: none;\n}\n\n.charity-container .charity-container__charity-phone {\n  position: absolute;\n  height: 60%;\n  bottom: 0;\n  right: 10%;\n}\n\n.charity-inner-container__slider img {\n  width: 100%;\n}\n\n.facts {\n  width: 100%;\n  padding: 2% 0;\n  position: relative;\n}\n\n.facts p {\n  margin-bottom: 0;\n  padding: 0 10%;\n  text-align: center;\n  font-size: 22px;\n  font-weight: bold;\n}\n\n.facts p::after {\n  content: \"\";\n  display: inline-block;\n  width: 35px;\n  height: 35px;\n  background: url(\"/assets/images/quoutes.svg\");\n  background-size: contain;\n  transform: rotate(180deg);\n  position: absolute;\n  top: 10px;\n  left: 40px;\n}\n\n.facts p::before {\n  content: \"\";\n  display: inline-block;\n  width: 35px;\n  height: 35px;\n  background: url(\"/assets/images/quoutes.svg\");\n  background-size: contain;\n  position: absolute;\n  bottom: 10px;\n  right: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvY2hhcml0eS9jaGFyaXR5LWlubmVyL0M6XFxPU1BhbmVsXFxkb21haW5zXFxhX2x1eF9fY2FyaXRhc1xcY2FyaXRhcy9zcmNcXGFwcFxcbW9kdWxlc1xcaG9tZVxcY29tcG9uZW50c1xcY2hhcml0eVxcY2hhcml0eS1pbm5lclxcY2hhcml0eS1pbm5lci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvY2hhcml0eS9jaGFyaXR5LWlubmVyL2NoYXJpdHktaW5uZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0E7RUFDQSxnQkFBQTtBQ0pBOztBRE9BO0VBQ0EsV0FBQTtBQ0pBOztBRE9BO0VBQ0ksZ0JBQUE7QUNKSjs7QURPQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0FDSko7O0FET0E7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0FDSko7O0FES0k7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFFQSx1Q0FBQTtFQUNBLGFBQUE7QUNIUjs7QURJUTtFQUNJLGNBQUE7RUFDQSxlQUFBO0FDRlo7O0FET0E7RUFDSSxhQUFBO0FDSko7O0FES0k7RUFDSSxpQkFBQTtFQUNBLDZEQUFBO0VBQ0Esd0JBQUE7RUFDQSwyQkFBQTtFQUNBLDBCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0NBQUE7VUFBQSwwQkFBQTtBQ0hSOztBRE9BO0VBQ0ksYUFBQTtBQ0pKOztBRE9BO0VBQ0csY0FBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7QUNKSDs7QURNRztFQUNJLHFCQUFBO0FDSlA7O0FEUUE7RUFDSSxjQUFBO0VBQ0EscUlBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FDTEo7O0FET0k7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0xSOztBRE9RO0VBQ0ksa0JBQUE7QUNMWjs7QURRUTtFQUNJLHFCQUFBO0VBQ0EsVUFBQTtBQ05aOztBRFFZO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7QUNOaEI7O0FEV0k7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7QUNUUjs7QURXUTtFQUNJLHVCQUFBO0VBQ0EsU0FBQTtBQ1RaOztBRFlRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0FDVlo7O0FEWVk7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLCtDQUFBO1VBQUEsdUNBQUE7QUNWaEI7O0FEZUk7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLDJEQUFBO0VBQ0EsNEJBQUE7RUFDQSwyQkFBQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNiUjs7QURlUTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ2JaOztBRGNZO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBRUEsdUNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7QUNaaEI7O0FEY2dCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtBQ1pwQjs7QURnQlk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDZGhCOztBRGdCZ0I7RUFDSSxhQUFBO0FDZHBCOztBRG9CSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FDbEJSOztBRHVCSTtFQUNJLFdBQUE7QUNwQlI7O0FEdUJBO0VBRUksV0FBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQ3JCSjs7QUR1Qkk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ3JCUjs7QUR1QlE7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDZDQUFBO0VBQ0Esd0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUNyQlo7O0FEd0JRO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSw2Q0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQ3RCWiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2NoYXJpdHkvY2hhcml0eS1pbm5lci9jaGFyaXR5LWlubmVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGNoYXJpdHktcGhvbmU6IFwiL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LXBob25lLnBuZ1wiO1xyXG4kY2hhcml0eS1iZzogJy9hc3NldHMvaW1hZ2VzL2NoYXJpdHkvY2hhcml0eS1iZy5wbmcnO1xyXG4kcXVvdXRlczogJy9hc3NldHMvaW1hZ2VzL3F1b3V0ZXMuc3ZnJztcclxuJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS1iaWcucG5nJztcclxuXHJcbi5jaGFyaXR5LWlubmVyLWNvbnRhaW5lcntcclxuYmFja2dyb3VuZDogI2ZmZjtcclxufVxyXG5cclxuI25nQ2Fyb3VzZWx7XHJcbndpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4jbmdDYXJvdXNlbC1jb250ZW57XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG4uaW1hZ2UtZmFtaWx5e1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgd2lkdGg6IDg1JTtcclxufVxyXG5cclxuLmFjaGl2bWVudC13cmFwcGVye1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDUlO1xyXG4gICAgLmFic29sdXRlX19jb250YWluZXItYWNoaXZtZW50e1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgIGJvdHRvbTogNSU7XHJcbiAgICAgICAgbGVmdDogMSU7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIC41KTtcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIC41KTtcclxuICAgICAgICBwYWRkaW5nOiAxcmVtO1xyXG4gICAgICAgIHNwYW57XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZDQwMDJkIDtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH0gXHJcbn1cclxuXHJcbi5jaGFyaXR5LWNvbnRhaW5lcl9fdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwIDUlICA7XHJcbiAgICBoM3tcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogODAlIDIwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNDhweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1bnNldDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgICAgIGFuaW1hdGlvbjogdGl0bGUgMnMgbGluZWFyO1xyXG4gICAgfVxyXG59XHJcblxyXG4uY2hhcml0eS1jb250YWluZXJfX3RleHR7XHJcbiAgICBwYWRkaW5nOiAwIDUlICA7XHJcbn1cclxuXHJcbi5idG5fYmUtcGFydC1vZiB7XHJcbiAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG4gICBwYWRkaW5nOiAuM3JlbSAycmVtO1xyXG4gICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgIG1hcmdpbjogYXV0bztcclxuICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgJjpob3ZlciB7XHJcbiAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgIH1cclxufVxyXG5cclxuLmNoYXJpdHktY29udGFpbmVyIHtcclxuICAgIGNvbG9yOiAjMjUyNTM5O1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDBkZWcsIHJnYmEoMCwgMCwgMCwgLjMpIDAlLCByZ2JhKDAsIDAsIDAsIC4zKSAxMDAlKSwgdXJsKCRjaGFyaXR5LWJnKSwgI2ZmZjtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcGFkZGluZzogNyUgMDtcclxuXHJcbiAgICAuY2hhcml0eS1oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAlO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICB6LWluZGV4OiAxO1xyXG5cclxuICAgICAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA5NiU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgICAgICAgICAgICAgICByaWdodDogLTE3cHg7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IDJweDtcclxuICAgICAgICAgICAgICAgIHotaW5kZXg6IDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb24ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgcGFkZGluZzogMXJlbTtcclxuICAgICAgICBtYXJnaW46IDEwJSAwO1xyXG5cclxuICAgICAgICAmOmZpcnN0LWNoaWxkIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG5cclxuICAgICAgICAgICAgJjo6YWZ0ZXIge1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTVweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAtMTVweDtcclxuICAgICAgICAgICAgICAgIHRvcDogMjBweDtcclxuICAgICAgICAgICAgICAgIGNsaXAtcGF0aDogcG9seWdvbigwIDEwMCUsIDAgMCwgMTAwJSAwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDQyMHB4O1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICByaWdodDogMiU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCRjaGFyaXR5LXBob25lKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBwYWRkaW5nLXRvcDogOSU7XHJcbiAgICAgICAgbWF4LXdpZHRoOiA0MzBweDtcclxuXHJcbiAgICAgICAgLmNvbnRhaW5lci1waG9uZSB7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4NSU7XHJcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjMjUyNTM5O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMXJlbSAxLjVyZW07XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogOXB4O1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiAuNXJlbSAwO1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcztcclxuXHJcbiAgICAgICAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpbnB1dCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmNGY0Zjc7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDFyZW07XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpmb2N1cyB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY2hhcml0eS1jb250YWluZXJfX2NoYXJpdHktcGhvbmUge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBoZWlnaHQ6IDYwJTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgcmlnaHQ6IDEwJTtcclxuICAgIH1cclxufVxyXG5cclxuLmNoYXJpdHktaW5uZXItY29udGFpbmVyX19zbGlkZXJ7XHJcbiAgICBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbn1cclxuLmZhY3RzIHtcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybCgkcXVvdXRlcykgMCUgMCUgbm8tcmVwZWF0LCB1cmwoJHF1b3V0ZXMpIDEwMCUgMTAwJSBuby1yZXBlYXQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDIlIDA7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgcCB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICBwYWRkaW5nOiAwIDEwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cclxuICAgICAgICAmOjphZnRlciB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDM1cHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdXJsKCRxdW91dGVzKTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRvcDogMTBweDtcclxuICAgICAgICAgICAgbGVmdDogNDBweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICY6OmJlZm9yZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDM1cHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMzVweDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdXJsKCRxdW91dGVzKTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIGJvdHRvbTogMTBweDtcclxuICAgICAgICAgICAgcmlnaHQ6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiLmNoYXJpdHktaW5uZXItY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuI25nQ2Fyb3VzZWwge1xuICB3aWR0aDogMTAwJTtcbn1cblxuI25nQ2Fyb3VzZWwtY29udGVuIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLmltYWdlLWZhbWlseSB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDg1JTtcbn1cblxuLmFjaGl2bWVudC13cmFwcGVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nLWJvdHRvbTogNSU7XG59XG4uYWNoaXZtZW50LXdyYXBwZXIgLmFic29sdXRlX19jb250YWluZXItYWNoaXZtZW50IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3R0b206IDUlO1xuICBsZWZ0OiAxJTtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gIGJveC1zaGFkb3c6IDAgMCAxMHB4IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgcGFkZGluZzogMXJlbTtcbn1cbi5hY2hpdm1lbnQtd3JhcHBlciAuYWJzb2x1dGVfX2NvbnRhaW5lci1hY2hpdm1lbnQgc3BhbiB7XG4gIGNvbG9yOiAjZDQwMDJkO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5jaGFyaXR5LWNvbnRhaW5lcl9fdGl0bGUge1xuICBwYWRkaW5nOiAwIDUlO1xufVxuLmNoYXJpdHktY29udGFpbmVyX190aXRsZSBoMyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUtYmlnLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogODAlIDIwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDQ4cHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1bnNldDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGFuaW1hdGlvbjogdGl0bGUgMnMgbGluZWFyO1xufVxuXG4uY2hhcml0eS1jb250YWluZXJfX3RleHQge1xuICBwYWRkaW5nOiAwIDUlO1xufVxuXG4uYnRuX2JlLXBhcnQtb2Yge1xuICBjb2xvcjogaW5oZXJpdDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbiAgcGFkZGluZzogMC4zcmVtIDJyZW07XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG59XG4uYnRuX2JlLXBhcnQtb2Y6aG92ZXIge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5jaGFyaXR5LWNvbnRhaW5lciB7XG4gIGNvbG9yOiAjMjUyNTM5O1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMGRlZywgcmdiYSgwLCAwLCAwLCAwLjMpIDAlLCByZ2JhKDAsIDAsIDAsIDAuMykgMTAwJSksIHVybChcIi9hc3NldHMvaW1hZ2VzL2NoYXJpdHkvY2hhcml0eS1iZy5wbmdcIiksICNmZmY7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcGFkZGluZzogNyUgMDtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMCU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWhlYWRlciBzcGFuIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWhlYWRlciBoMiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgei1pbmRleDogMTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1oZWFkZXIgaDI6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA5NiU7XG4gIGhlaWdodDogMTBweDtcbiAgYmFja2dyb3VuZDogcmVkO1xuICByaWdodDogLTE3cHg7XG4gIGJvdHRvbTogMnB4O1xuICB6LWluZGV4OiAwO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fb3B0aW9uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3JkZXItcmFkaXVzOiA3cHg7XG4gIHBhZGRpbmc6IDFyZW07XG4gIG1hcmdpbjogMTAlIDA7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb246Zmlyc3QtY2hpbGQge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgbWFyZ2luOiAwO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fb3B0aW9uOmxhc3QtY2hpbGQge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX29wdGlvbjpsYXN0LWNoaWxkOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogLTE1cHg7XG4gIHRvcDogMjBweDtcbiAgY2xpcC1wYXRoOiBwb2x5Z29uKDAgMTAwJSwgMCAwLCAxMDAlIDApO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiA0MjBweDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogMiU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2NoYXJpdHkvY2hhcml0eS1waG9uZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAwO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwYWRkaW5nLXRvcDogOSU7XG4gIG1heC13aWR0aDogNDMwcHg7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUge1xuICB3aWR0aDogODUlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogbm9uZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIgLmNvbnRhaW5lci1waG9uZSBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogIzI1MjUzOTtcbiAgcGFkZGluZzogMXJlbSAxLjVyZW07XG4gIHdpZHRoOiAxMDAlO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIG1hcmdpbjogMC41cmVtIDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUgaW5wdXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjZjRmNGY3O1xuICBoZWlnaHQ6IDNyZW07XG4gIHBhZGRpbmc6IDAgMXJlbTtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX2NoYXJpdHktcGhvbmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNjAlO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAxMCU7XG59XG5cbi5jaGFyaXR5LWlubmVyLWNvbnRhaW5lcl9fc2xpZGVyIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZmFjdHMge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMiUgMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZhY3RzIHAge1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBwYWRkaW5nOiAwIDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmZhY3RzIHA6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMzVweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9xdW91dGVzLnN2Z1wiKTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgbGVmdDogNDBweDtcbn1cbi5mYWN0cyBwOjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3F1b3V0ZXMuc3ZnXCIpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAxMHB4O1xuICByaWdodDogNDBweDtcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/charity/charity-inner/charity-inner.component.ts": 
        /*!******************************************************************************************!*\
          !*** ./src/app/modules/home/components/charity/charity-inner/charity-inner.component.ts ***!
          \******************************************************************************************/
        /*! exports provided: CharityInnerComponent, NguCarouselStore */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharityInnerComponent", function () { return CharityInnerComponent; });
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NguCarouselStore", function () { return NguCarouselStore; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var CharityInnerComponent = /** @class */ (function () {
                function CharityInnerComponent() {
                    this.family = "/assets/images/charity/charity-inner/family.png";
                    this.imgags = [
                        '/assets/images/charity/charity-inner/slide1.png',
                        '/assets/images/charity/charity-inner/slide2.png',
                        '/assets/images/charity/charity-inner/slide3.png'
                    ];
                    this.carouselTileItems = [0];
                    this.carouselTiles = {
                        0: []
                    };
                    this.carouselTile = {
                        grid: { xs: 1, sm: 1, md: 1, lg: 3, all: 0 },
                        slide: 1,
                        speed: 250,
                        loop: true,
                        load: 1,
                        velocity: 0,
                        touch: true,
                        easing: 'cubic-bezier(0, 0, 0.2, 1)'
                    };
                }
                CharityInnerComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.carouselTileItems.forEach(function (el) {
                        _this.carouselTileLoad(el);
                    });
                };
                CharityInnerComponent.prototype.carouselTileLoad = function (j) {
                    // console.log(this.carouselTiles[j]);
                    var len = this.carouselTiles[j].length;
                    if (len <= 30) {
                        for (var i = len; i < len + 15; i++) {
                            this.carouselTiles[j].push(this.imgags[Math.floor(Math.random() * this.imgags.length)]);
                        }
                    }
                };
                return CharityInnerComponent;
            }());
            CharityInnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-charity-inner',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./charity-inner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity-inner/charity-inner.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./charity-inner.component.scss */ "./src/app/modules/home/components/charity/charity-inner/charity-inner.component.scss")).default]
                })
            ], CharityInnerComponent);
            var NguCarouselStore = /** @class */ (function () {
                function NguCarouselStore() {
                }
                return NguCarouselStore;
            }());
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/charity/charity.component.scss": 
        /*!************************************************************************!*\
          !*** ./src/app/modules/home/components/charity/charity.component.scss ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".charity-container {\n  color: #252539;\n  height: 100vh;\n  background: linear-gradient(0deg, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0.3) 100%), url(\"/assets/images/charity/charity-bg.png\"), #fff;\n  background-size: cover;\n  position: relative;\n  overflow: hidden;\n}\n.charity-container .charity-header {\n  width: 100%;\n  height: 10%;\n  text-align: center;\n  position: relative;\n}\n.charity-container .charity-header span {\n  position: relative;\n}\n.charity-container .charity-header h2 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 30%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 48px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s linear;\n          animation: title 2s linear;\n}\n.charity-container .charity-container__option {\n  position: relative;\n  border-radius: 7px;\n  padding: 1rem;\n  margin: 10% 0;\n  transition: all 0.2s;\n}\n.charity-container .charity-container__option:first-child {\n  background: #fff;\n  -webkit-animation: message1 2s;\n          animation: message1 2s;\n}\n.charity-container .charity-container__option:first-child::before {\n  content: \"\";\n  display: inline-block;\n  background: #fff;\n  width: 15px;\n  height: 15px;\n  position: absolute;\n  left: -15px;\n  top: 20px;\n  -webkit-clip-path: polygon(100% 0, 0 0, 100% 100%);\n          clip-path: polygon(100% 0, 0 0, 100% 100%);\n}\n.charity-container .charity-container__option:last-child {\n  background: #000;\n  color: #fff;\n  -webkit-animation: message2 2s;\n          animation: message2 2s;\n}\n.charity-container .charity-container__option:last-child::after {\n  content: \"\";\n  display: inline-block;\n  background: #000;\n  width: 15px;\n  height: 15px;\n  position: absolute;\n  right: -15px;\n  top: 20px;\n  -webkit-clip-path: polygon(0 100%, 0 0, 100% 0);\n          clip-path: polygon(0 100%, 0 0, 100% 0);\n}\n.charity-container .charity-container__wrapper {\n  position: absolute;\n  height: 420px;\n  bottom: 0;\n  right: 10%;\n  background: url(\"/assets/images/charity/charity-phone.png\");\n  background-repeat: no-repeat;\n  background-position: center;\n  background-position-y: 0;\n  background-size: cover;\n  padding-top: 9%;\n  max-width: 430px;\n}\n.charity-container .charity-container__wrapper .container-phone {\n  width: 85%;\n  margin: auto;\n  text-align: center;\n}\n.charity-container .charity-container__wrapper .container-phone a {\n  display: inline-block;\n  color: #252539;\n  padding: 1rem 1.5rem;\n  width: 100%;\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);\n  border-radius: 9px;\n  margin: 0.5rem 0;\n  transition: all 0.3s;\n}\n.charity-container .charity-container__wrapper .container-phone a:hover {\n  background: red;\n  color: #fff;\n  text-decoration: none;\n}\n.charity-container .charity-container__wrapper .container-phone input {\n  width: 100%;\n  border: none;\n  background: #f4f4f7;\n  height: 3rem;\n  padding: 0 1rem;\n  border: none;\n}\n.charity-container .charity-container__wrapper .container-phone input:focus {\n  outline: none;\n}\n.charity-container .charity-container__charity-phone {\n  position: absolute;\n  height: 60%;\n  bottom: 0;\n  right: 10%;\n}\n@-webkit-keyframes message1 {\n  from {\n    transform: translateX(-150%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes message1 {\n  from {\n    transform: translateX(-150%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@-webkit-keyframes message2 {\n  from {\n    transform: translateY(150%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n@keyframes message2 {\n  from {\n    transform: translateY(150%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvY2hhcml0eS9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXG1vZHVsZXNcXGhvbWVcXGNvbXBvbmVudHNcXGNoYXJpdHlcXGNoYXJpdHkuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2NoYXJpdHkvY2hhcml0eS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNJLGNBQUE7RUFDQSxhQUFBO0VBQ0EscUlBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNGSjtBREtJO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDSFI7QURJUTtFQUNJLGtCQUFBO0FDRlo7QURLUTtFQUNJLGlCQUFBO0VBQ0EsNkRBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQ0FBQTtVQUFBLDBCQUFBO0FDSFo7QURPSTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0FDTFI7QURNUTtFQUNJLGdCQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtBQ0paO0FES1k7RUFDRyxXQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtEQUFBO1VBQUEsMENBQUE7QUNIZjtBRE1RO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtBQ0paO0FES1k7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsU0FBQTtFQUNBLCtDQUFBO1VBQUEsdUNBQUE7QUNIaEI7QURRSTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsMkRBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0Esd0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ05SO0FEUVE7RUFDSSxVQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0FDUFo7QURTWTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUVBLHVDQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0FDUGhCO0FEU2dCO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtBQ1BwQjtBRFdZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ1RoQjtBRFdnQjtFQUNJLGFBQUE7QUNUcEI7QURlSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FDYlI7QURrQkE7RUFDSTtJQUNJLDRCQUFBO0VDZk47RURpQkU7SUFDSSx5QkFBQTtFQ2ZOO0FBQ0Y7QURTQTtFQUNJO0lBQ0ksNEJBQUE7RUNmTjtFRGlCRTtJQUNJLHlCQUFBO0VDZk47QUFDRjtBRGtCQTtFQUNJO0lBQ0ksMkJBQUE7RUNoQk47RURrQkU7SUFDSSx5QkFBQTtFQ2hCTjtBQUNGO0FEVUE7RUFDSTtJQUNJLDJCQUFBO0VDaEJOO0VEa0JFO0lBQ0kseUJBQUE7RUNoQk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2NoYXJpdHkvY2hhcml0eS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRjaGFyaXR5LWJnOiAnL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LWJnLnBuZyc7XHJcbiRjaGFyaXR5LXBob25lOiAnL2Fzc2V0cy9pbWFnZXMvY2hhcml0eS9jaGFyaXR5LXBob25lLnBuZyc7XHJcbiR1bmRlcmxpbmU6ICcvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUtYmlnLnBuZyc7XHJcbi5jaGFyaXR5LWNvbnRhaW5lciB7XHJcbiAgICBjb2xvcjogIzI1MjUzOTtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMGRlZywgcmdiYSgwLCAwLCAwLCAuMykgMCUsIHJnYmEoMCwgMCwgMCwgLjMpIDEwMCUpLCB1cmwoJGNoYXJpdHktYmcpLCAjZmZmO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgXHJcbiAgICAuY2hhcml0eS1oZWFkZXIge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB9XHJcbiAgICBcclxuICAgICAgICBoMiB7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAzMCU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgZm9udC1zaXplOiA0OHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogdGl0bGUgMnMgbGluZWFyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb24ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgcGFkZGluZzogMXJlbTtcclxuICAgICAgICBtYXJnaW46ICAxMCUgMDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjJzO1xyXG4gICAgICAgICY6Zmlyc3QtY2hpbGR7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogbWVzc2FnZTEgMnMgO1xyXG4gICAgICAgICAgICAmOjpiZWZvcmV7XHJcbiAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XHJcbiAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcclxuICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICBsZWZ0OiAtMTVweDtcclxuICAgICAgICAgICAgICAgdG9wOiAyMHB4O1xyXG4gICAgICAgICAgICAgICBjbGlwLXBhdGg6IHBvbHlnb24oMTAwJSAwLCAwIDAsIDEwMCUgMTAwJSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgJjpsYXN0LWNoaWxke1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgICAgYW5pbWF0aW9uOiBtZXNzYWdlMiAycyA7XHJcbiAgICAgICAgICAgICY6OmFmdGVyIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICByaWdodDogLTE1cHg7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICBjbGlwLXBhdGg6IHBvbHlnb24oMCAxMDAlLCAwIDAsIDEwMCUgMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MjBweDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgcmlnaHQ6IDEwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJGNoYXJpdHktcGhvbmUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA5JTtcclxuICAgICAgICBtYXgtd2lkdGg6IDQzMHB4O1xyXG5cclxuICAgICAgICAuY29udGFpbmVyLXBob25lIHtcclxuICAgICAgICAgICAgd2lkdGg6IDg1JTtcclxuXHJcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgICAgICAgICAgYSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzI1MjUzOTtcclxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDFyZW0gMS41cmVtO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbjogLjVyZW0gMDtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcblxyXG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaW5wdXQge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZjRmNGY3O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzcmVtO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMCAxcmVtO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG5cclxuICAgICAgICAgICAgICAgICY6Zm9jdXMge1xyXG4gICAgICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNoYXJpdHktY29udGFpbmVyX19jaGFyaXR5LXBob25lIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiA2MCU7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OiAxMCU7XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5Aa2V5ZnJhbWVzIG1lc3NhZ2UxIHtcclxuICAgIGZyb217XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xNTAlKTtcclxuICAgIH1cclxuICAgIHRve1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwJSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgbWVzc2FnZTIge1xyXG4gICAgZnJvbXtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMTUwJSk7XHJcbiAgICB9XHJcbiAgICB0b3tcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLy8gQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTQwMHB4KSB7XHJcbi8vICAgICAuY2hhcml0eS1jb250YWluZXIge1xyXG5cclxuLy8gICAgICAgICAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIge1xyXG4vLyAgICAgICAgICAgICBoZWlnaHQ6IDU3JTtcclxuLy8gICAgICAgICAgICAgcGFkZGluZy10b3A6IDEyJTtcclxuLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiA2MDBweDtcclxuXHJcbi8vICAgICAgICAgICAgIC5jb250YWluZXItcGhvbmUge1xyXG4vLyAgICAgICAgICAgICAgICAgaW5wdXQge1xyXG4vLyAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogNSUgMDtcclxuLy8gICAgICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuLy8gQG1lZGlhIChtaW4td2lkdGg6IDEyNDBweCkgYW5kIChtYXgtd2lkdGg6IDEzOTlweCkge1xyXG4vLyAgICAgLmNoYXJpdHktY29udGFpbmVyIHtcclxuXHJcbi8vICAgICAgICAgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiA3MSU7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMiU7XHJcbi8vICAgICAgICAgICAgIG1heC13aWR0aDogNDQ3cHg7XHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4vLyBAbWVkaWEgKG1pbi13aWR0aDogNDAwcHgpIGFuZCAobWF4LXdpZHRoOiAxMjQwcHgpIHtcclxuLy8gICAgIC5jaGFyaXR5LWNvbnRhaW5lciB7XHJcblxyXG4vLyAgICAgICAgIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciB7XHJcbi8vICAgICAgICAgICAgIGhlaWdodDogODAlO1xyXG4vLyAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMjIlO1xyXG4vLyAgICAgICAgICAgICBtYXgtd2lkdGg6IDQyMHB4O1xyXG5cclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vIH1cclxuXHJcbi8vIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDIwMDBweCkge1xyXG4vLyAgICAgLmNoYXJpdHktY29udGFpbmVyIHtcclxuXHJcbi8vICAgICAgICAgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiA1NSU7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMiU7XHJcbi8vICAgICAgICAgICAgIG1heC13aWR0aDogNTAwcHg7XHJcblxyXG4vLyAgICAgICAgICAgICAuY29udGFpbmVyLXBob25lIHtcclxuLy8gICAgICAgICAgICAgICAgIGlucHV0IHtcclxuLy8gICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDUlIDA7XHJcbi8vICAgICAgICAgICAgICAgICB9XHJcbi8vICAgICAgICAgICAgIH1cclxuLy8gICAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vIH1cclxuXHJcbi8vIEBtZWRpYSAobWluLXdpZHRoOiAzODBweCkgYW5kIChtYXgtd2lkdGg6IDc1MHB4KSB7XHJcbi8vICAgICAuY2hhcml0eS1jb250YWluZXIge1xyXG5cclxuLy8gICAgICAgICAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIge1xyXG4vLyAgICAgICAgICAgICBoZWlnaHQ6IDQ5JTtcclxuLy8gICAgICAgICAgICAgcGFkZGluZy10b3A6IDIyJTtcclxuLy8gICAgICAgICAgICAgbWF4LXdpZHRoOiA1MTlweDtcclxuXHJcbi8vICAgICAgICAgfVxyXG4vLyAgICAgfVxyXG4vLyB9XHJcblxyXG4vLyBAbWVkaWEgKG1pbi13aWR0aDogMzgwcHgpIGFuZCAobWF4LXdpZHRoOiA3MjBweCkge1xyXG4vLyAgICAgLmNoYXJpdHktY29udGFpbmVyIHtcclxuXHJcbi8vICAgICAgICAgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcclxuLy8gICAgICAgICAgICAgaGVpZ2h0OiA1MiU7XHJcbi8vICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAyMiU7XHJcbi8vICAgICAgICAgICAgIG1heC13aWR0aDogNTQxcHg7XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcblxyXG4vLyAgICAgICAgIH1cclxuLy8gICAgIH1cclxuLy8gfVxyXG4iLCIuY2hhcml0eS1jb250YWluZXIge1xuICBjb2xvcjogIzI1MjUzOTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDBkZWcsIHJnYmEoMCwgMCwgMCwgMC4zKSAwJSwgcmdiYSgwLCAwLCAwLCAwLjMpIDEwMCUpLCB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jaGFyaXR5L2NoYXJpdHktYmcucG5nXCIpLCAjZmZmO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktaGVhZGVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1oZWFkZXIgc3BhbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1oZWFkZXIgaDIge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLWJpZy5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAzMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiA0OHB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHRpdGxlIDJzIGxpbmVhcjtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX29wdGlvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm9yZGVyLXJhZGl1czogN3B4O1xuICBwYWRkaW5nOiAxcmVtO1xuICBtYXJnaW46IDEwJSAwO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4ycztcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX29wdGlvbjpmaXJzdC1jaGlsZCB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGFuaW1hdGlvbjogbWVzc2FnZTEgMnM7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb246Zmlyc3QtY2hpbGQ6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgd2lkdGg6IDE1cHg7XG4gIGhlaWdodDogMTVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMTVweDtcbiAgdG9wOiAyMHB4O1xuICBjbGlwLXBhdGg6IHBvbHlnb24oMTAwJSAwLCAwIDAsIDEwMCUgMTAwJSk7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX19vcHRpb246bGFzdC1jaGlsZCB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIGNvbG9yOiAjZmZmO1xuICBhbmltYXRpb246IG1lc3NhZ2UyIDJzO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fb3B0aW9uOmxhc3QtY2hpbGQ6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtMTVweDtcbiAgdG9wOiAyMHB4O1xuICBjbGlwLXBhdGg6IHBvbHlnb24oMCAxMDAlLCAwIDAsIDEwMCUgMCk7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDQyMHB4O1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAxMCU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2NoYXJpdHkvY2hhcml0eS1waG9uZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAwO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwYWRkaW5nLXRvcDogOSU7XG4gIG1heC13aWR0aDogNDMwcHg7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUge1xuICB3aWR0aDogODUlO1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX3dyYXBwZXIgLmNvbnRhaW5lci1waG9uZSBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogIzI1MjUzOTtcbiAgcGFkZGluZzogMXJlbSAxLjVyZW07XG4gIHdpZHRoOiAxMDAlO1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm94LXNoYWRvdzogMCAwIDIwcHggcmdiYSgwLCAwLCAwLCAwLjIpO1xuICBib3JkZXItcmFkaXVzOiA5cHg7XG4gIG1hcmdpbjogMC41cmVtIDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGNvbG9yOiAjZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uY2hhcml0eS1jb250YWluZXIgLmNoYXJpdHktY29udGFpbmVyX193cmFwcGVyIC5jb250YWluZXItcGhvbmUgaW5wdXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjZjRmNGY3O1xuICBoZWlnaHQ6IDNyZW07XG4gIHBhZGRpbmc6IDAgMXJlbTtcbiAgYm9yZGVyOiBub25lO1xufVxuLmNoYXJpdHktY29udGFpbmVyIC5jaGFyaXR5LWNvbnRhaW5lcl9fd3JhcHBlciAuY29udGFpbmVyLXBob25lIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5jaGFyaXR5LWNvbnRhaW5lciAuY2hhcml0eS1jb250YWluZXJfX2NoYXJpdHktcGhvbmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogNjAlO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAxMCU7XG59XG5cbkBrZXlmcmFtZXMgbWVzc2FnZTEge1xuICBmcm9tIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTE1MCUpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCUpO1xuICB9XG59XG5Aa2V5ZnJhbWVzIG1lc3NhZ2UyIHtcbiAgZnJvbSB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDE1MCUpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpO1xuICB9XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/charity/charity.component.ts": 
        /*!**********************************************************************!*\
          !*** ./src/app/modules/home/components/charity/charity.component.ts ***!
          \**********************************************************************/
        /*! exports provided: CharityComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharityComponent", function () { return CharityComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var CharityComponent = /** @class */ (function () {
                function CharityComponent(store) {
                    this.store = store;
                    this.charityPhone = '/assets/images/charity/charity-phone.png';
                }
                CharityComponent.prototype.ngOnInit = function () {
                };
                return CharityComponent;
            }());
            CharityComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            CharityComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-charity',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./charity.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/charity/charity.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./charity.component.scss */ "./src/app/modules/home/components/charity/charity.component.scss")).default]
                })
            ], CharityComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.scss": 
        /*!***********************************************************************************************!*\
          !*** ./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.scss ***!
          \***********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".contacts-inner-container {\n  background: #fff;\n}\n.contacts-inner-container .contacts-container__title {\n  padding: 0 5%;\n  padding-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n.contacts-inner-container .contacts-container__title h3 {\n  display: inline-block;\n  width: 105%;\n  background: url(\"/assets/images/underline.png\");\n  background-repeat: no-repeat;\n  background-position-x: right;\n  background-position-y: 89%;\n  margin-bottom: 15%;\n  font-size: 2rem;\n}\n.contacts-inner-container .contacts-container__title p {\n  margin-bottom: 0;\n}\n.contacts-inner-container .contacts-container__title p:first-child {\n  color: #bfbfbf;\n}\n.contacts-inner-container .contacts-container__title .contacts-container__title--redline {\n  padding-left: 1rem;\n  position: relative;\n}\n.contacts-inner-container .contacts-container__title .contacts-container__title--redline::before {\n  content: \"\";\n  display: inline-block;\n  position: absolute;\n  left: -12px;\n  top: 50%;\n  width: 20px;\n  height: 2px;\n  background: #d4002d;\n}\n.contacts-inner-container .legal-info {\n  padding: 0 5%;\n  margin-bottom: 1rem;\n  text-transform: uppercase;\n}\n.contacts-inner-container .legal-info h2 {\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n.contacts-inner-container .legal-info p {\n  margin-bottom: 0;\n}\n.contacts-inner-container .legal-info p:nth-child(3) {\n  margin-bottom: 1rem;\n}\n.contacts-inner-container .legal-info p:nth-child(8) {\n  margin-bottom: 1rem;\n}\n.contacts-inner-container .contacts-container__address--padding {\n  padding: 0 2%;\n}\n.contacts-inner-container__submitQ {\n  background: #b5e5f9;\n  background-image: url(\"/assets/images/contacts/contacts-inner/envelope.png\");\n  background-position-x: 100%;\n  background-size: cover;\n  background-repeat: no-repeat;\n  padding: 4% 35% 4% 4%;\n  margin: 2rem;\n  text-align: left;\n}\n.contacts-inner-container__submitQ h2 {\n  width: 100%;\n}\n.contacts-inner-container__submitQ input {\n  width: 49%;\n  background: transparent;\n  border: none;\n  border-bottom: 1px solid #000;\n  display: inline-block;\n  margin: auto;\n  padding: 0 1rem;\n  padding-top: 1rem;\n  padding-bottom: 0.5rem;\n}\n.contacts-inner-container__submitQ input:focus {\n  outline: none;\n}\n.contacts-inner-container__submitQ .input-wrapper {\n  display: flex;\n  justify-content: flex-start;\n}\n.contacts-inner-container__submitQ textarea {\n  width: 100%;\n  background: transparent;\n  border: none;\n  border-bottom: 1px solid #000;\n  resize: none;\n  padding: 0 1rem;\n  padding-top: 1rem;\n  padding-bottom: 0.5rem;\n}\n.contacts-inner-container__submitQ textarea:focus {\n  outline: none;\n}\n.contacts-inner-container__submitQ a {\n  text-transform: uppercase;\n  padding: 0.3rem 2rem;\n  background: #fff;\n  color: inherit;\n  padding: 0.8rem 3rem;\n  display: inline-block;\n  border-radius: 4px;\n  margin-top: 5%;\n  font-size: 12px;\n  font-weight: bold;\n}\n.contacts-inner-container__submitQ a:hover {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvY29udGFjdHMvY29udGFjdHMtaW5uZXIvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxjb250YWN0c1xcY29udGFjdHMtaW5uZXJcXGNvbnRhY3RzLWlubmVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9jb250YWN0cy9jb250YWN0cy1pbm5lci9jb250YWN0cy1pbm5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNDLGdCQUFBO0FDREQ7QURFSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHFCQUFBO0FDQVI7QURDUTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLCtDQUFBO0VBQ0EsNEJBQUE7RUFDQSw0QkFBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDQ1o7QURDUTtFQUVJLGdCQUFBO0FDQVo7QURDWTtFQUNJLGNBQUE7QUNDaEI7QURHUTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7QUNEWjtBREVZO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUNBaEI7QURLSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FDSFI7QURJUTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7QUNGWjtBRElRO0VBQ0ksZ0JBQUE7QUNGWjtBRElZO0VBQ0ksbUJBQUE7QUNGaEI7QURJWTtFQUNJLG1CQUFBO0FDRmhCO0FET0k7RUFDSSxhQUFBO0FDTFI7QURTQTtFQUNJLG1CQUFBO0VBQ0EsNEVBQUE7RUFFQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQ1BKO0FEU0k7RUFDSSxXQUFBO0FDUFI7QURVSTtFQUNJLFVBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0FDUlI7QURTUTtFQUNJLGFBQUE7QUNQWjtBRFdJO0VBQ0ksYUFBQTtFQUNBLDJCQUFBO0FDVFI7QURhSTtFQUNJLFdBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ1hSO0FEWVE7RUFDSSxhQUFBO0FDVlo7QURjSTtFQUNJLHlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDWlI7QURjUTtFQUNJLHFCQUFBO0FDWloiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9jb250YWN0cy9jb250YWN0cy1pbm5lci9jb250YWN0cy1pbm5lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRtYXBTdWJzY3I6ICcvYXNzZXRzL2ltYWdlcy9jb250YWN0cy9jb250YWN0cy1pbm5lci9lbnZlbG9wZS5wbmcnO1xyXG4kdW5kZXJsaW5lOicvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUucG5nJztcclxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lciB7XHJcbiBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgNSU7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IC41cmVtO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IC41cmVtO1xyXG4gICAgICAgIGgze1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDUlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSk7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogcmlnaHQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODklO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxNSU7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbVxyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgJjpmaXJzdC1jaGlsZHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjYmZiZmJmO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb250YWN0cy1jb250YWluZXJfX3RpdGxlLS1yZWRsaW5le1xyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAtMTJweDtcclxuICAgICAgICAgICAgICAgIHRvcDogNTAlO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDJweDtcclxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNkNDAwMmQgO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5sZWdhbC1pbmZvIHtcclxuICAgICAgICBwYWRkaW5nOiAwIDUlO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBoMntcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMXJlbTtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuXHJcbiAgICAgICAgICAgICY6bnRoLWNoaWxkKDMpe1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAmOm50aC1jaGlsZCg4KXtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhY3RzLWNvbnRhaW5lcl9fYWRkcmVzcy0tcGFkZGluZyB7XHJcbiAgICAgICAgcGFkZGluZzogMCAyJTtcclxuICAgIH1cclxufVxyXG5cclxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lcl9fc3VibWl0USB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjYjVlNWY5O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCRtYXBTdWJzY3IpO1xyXG5cclxuICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgcGFkZGluZzogNCUgMzUlIDQlIDQlO1xyXG4gICAgbWFyZ2luOiAycmVtO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgaW5wdXQge1xyXG4gICAgICAgIHdpZHRoOiA0OSU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBtYXJnaW46ICBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMXJlbSA7XHJcbiAgICAgICAgcGFkZGluZy10b3A6MXJlbSA7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IC41cmVtIDtcclxuICAgICAgICAmOmZvY3Vze1xyXG4gICAgICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaW5wdXQtd3JhcHBlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICB0ZXh0YXJlYSB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgICAgIHJlc2l6ZTogbm9uZTtcclxuICAgICAgICBwYWRkaW5nOiAwIDFyZW0gO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOjFyZW0gO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAuNXJlbSA7XHJcbiAgICAgICAgJjpmb2N1c3tcclxuICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYSB7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBwYWRkaW5nOiAuM3JlbSAycmVtO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgY29sb3I6IGluaGVyaXQ7XHJcbiAgICAgICAgcGFkZGluZzogLjhyZW0gM3JlbTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHJcbiAgICAgICAgJjpob3ZlcntcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuIiwiLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyIC5jb250YWN0cy1jb250YWluZXJfX3RpdGxlIHtcbiAgcGFkZGluZzogMCA1JTtcbiAgcGFkZGluZy10b3A6IDAuNXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lciAuY29udGFjdHMtY29udGFpbmVyX190aXRsZSBoMyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwNSU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogcmlnaHQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teTogODklO1xuICBtYXJnaW4tYm90dG9tOiAxNSU7XG4gIGZvbnQtc2l6ZTogMnJlbTtcbn1cbi5jb250YWN0cy1pbm5lci1jb250YWluZXIgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUgcCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyIC5jb250YWN0cy1jb250YWluZXJfX3RpdGxlIHA6Zmlyc3QtY2hpbGQge1xuICBjb2xvcjogI2JmYmZiZjtcbn1cbi5jb250YWN0cy1pbm5lci1jb250YWluZXIgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUtLXJlZGxpbmUge1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5jb250YWN0cy1pbm5lci1jb250YWluZXIgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUgLmNvbnRhY3RzLWNvbnRhaW5lcl9fdGl0bGUtLXJlZGxpbmU6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAtMTJweDtcbiAgdG9wOiA1MCU7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZDogI2Q0MDAyZDtcbn1cbi5jb250YWN0cy1pbm5lci1jb250YWluZXIgLmxlZ2FsLWluZm8ge1xuICBwYWRkaW5nOiAwIDUlO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lciAubGVnYWwtaW5mbyBoMiB7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyIC5sZWdhbC1pbmZvIHAge1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lciAubGVnYWwtaW5mbyBwOm50aC1jaGlsZCgzKSB7XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyIC5sZWdhbC1pbmZvIHA6bnRoLWNoaWxkKDgpIHtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cbi5jb250YWN0cy1pbm5lci1jb250YWluZXIgLmNvbnRhY3RzLWNvbnRhaW5lcl9fYWRkcmVzcy0tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDAgMiU7XG59XG5cbi5jb250YWN0cy1pbm5lci1jb250YWluZXJfX3N1Ym1pdFEge1xuICBiYWNrZ3JvdW5kOiAjYjVlNWY5O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jb250YWN0cy9jb250YWN0cy1pbm5lci9lbnZlbG9wZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgcGFkZGluZzogNCUgMzUlIDQlIDQlO1xuICBtYXJnaW46IDJyZW07XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyX19zdWJtaXRRIGgyIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyX19zdWJtaXRRIGlucHV0IHtcbiAgd2lkdGg6IDQ5JTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwMDA7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiBhdXRvO1xuICBwYWRkaW5nOiAwIDFyZW07XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMC41cmVtO1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lcl9fc3VibWl0USBpbnB1dDpmb2N1cyB7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyX19zdWJtaXRRIC5pbnB1dC13cmFwcGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lcl9fc3VibWl0USB0ZXh0YXJlYSB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiBub25lO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzAwMDtcbiAgcmVzaXplOiBub25lO1xuICBwYWRkaW5nOiAwIDFyZW07XG4gIHBhZGRpbmctdG9wOiAxcmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMC41cmVtO1xufVxuLmNvbnRhY3RzLWlubmVyLWNvbnRhaW5lcl9fc3VibWl0USB0ZXh0YXJlYTpmb2N1cyB7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyX19zdWJtaXRRIGEge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nOiAwLjNyZW0gMnJlbTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6IGluaGVyaXQ7XG4gIHBhZGRpbmc6IDAuOHJlbSAzcmVtO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgbWFyZ2luLXRvcDogNSU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uY29udGFjdHMtaW5uZXItY29udGFpbmVyX19zdWJtaXRRIGE6aG92ZXIge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.ts": 
        /*!*********************************************************************************************!*\
          !*** ./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.ts ***!
          \*********************************************************************************************/
        /*! exports provided: ContactsInnerComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsInnerComponent", function () { return ContactsInnerComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ContactsInnerComponent = /** @class */ (function () {
                function ContactsInnerComponent() {
                }
                ContactsInnerComponent.prototype.ngOnInit = function () {
                };
                return ContactsInnerComponent;
            }());
            ContactsInnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-contacts-inner',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contacts-inner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contacts-inner.component.scss */ "./src/app/modules/home/components/contacts/contacts-inner/contacts-inner.component.scss")).default]
                })
            ], ContactsInnerComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/contacts/contacts.component.scss": 
        /*!**************************************************************************!*\
          !*** ./src/app/modules/home/components/contacts/contacts.component.scss ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".contacts-container {\n  height: 100vh;\n  position: relative;\n  overflow: hidden;\n  background: url(\"/assets/images/contacts/contacts-bg.png\");\n  background-size: cover;\n}\n.contacts-container .contacts-header {\n  width: 100%;\n  height: 10%;\n  text-transform: uppercase;\n  text-align: center;\n  position: relative;\n}\n.contacts-container .contacts-header a {\n  color: inherit;\n}\n.contacts-container .contacts-header h2 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 20%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 48px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\nspan {\n  position: relative;\n}\n.contacts-content {\n  margin-top: 10%;\n}\n.contacts-content h2 {\n  font-weight: bold;\n}\n.contacts-content .contacts-content__tel {\n  -webkit-animation: tel 2s;\n          animation: tel 2s;\n}\n.contacts-content .contacts-content__email {\n  -webkit-animation: email 3s;\n          animation: email 3s;\n  font-weight: bold;\n}\n.contacts-content .contacts-content__work-time {\n  color: #969697;\n  -webkit-animation: time 4s;\n          animation: time 4s;\n}\n.contacts-content span {\n  font-weight: bold;\n  text-transform: lowercase;\n}\n.contacts-content .input-container {\n  z-index: 1;\n}\n.contacts-content .input-container input {\n  width: 75%;\n  border: none;\n  padding: 0.8rem;\n  color: #969697;\n}\n.contacts-content .input-container input:focus {\n  outline: none;\n}\n.contacts-content .input-container .input-container__btn {\n  padding: 0.8rem 0.8rem;\n  background: #212529;\n  color: #fff;\n  margin-left: -8px;\n  border-radius: 7px;\n}\n.contacts-content .contacts-content__sub-comment {\n  padding-left: 1.5rem;\n}\n.contacts-content .contacts-content__sub-comment span {\n  text-transform: unset;\n}\n.contacts-content__footer {\n  padding-top: 1rem;\n  -webkit-animation: footer 2s;\n          animation: footer 2s;\n}\n.contacts-content__footer h2 {\n  width: 100%;\n  text-transform: uppercase;\n  font-size: 16px;\n  margin-bottom: 1rem;\n}\n.contacts-content__footer a {\n  width: 100%;\n  display: inline-block;\n  color: #212529;\n  margin-bottom: 0.8rem;\n}\n.contacts-content__footer a:hover {\n  text-decoration: none;\n}\n#ball1 {\n  position: absolute;\n  transition: all 0.3s;\n  left: 0%;\n  z-index: 0;\n  -webkit-animation: ball1 2s 0.1s linear;\n          animation: ball1 2s 0.1s linear;\n  transition: all 0.1s;\n  top: 55%;\n}\n@-webkit-keyframes footer {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes footer {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@-webkit-keyframes email {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes email {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@-webkit-keyframes time {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes time {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@-webkit-keyframes tel {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes tel {\n  from {\n    transform: translateX(-100%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@-webkit-keyframes ball1 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 40%;\n  }\n  50% {\n    top: 80%;\n  }\n  75% {\n    top: 40%;\n  }\n  100% {\n    top: 55%;\n  }\n}\n@keyframes ball1 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 40%;\n  }\n  50% {\n    top: 80%;\n  }\n  75% {\n    top: 40%;\n  }\n  100% {\n    top: 55%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvY29udGFjdHMvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxjb250YWN0c1xcY29udGFjdHMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2NvbnRhY3RzL2NvbnRhY3RzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBRUksYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwREFBQTtFQUNBLHNCQUFBO0FDSEo7QURLSTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDSFI7QURJUTtFQUNJLGNBQUE7QUNGWjtBREtRO0VBQ0ksaUJBQUE7RUFDQSw2REFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7RUFDQSwwQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLDJCQUFBO1VBQUEsbUJBQUE7QUNIWjtBRFFBO0VBQ0ksa0JBQUE7QUNMSjtBRFVBO0VBRUksZUFBQTtBQ1JKO0FEVUk7RUFDSSxpQkFBQTtBQ1JSO0FEV0k7RUFDSSx5QkFBQTtVQUFBLGlCQUFBO0FDVFI7QURZSTtFQUNJLDJCQUFBO1VBQUEsbUJBQUE7RUFDQSxpQkFBQTtBQ1ZSO0FEYUk7RUFDSSxjQUFBO0VBQ0EsMEJBQUE7VUFBQSxrQkFBQTtBQ1hSO0FEY0k7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0FDWlI7QURlSTtFQUNJLFVBQUE7QUNiUjtBRGNRO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ1paO0FEY1k7RUFDSSxhQUFBO0FDWmhCO0FEaUJRO0VBQ0ksc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDZlo7QURtQkk7RUFDSSxvQkFBQTtBQ2pCUjtBRG1CUTtFQUNJLHFCQUFBO0FDakJaO0FEd0JBO0VBQ0ksaUJBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0FDckJKO0FEc0JJO0VBQ0ksV0FBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDcEJSO0FEd0JJO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0FDdEJSO0FEd0JRO0VBQ0kscUJBQUE7QUN0Qlo7QUQwQkE7RUFDSSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSx1Q0FBQTtVQUFBLCtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxRQUFBO0FDdkJKO0FEMEJBO0VBQ0k7SUFDSSw0QkFBQTtFQ3ZCTjtFRDBCRTtJQUNJLHlCQUFBO0VDeEJOO0FBQ0Y7QURpQkE7RUFDSTtJQUNJLDRCQUFBO0VDdkJOO0VEMEJFO0lBQ0kseUJBQUE7RUN4Qk47QUFDRjtBRDJCQTtFQUNJO0lBQ0ksNEJBQUE7RUN6Qk47RUQ0QkU7SUFDSSx5QkFBQTtFQzFCTjtBQUNGO0FEbUJBO0VBQ0k7SUFDSSw0QkFBQTtFQ3pCTjtFRDRCRTtJQUNJLHlCQUFBO0VDMUJOO0FBQ0Y7QUQ2QkE7RUFDSTtJQUNJLDRCQUFBO0VDM0JOO0VEOEJFO0lBQ0kseUJBQUE7RUM1Qk47QUFDRjtBRHFCQTtFQUNJO0lBQ0ksNEJBQUE7RUMzQk47RUQ4QkU7SUFDSSx5QkFBQTtFQzVCTjtBQUNGO0FEK0JBO0VBQ0k7SUFDSSw0QkFBQTtFQzdCTjtFRGdDRTtJQUNJLHlCQUFBO0VDOUJOO0FBQ0Y7QUR1QkE7RUFDSTtJQUNJLDRCQUFBO0VDN0JOO0VEZ0NFO0lBQ0kseUJBQUE7RUM5Qk47QUFDRjtBRGtDQTtFQUNJO0lBQ0ksU0FBQTtFQ2hDTjtFRGtDRTtJQUNJLFFBQUE7RUNoQ047RURrQ0U7SUFDSSxRQUFBO0VDaENOO0VEa0NFO0lBQ0ksUUFBQTtFQ2hDTjtFRGtDRTtJQUNJLFFBQUE7RUNoQ047QUFDRjtBRGlCQTtFQUNJO0lBQ0ksU0FBQTtFQ2hDTjtFRGtDRTtJQUNJLFFBQUE7RUNoQ047RURrQ0U7SUFDSSxRQUFBO0VDaENOO0VEa0NFO0lBQ0ksUUFBQTtFQ2hDTjtFRGtDRTtJQUNJLFFBQUE7RUNoQ047QUFDRiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL2NvbnRhY3RzL2NvbnRhY3RzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGNvbnRhY3QtYmc6ICcvYXNzZXRzL2ltYWdlcy9jb250YWN0cy9jb250YWN0cy1iZy5wbmcnO1xyXG4kdW5kZXJsaW5lOiAnL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLWJpZy5wbmcnO1xyXG5cclxuLmNvbnRhY3RzLWNvbnRhaW5lciB7XHJcblxyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJGNvbnRhY3QtYmcpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHJcbiAgICAuY29udGFjdHMtaGVhZGVyIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwJTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYXtcclxuICAgICAgICAgICAgY29sb3I6IGluaGVyaXQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBoMiB7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAyMCU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgZm9udC1zaXplOiA0OHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogdGl0bGUgMnM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5zcGFuIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuXHJcblxyXG4uY29udGFjdHMtY29udGVudCB7XHJcblxyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGFjdHMtY29udGVudF9fdGVse1xyXG4gICAgICAgIGFuaW1hdGlvbjogdGVsIDJzO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250YWN0cy1jb250ZW50X19lbWFpbCB7XHJcbiAgICAgICAgYW5pbWF0aW9uOiBlbWFpbCAzcztcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGFjdHMtY29udGVudF9fd29yay10aW1lIHtcclxuICAgICAgICBjb2xvcjogIzk2OTY5NztcclxuICAgICAgICBhbmltYXRpb246IHRpbWUgNHM7XHJcbiAgICB9XHJcblxyXG4gICAgc3BhbiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGxvd2VyY2FzZTtcclxuICAgIH1cclxuXHJcbiAgICAuaW5wdXQtY29udGFpbmVyIHtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgICAgIGlucHV0IHtcclxuICAgICAgICAgICAgd2lkdGg6IDc1JTtcclxuICAgICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAuOHJlbTtcclxuICAgICAgICAgICAgY29sb3I6ICM5Njk2OTc7XHJcblxyXG4gICAgICAgICAgICAmOmZvY3VzIHtcclxuICAgICAgICAgICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAuaW5wdXQtY29udGFpbmVyX19idG4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAuOHJlbSAuOHJlbTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzIxMjUyOTtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtOHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5jb250YWN0cy1jb250ZW50X19zdWItY29tbWVudCB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxLjVyZW07XHJcblxyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb250YWN0cy1jb250ZW50X19mb290ZXIge1xyXG4gICAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgICBhbmltYXRpb246IGZvb3RlciAycztcclxuICAgIGgyIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBhIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgY29sb3I6ICMyMTI1Mjk7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLjhyZW07XHJcblxyXG4gICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiNiYWxsMSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgbGVmdDogMCU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgYW5pbWF0aW9uOiBiYWxsMSAycyAuMXMgbGluZWFyO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4xcyA7XHJcbiAgICB0b3A6IDU1JTtcclxufVxyXG5cclxuQGtleWZyYW1lcyBmb290ZXIge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDAlKTtcclxuICAgIH1cclxufVxyXG5cclxuQGtleWZyYW1lcyBlbWFpbCB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHRpbWUge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDAlKTtcclxuICAgIH1cclxufVxyXG5cclxuQGtleWZyYW1lcyB0ZWwge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTtcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDAlKTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbkBrZXlmcmFtZXMgYmFsbDEge1xyXG4gICAgMCUge1xyXG4gICAgICAgIHRvcDogLTUwJTtcclxuICAgIH1cclxuICAgIDI1JXtcclxuICAgICAgICB0b3A6IDQwJTtcclxuICAgIH1cclxuICAgIDUwJXtcclxuICAgICAgICB0b3A6IDgwJTtcclxuICAgIH1cclxuICAgIDc1JXtcclxuICAgICAgICB0b3A6IDQwJTtcclxuICAgIH1cclxuICAgIDEwMCV7XHJcbiAgICAgICAgdG9wOiA1NSU7XHJcbiAgICB9XHJcbn0iLCIuY29udGFjdHMtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jb250YWN0cy9jb250YWN0cy1iZy5wbmdcIik7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4uY29udGFjdHMtY29udGFpbmVyIC5jb250YWN0cy1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMCU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmNvbnRhY3RzLWNvbnRhaW5lciAuY29udGFjdHMtaGVhZGVyIGEge1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbi5jb250YWN0cy1jb250YWluZXIgLmNvbnRhY3RzLWhlYWRlciBoMiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUtYmlnLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogODAlIDIwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDQ4cHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1bnNldDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGFuaW1hdGlvbjogdGl0bGUgMnM7XG59XG5cbnNwYW4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5jb250YWN0cy1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMTAlO1xufVxuLmNvbnRhY3RzLWNvbnRlbnQgaDIge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5jb250YWN0cy1jb250ZW50IC5jb250YWN0cy1jb250ZW50X190ZWwge1xuICBhbmltYXRpb246IHRlbCAycztcbn1cbi5jb250YWN0cy1jb250ZW50IC5jb250YWN0cy1jb250ZW50X19lbWFpbCB7XG4gIGFuaW1hdGlvbjogZW1haWwgM3M7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLmNvbnRhY3RzLWNvbnRlbnQgLmNvbnRhY3RzLWNvbnRlbnRfX3dvcmstdGltZSB7XG4gIGNvbG9yOiAjOTY5Njk3O1xuICBhbmltYXRpb246IHRpbWUgNHM7XG59XG4uY29udGFjdHMtY29udGVudCBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2U7XG59XG4uY29udGFjdHMtY29udGVudCAuaW5wdXQtY29udGFpbmVyIHtcbiAgei1pbmRleDogMTtcbn1cbi5jb250YWN0cy1jb250ZW50IC5pbnB1dC1jb250YWluZXIgaW5wdXQge1xuICB3aWR0aDogNzUlO1xuICBib3JkZXI6IG5vbmU7XG4gIHBhZGRpbmc6IDAuOHJlbTtcbiAgY29sb3I6ICM5Njk2OTc7XG59XG4uY29udGFjdHMtY29udGVudCAuaW5wdXQtY29udGFpbmVyIGlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbi5jb250YWN0cy1jb250ZW50IC5pbnB1dC1jb250YWluZXIgLmlucHV0LWNvbnRhaW5lcl9fYnRuIHtcbiAgcGFkZGluZzogMC44cmVtIDAuOHJlbTtcbiAgYmFja2dyb3VuZDogIzIxMjUyOTtcbiAgY29sb3I6ICNmZmY7XG4gIG1hcmdpbi1sZWZ0OiAtOHB4O1xuICBib3JkZXItcmFkaXVzOiA3cHg7XG59XG4uY29udGFjdHMtY29udGVudCAuY29udGFjdHMtY29udGVudF9fc3ViLWNvbW1lbnQge1xuICBwYWRkaW5nLWxlZnQ6IDEuNXJlbTtcbn1cbi5jb250YWN0cy1jb250ZW50IC5jb250YWN0cy1jb250ZW50X19zdWItY29tbWVudCBzcGFuIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVuc2V0O1xufVxuXG4uY29udGFjdHMtY29udGVudF9fZm9vdGVyIHtcbiAgcGFkZGluZy10b3A6IDFyZW07XG4gIGFuaW1hdGlvbjogZm9vdGVyIDJzO1xufVxuLmNvbnRhY3RzLWNvbnRlbnRfX2Zvb3RlciBoMiB7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG59XG4uY29udGFjdHMtY29udGVudF9fZm9vdGVyIGEge1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBjb2xvcjogIzIxMjUyOTtcbiAgbWFyZ2luLWJvdHRvbTogMC44cmVtO1xufVxuLmNvbnRhY3RzLWNvbnRlbnRfX2Zvb3RlciBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4jYmFsbDEge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBsZWZ0OiAwJTtcbiAgei1pbmRleDogMDtcbiAgYW5pbWF0aW9uOiBiYWxsMSAycyAwLjFzIGxpbmVhcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXM7XG4gIHRvcDogNTUlO1xufVxuXG5Aa2V5ZnJhbWVzIGZvb3RlciB7XG4gIGZyb20ge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XG4gIH1cbiAgdG8ge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwJSk7XG4gIH1cbn1cbkBrZXlmcmFtZXMgZW1haWwge1xuICBmcm9tIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCUpO1xuICB9XG59XG5Aa2V5ZnJhbWVzIHRpbWUge1xuICBmcm9tIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwMCUpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCUpO1xuICB9XG59XG5Aa2V5ZnJhbWVzIHRlbCB7XG4gIGZyb20ge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7XG4gIH1cbiAgdG8ge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwJSk7XG4gIH1cbn1cbkBrZXlmcmFtZXMgYmFsbDEge1xuICAwJSB7XG4gICAgdG9wOiAtNTAlO1xuICB9XG4gIDI1JSB7XG4gICAgdG9wOiA0MCU7XG4gIH1cbiAgNTAlIHtcbiAgICB0b3A6IDgwJTtcbiAgfVxuICA3NSUge1xuICAgIHRvcDogNDAlO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogNTUlO1xuICB9XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/contacts/contacts.component.ts": 
        /*!************************************************************************!*\
          !*** ./src/app/modules/home/components/contacts/contacts.component.ts ***!
          \************************************************************************/
        /*! exports provided: ContactsComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsComponent", function () { return ContactsComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var ContactsComponent = /** @class */ (function () {
                function ContactsComponent(store) {
                    this.store = store;
                    this.ball1 = "/assets/images/projects/ball-left.png";
                }
                ContactsComponent.prototype.ngOnInit = function () {
                };
                return ContactsComponent;
            }());
            ContactsComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            ContactsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-contacts',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contacts.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/contacts/contacts.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contacts.component.scss */ "./src/app/modules/home/components/contacts/contacts.component.scss")).default]
                })
            ], ContactsComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/map/map.component.scss": 
        /*!****************************************************************!*\
          !*** ./src/app/modules/home/components/map/map.component.scss ***!
          \****************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".map-container {\n  height: 100vh;\n  position: relative;\n}\n.map-container .map-container__absolute-title {\n  background: transparent;\n  position: absolute;\n  font-weight: bold;\n  display: inline-block;\n  margin-bottom: 0;\n  z-index: 2;\n  top: 5%;\n  left: 50%;\n  transform: translate(-50%, 0%);\n}\n.map-container .map-container__absolute-title h2 {\n  font-size: 42px;\n  width: 110%;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.map-container img {\n  width: 100%;\n  height: 100%;\n}\n.map-container .map-container__absolute-sub {\n  position: absolute;\n  height: 230px;\n  width: 100%;\n  bottom: 0;\n  z-index: 2;\n  background: #b5e5f9;\n  background-image: url(\"/assets/images/map/subscribtion.png\");\n  background-position-x: 100%;\n  padding-left: 7%;\n  padding-top: 1%;\n  -webkit-animation: sub 2s;\n          animation: sub 2s;\n}\n.map-container .map-container__absolute-sub a {\n  background: #fff;\n  padding: 1rem 2rem;\n  border-radius: 7px;\n  color: inherit;\n  font-weight: bold;\n  display: inline-block;\n  margin-top: 2%;\n}\n.map-container .map-container__absolute-sub h2 {\n  width: 65%;\n}\n@-webkit-keyframes sub {\n  0% {\n    transform: translateY(105%);\n  }\n  50% {\n    transform: translateY(105%);\n  }\n  100% {\n    transform: translateY(0%);\n  }\n}\n@keyframes sub {\n  0% {\n    transform: translateY(105%);\n  }\n  50% {\n    transform: translateY(105%);\n  }\n  100% {\n    transform: translateY(0%);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbWFwL0M6XFxPU1BhbmVsXFxkb21haW5zXFxhX2x1eF9fY2FyaXRhc1xcY2FyaXRhcy9zcmNcXGFwcFxcbW9kdWxlc1xcaG9tZVxcY29tcG9uZW50c1xcbWFwXFxtYXAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL21hcC9tYXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBS0E7RUFFSSxhQUFBO0VBQ0Esa0JBQUE7QUNMSjtBRE9JO0VBQ0ksdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBRUEscUJBQUE7RUFDQSxnQkFBQTtFQUVBLFVBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLDhCQUFBO0FDUFI7QURRUTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EseURBQUE7RUFDQSwyQkFBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7RUFDQSwyQkFBQTtVQUFBLG1CQUFBO0FDTlo7QURVSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDUlI7QURZSTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsNERBQUE7RUFDQSwyQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO1VBQUEsaUJBQUE7QUNWUjtBRFlRO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDVlo7QURhUTtFQUNJLFVBQUE7QUNYWjtBRGdCQTtFQUNJO0lBQ0ksMkJBQUE7RUNiTjtFRGVFO0lBQ0ksMkJBQUE7RUNiTjtFRGVFO0lBQ0kseUJBQUE7RUNiTjtBQUNGO0FESUE7RUFDSTtJQUNJLDJCQUFBO0VDYk47RURlRTtJQUNJLDJCQUFBO0VDYk47RURlRTtJQUNJLHlCQUFBO0VDYk47QUFDRiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL21hcC9tYXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkbWFwU3Vic2NyOiAnL2Fzc2V0cy9pbWFnZXMvbWFwL3N1YnNjcmlidGlvbi5wbmcnO1xyXG5cclxuJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmcnO1xyXG4kYmFsbDogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LWlubmVyL2JhbGxvbnMucG5nJztcclxuXHJcbi5tYXAtY29udGFpbmVyIHtcclxuXHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgIC5tYXAtY29udGFpbmVyX19hYnNvbHV0ZS10aXRsZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgXHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgXHJcbiAgICAgICAgei1pbmRleDogMjtcclxuICAgICAgICB0b3A6IDUlO1xyXG4gICAgICAgIGxlZnQ6IDUwJTtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwJSk7XHJcbiAgICAgICAgaDJ7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNDJweDtcclxuICAgICAgICAgICAgd2lkdGg6IDExMCU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IHVybCgkdW5kZXJsaW5lKSBuby1yZXBlYXQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogdGl0bGUgMnM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAubWFwLWNvbnRhaW5lcl9fYWJzb2x1dGUtc3ViIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgaGVpZ2h0OiAyMzBweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgei1pbmRleDogMjtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjYjVlNWY5O1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgkbWFwU3Vic2NyKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA3JTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMSU7XHJcbiAgICAgICAgYW5pbWF0aW9uOiBzdWIgMnMgO1xyXG5cclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICAgICAgcGFkZGluZzogMXJlbSAycmVtO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgICAgd2lkdGg6IDY1JTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgc3ViIHtcclxuICAgIDAle1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMDUlKTtcclxuICAgIH1cclxuICAgIDUwJXtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMTA1JSk7XHJcbiAgICB9XHJcbiAgICAxMDAle1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgwJSk7XHJcbiAgICB9XHJcbn0iLCIubWFwLWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5tYXAtY29udGFpbmVyIC5tYXAtY29udGFpbmVyX19hYnNvbHV0ZS10aXRsZSB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHotaW5kZXg6IDI7XG4gIHRvcDogNSU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgMCUpO1xufVxuLm1hcC1jb250YWluZXIgLm1hcC1jb250YWluZXJfX2Fic29sdXRlLXRpdGxlIGgyIHtcbiAgZm9udC1zaXplOiA0MnB4O1xuICB3aWR0aDogMTEwJTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHRpdGxlIDJzO1xufVxuLm1hcC1jb250YWluZXIgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cbi5tYXAtY29udGFpbmVyIC5tYXAtY29udGFpbmVyX19hYnNvbHV0ZS1zdWIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjMwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHotaW5kZXg6IDI7XG4gIGJhY2tncm91bmQ6ICNiNWU1Zjk7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaW1hZ2VzL21hcC9zdWJzY3JpYnRpb24ucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDEwMCU7XG4gIHBhZGRpbmctbGVmdDogNyU7XG4gIHBhZGRpbmctdG9wOiAxJTtcbiAgYW5pbWF0aW9uOiBzdWIgMnM7XG59XG4ubWFwLWNvbnRhaW5lciAubWFwLWNvbnRhaW5lcl9fYWJzb2x1dGUtc3ViIGEge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBwYWRkaW5nOiAxcmVtIDJyZW07XG4gIGJvcmRlci1yYWRpdXM6IDdweDtcbiAgY29sb3I6IGluaGVyaXQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi10b3A6IDIlO1xufVxuLm1hcC1jb250YWluZXIgLm1hcC1jb250YWluZXJfX2Fic29sdXRlLXN1YiBoMiB7XG4gIHdpZHRoOiA2NSU7XG59XG5cbkBrZXlmcmFtZXMgc3ViIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMDUlKTtcbiAgfVxuICA1MCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMDUlKTtcbiAgfVxuICAxMDAlIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpO1xuICB9XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/map/map.component.ts": 
        /*!**************************************************************!*\
          !*** ./src/app/modules/home/components/map/map.component.ts ***!
          \**************************************************************/
        /*! exports provided: MapComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function () { return MapComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var MapComponent = /** @class */ (function () {
                function MapComponent(store) {
                    this.store = store;
                    this.mapPng = './assets/images/map/map.png';
                    this.mapSubscr = './assets/images/map/subscribtion.png';
                }
                MapComponent.prototype.ngOnInit = function () {
                };
                return MapComponent;
            }());
            MapComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-map',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./map.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/map/map.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./map.component.scss */ "./src/app/modules/home/components/map/map.component.scss")).default]
                })
            ], MapComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news-inner/news-event/news-event.component.scss": 
        /*!**********************************************************************************************!*\
          !*** ./src/app/modules/home/components/news/news-inner/news-event/news-event.component.scss ***!
          \**********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".news-event-container {\n  background: url(\"/assets/images/about/about-inner/ballons.png\"), #fff;\n  background-repeat: no-repeat;\n  background-position-y: 130%;\n  background-position-x: 95%;\n  -webkit-animation: up infinite 10s linear;\n          animation: up infinite 10s linear;\n}\n\n@-webkit-keyframes up {\n  from {\n    background-position-y: 130%;\n  }\n  to {\n    background-position-y: -130%;\n  }\n}\n\n@keyframes up {\n  from {\n    background-position-y: 130%;\n  }\n  to {\n    background-position-y: -130%;\n  }\n}\n\n.news-event-container__title {\n  margin-bottom: 5%;\n  width: 100%;\n  padding: 10% 10%;\n  text-align: center;\n  color: #ffffff;\n  background: rgba(0, 0, 0, 0.3);\n  padding-top: 15%;\n}\n\n.news-event-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 50%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 32px;\n  margin-bottom: 0;\n  z-index: 2;\n}\n\n.news-event-container__title .news-event-container__title__body {\n  margin-top: 8%;\n}\n\n.news-event-container__title .news-event-container__title__body h3 {\n  font-weight: bold;\n  background: none;\n  font-size: 14px;\n  display: inline-block;\n}\n\n.news-event-container__title .news-event-container__title__body p {\n  padding: 0 15%;\n  margin-top: 1rem;\n}\n\n.news-event-container__content {\n  padding: 0 18%;\n}\n\n.news-event-container__content h3 {\n  margin-top: 5%;\n}\n\n.info--grey-color {\n  margin-top: 5%;\n  color: #bfbfbf;\n  font-size: 12px;\n  padding: 0 18%;\n}\n\n.image-left {\n  width: 90%;\n}\n\n#ngCarousel {\n  margin: 3% 0;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9uZXdzLWlubmVyL25ld3MtZXZlbnQvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxuZXdzXFxuZXdzLWlubmVyXFxuZXdzLWV2ZW50XFxuZXdzLWV2ZW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9uZXdzL25ld3MtaW5uZXIvbmV3cy1ldmVudC9uZXdzLWV2ZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0kscUVBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5Q0FBQTtVQUFBLGlDQUFBO0FDSEo7O0FETUE7RUFDSTtJQUNJLDJCQUFBO0VDSE47RURLRTtJQUNJLDRCQUFBO0VDSE47QUFDRjs7QURIQTtFQUNJO0lBQ0ksMkJBQUE7RUNITjtFREtFO0lBQ0ksNEJBQUE7RUNITjtBQUNGOztBREtBO0VBQ0ksaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtFQUNBLGdCQUFBO0FDSEo7O0FESUk7RUFFSSxpQkFBQTtFQUNBLHlEQUFBO0VBQ0EsMEJBQUE7RUFDQSwwQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtBQ0hSOztBRGVJO0VBQ0ksY0FBQTtBQ2JSOztBRGNRO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtBQ1paOztBRGNRO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0FDWlo7O0FEbUJBO0VBQ0ksY0FBQTtBQ2hCSjs7QURpQks7RUFDRyxjQUFBO0FDZlI7O0FEbUJBO0VBQ0ksY0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ2hCSjs7QURrQkE7RUFDSSxVQUFBO0FDZko7O0FEbUJBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNoQkoiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9uZXdzL25ld3MtaW5uZXIvbmV3cy1ldmVudC9uZXdzLWV2ZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmcnO1xyXG4kYmFsbDogJy9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0LWlubmVyL2JhbGxvbnMucG5nJztcclxuXHJcblxyXG4ubmV3cy1ldmVudC1jb250YWluZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJGJhbGwpLCAjZmZmO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMTMwJTs7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDk1JTtcclxuICAgIGFuaW1hdGlvbjogdXAgaW5maW5pdGUgMTBzIGxpbmVhcjtcclxufVxyXG5cclxuQGtleWZyYW1lcyB1cCB7XHJcbiAgICBmcm9te1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMTMwJTtcclxuICAgIH1cclxuICAgIHRve1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogLTEzMCU7XHJcbiAgICB9XHJcbn1cclxuLm5ld3MtZXZlbnQtY29udGFpbmVyX190aXRsZXtcclxuICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAxMCUgMTAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogIzAwMDAwMCwgJGFscGhhOiAuMyk7XHJcbiAgICBwYWRkaW5nLXRvcDogMTUlO1xyXG4gICAgaDMge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgkdW5kZXJsaW5lKSBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiA1MCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7IFxyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBmb250LXNpemU6IDMycHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICB6LWluZGV4OiAyO1xyXG4gICAgICAgIC8vICY6OmJlZm9yZXtcclxuICAgICAgICAvLyAgICAgY29udGVudDogJyc7XHJcbiAgICAgICAgLy8gICAgIHRvcDogMDtcclxuICAgICAgICAvLyAgICAgbGVmdDogMDtcclxuICAgICAgICAvLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIC8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgLy8gICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIC8vICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLy8gICAgIGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IC4zKTtcclxuICAgICAgICAvLyAgfVxyXG4gICAgfVxyXG4gICAgLm5ld3MtZXZlbnQtY29udGFpbmVyX190aXRsZV9fYm9keXtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4JTtcclxuICAgICAgICBoMyB7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICB9XHJcbiAgICAgICAgcHtcclxuICAgICAgICAgICAgcGFkZGluZzogMCAxNSUgO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBcclxufVxyXG5cclxuLm5ld3MtZXZlbnQtY29udGFpbmVyX19jb250ZW50e1xyXG4gICAgcGFkZGluZzogIDAgMTglO1xyXG4gICAgIGgze1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgIH1cclxuICAgXHJcbn1cclxuLmluZm8tLWdyZXktY29sb3J7XHJcbiAgICBtYXJnaW4tdG9wOiA1JTtcclxuICAgIGNvbG9yOiAjYmZiZmJmO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgcGFkZGluZzogIDAgMTglO1xyXG59XHJcbi5pbWFnZS1sZWZ0e1xyXG4gICAgd2lkdGg6IDkwJTtcclxufVxyXG5cclxuXHJcbiNuZ0Nhcm91c2Vse1xyXG4gICAgbWFyZ2luOiAzJSAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn0iLCIubmV3cy1ldmVudC1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1pbm5lci9iYWxsb25zLnBuZ1wiKSwgI2ZmZjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAxMzAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDk1JTtcbiAgYW5pbWF0aW9uOiB1cCBpbmZpbml0ZSAxMHMgbGluZWFyO1xufVxuXG5Aa2V5ZnJhbWVzIHVwIHtcbiAgZnJvbSB7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAxMzAlO1xuICB9XG4gIHRvIHtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IC0xMzAlO1xuICB9XG59XG4ubmV3cy1ldmVudC1jb250YWluZXJfX3RpdGxlIHtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxMCUgMTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIHBhZGRpbmctdG9wOiAxNSU7XG59XG4ubmV3cy1ldmVudC1jb250YWluZXJfX3RpdGxlIGgzIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDUwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAzMnB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB6LWluZGV4OiAyO1xufVxuLm5ld3MtZXZlbnQtY29udGFpbmVyX190aXRsZSAubmV3cy1ldmVudC1jb250YWluZXJfX3RpdGxlX19ib2R5IHtcbiAgbWFyZ2luLXRvcDogOCU7XG59XG4ubmV3cy1ldmVudC1jb250YWluZXJfX3RpdGxlIC5uZXdzLWV2ZW50LWNvbnRhaW5lcl9fdGl0bGVfX2JvZHkgaDMge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4ubmV3cy1ldmVudC1jb250YWluZXJfX3RpdGxlIC5uZXdzLWV2ZW50LWNvbnRhaW5lcl9fdGl0bGVfX2JvZHkgcCB7XG4gIHBhZGRpbmc6IDAgMTUlO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xufVxuXG4ubmV3cy1ldmVudC1jb250YWluZXJfX2NvbnRlbnQge1xuICBwYWRkaW5nOiAwIDE4JTtcbn1cbi5uZXdzLWV2ZW50LWNvbnRhaW5lcl9fY29udGVudCBoMyB7XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuXG4uaW5mby0tZ3JleS1jb2xvciB7XG4gIG1hcmdpbi10b3A6IDUlO1xuICBjb2xvcjogI2JmYmZiZjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBwYWRkaW5nOiAwIDE4JTtcbn1cblxuLmltYWdlLWxlZnQge1xuICB3aWR0aDogOTAlO1xufVxuXG4jbmdDYXJvdXNlbCB7XG4gIG1hcmdpbjogMyUgMDtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news-inner/news-event/news-event.component.ts": 
        /*!********************************************************************************************!*\
          !*** ./src/app/modules/home/components/news/news-inner/news-event/news-event.component.ts ***!
          \********************************************************************************************/
        /*! exports provided: NewsEventComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsEventComponent", function () { return NewsEventComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var NewsEventComponent = /** @class */ (function () {
                function NewsEventComponent() {
                    this.header = "/assets/images/news/news-event/title.png";
                    this.imageRight = "/assets/images/projects/project/imageRight.png";
                    this.styles = {
                        'background': "url(" + this.header + ")",
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover'
                    };
                    this.imgags = [
                        '/assets/images/charity/charity-inner/slide1.png',
                        '/assets/images/charity/charity-inner/slide2.png',
                        '/assets/images/charity/charity-inner/slide3.png'
                    ];
                    this.carouselTileItems = [0];
                    this.carouselTiles = {
                        0: []
                    };
                    this.carouselTile = {
                        grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
                        slide: 3,
                        speed: 250,
                        point: {
                            visible: false
                        },
                        load: 3,
                        velocity: 0,
                        touch: true,
                        easing: 'cubic-bezier(0, 0, 0.2, 1)'
                    };
                }
                NewsEventComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.carouselTileItems.forEach(function (el) {
                        _this.carouselTileLoad(el);
                    });
                };
                NewsEventComponent.prototype.carouselTileLoad = function (j) {
                    // console.log(this.carouselTiles[j]);
                    var len = this.carouselTiles[j].length;
                    if (len <= 30) {
                        for (var i = len; i < len + 15; i++) {
                            this.carouselTiles[j].push(this.imgags[Math.floor(Math.random() * this.imgags.length)]);
                        }
                    }
                };
                return NewsEventComponent;
            }());
            NewsEventComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-news-event',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./news-event.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-event/news-event.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./news-event.component.scss */ "./src/app/modules/home/components/news/news-inner/news-event/news-event.component.scss")).default]
                })
            ], NewsEventComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news-inner/news-inner.component.scss": 
        /*!***********************************************************************************!*\
          !*** ./src/app/modules/home/components/news/news-inner/news-inner.component.scss ***!
          \***********************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".news-inner-container {\n  background: #fff;\n}\n\n.news-inner-container__title {\n  padding: 0 5%;\n}\n\n.news-inner-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 42px;\n  margin-bottom: 0;\n}\n\n.news-inner-container__title p {\n  color: #bfbfbf;\n  padding-left: 1rem;\n}\n\n.news-inner-container__tabs {\n  width: 100%;\n  padding: 0 5%;\n}\n\n.news-inner-container__tabs .nav-item:first-child .nav-link {\n  border-right: none;\n}\n\n.news-inner-container__tabs .nav-item:last-child .nav-link {\n  margin-left: -2px;\n}\n\n.news-inner-container__tabs .nav-link {\n  display: inline-block;\n  padding: 1rem 1.5rem;\n  color: #000;\n  border: 1px solid #000;\n  transition: all 0.3s;\n}\n\n.news-inner-container__tabs .nav-link:hover {\n  background: #000;\n  color: #fff;\n  text-decoration: none;\n}\n\n.news-inner-container__tabs .nav-tabs {\n  border-bottom: none;\n  margin-bottom: 5%;\n}\n\n.news-inner-container__tabs img {\n  width: 100%;\n  z-index: 1;\n}\n\n.news-inner-container__tabs .news-card {\n  position: relative;\n  margin-bottom: 5%;\n}\n\n.news-inner-container__tabs .news-card p {\n  margin-bottom: 0;\n  color: #fff;\n  font-size: 16px;\n}\n\n.news-inner-container__tabs .news-card p span {\n  font-size: 12px;\n}\n\n.news-inner-container__tabs .news-card p span:first-child {\n  margin-right: 5%;\n}\n\n.news-inner-container__tabs .news-card .news-info {\n  position: absolute;\n  z-index: 2;\n  bottom: 0;\n  width: 100%;\n  padding: 5%;\n  background: linear-gradient(0deg, rgba(0, 0, 0, 0.6) 34%, rgba(0, 0, 0, 0) 80%);\n  padding-top: 10%;\n}\n\n.news-inner-container__tabs .waterfall-second:last-child {\n  padding-top: 20%;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info {\n  height: 200px;\n  padding-top: 2%;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info a:hover {\n  text-decoration: none;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info p:first-child {\n  color: #878787;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info p {\n  position: relative;\n  display: inline-block;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info p span {\n  color: #000;\n  font-weight: bold;\n}\n\n.news-inner-container__tabs .waterfall-second .news-info p span::after {\n  content: \"\";\n  width: 20px;\n  height: 1px;\n  background: #000;\n  position: absolute;\n  right: -25px;\n  top: 55%;\n}\n\n.news-inner-container__tabs .waterfall-second img {\n  max-height: 200px;\n}\n\n.waterfall-first {\n  transition: all 0.3s;\n  -webkit-animation: waterfall-1 1.6s ease-in-out;\n          animation: waterfall-1 1.6s ease-in-out;\n}\n\n.waterfall-second {\n  transition: all 0.3s;\n  -webkit-animation: waterfall-2 2s ease-in-out;\n          animation: waterfall-2 2s ease-in-out;\n}\n\n@-webkit-keyframes waterfall-1 {\n  from {\n    transform: translateX(-115%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n\n@keyframes waterfall-1 {\n  from {\n    transform: translateX(-115%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n\n@-webkit-keyframes waterfall-2 {\n  from {\n    transform: translateY(115%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n\n@keyframes waterfall-2 {\n  from {\n    transform: translateY(115%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9uZXdzLWlubmVyL0M6XFxPU1BhbmVsXFxkb21haW5zXFxhX2x1eF9fY2FyaXRhc1xcY2FyaXRhcy9zcmNcXGFwcFxcbW9kdWxlc1xcaG9tZVxcY29tcG9uZW50c1xcbmV3c1xcbmV3cy1pbm5lclxcbmV3cy1pbm5lci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9uZXdzLWlubmVyL25ld3MtaW5uZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxnQkFBQTtBQ0FKOztBREVBO0VBQ0ksYUFBQTtBQ0NKOztBRENJO0VBQ0ksaUJBQUE7RUFDQSx5REFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NSOztBREVJO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0FDQVI7O0FESUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtBQ0RKOztBRElRO0VBQ0ksa0JBQUE7QUNGWjs7QURLUTtFQUNJLGlCQUFBO0FDSFo7O0FEUUk7RUFDSSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUNOUjs7QURTUTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0FDUFo7O0FEV0k7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0FDVFI7O0FEWUk7RUFDSSxXQUFBO0VBQ0EsVUFBQTtBQ1ZSOztBRGFJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQ1hSOztBRGNRO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ1paOztBRGNZO0VBQ0ksZUFBQTtBQ1poQjs7QURjZ0I7RUFDSSxnQkFBQTtBQ1pwQjs7QURpQlE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSwrRUFBQTtFQUNBLGdCQUFBO0FDZlo7O0FEdUJRO0VBQ0ksZ0JBQUE7QUNyQlo7O0FEd0JRO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUN0Qlo7O0FEd0JnQjtFQUNJLHFCQUFBO0FDdEJwQjs7QUQwQlk7RUFDSSxjQUFBO0FDeEJoQjs7QUQyQlk7RUFDSSxrQkFBQTtFQUNBLHFCQUFBO0FDekJoQjs7QUQwQmdCO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FDeEJwQjs7QUQwQm9CO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0FDeEJ4Qjs7QUQrQlE7RUFDSSxpQkFBQTtBQzdCWjs7QURrQ0E7RUFDSSxvQkFBQTtFQUNBLCtDQUFBO1VBQUEsdUNBQUE7QUMvQko7O0FEa0NBO0VBQ0ksb0JBQUE7RUFDQSw2Q0FBQTtVQUFBLHFDQUFBO0FDL0JKOztBRG1DQTtFQUNJO0lBQ0ksNEJBQUE7RUNoQ047RURrQ0U7SUFDSSx5QkFBQTtFQ2hDTjtBQUNGOztBRDBCQTtFQUNJO0lBQ0ksNEJBQUE7RUNoQ047RURrQ0U7SUFDSSx5QkFBQTtFQ2hDTjtBQUNGOztBRG1DQTtFQUNJO0lBQ0ksMkJBQUE7RUNqQ047RURtQ0U7SUFDSSx5QkFBQTtFQ2pDTjtBQUNGOztBRDJCQTtFQUNJO0lBQ0ksMkJBQUE7RUNqQ047RURtQ0U7SUFDSSx5QkFBQTtFQ2pDTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9uZXdzLWlubmVyL25ld3MtaW5uZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkdW5kZXJsaW5lOiAnL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZyc7XHJcbi5uZXdzLWlubmVyLWNvbnRhaW5lcntcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuLm5ld3MtaW5uZXItY29udGFpbmVyX190aXRsZSB7XHJcbiAgICBwYWRkaW5nOiAwIDUlO1xyXG5cclxuICAgIGgzIHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgZm9udC1zaXplOiA0MnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgICAgY29sb3I6ICNiZmJmYmY7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG59XHJcblxyXG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAwIDUlO1xyXG5cclxuICAgIC5uYXYtaXRlbSB7XHJcbiAgICAgICAgJjpmaXJzdC1jaGlsZCAubmF2LWxpbmsge1xyXG4gICAgICAgICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAmOmxhc3QtY2hpbGQgLm5hdi1saW5rIHtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0ycHg7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICAubmF2LWxpbmsge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBwYWRkaW5nOiAxcmVtIDEuNXJlbTtcclxuICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcblxyXG5cclxuICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdi10YWJzIHtcclxuICAgICAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDUlO1xyXG4gICAgfVxyXG5cclxuICAgIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgIH1cclxuXHJcbiAgICAubmV3cy1jYXJkIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNSU7XHJcblxyXG5cclxuICAgICAgICBwIHtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuXHJcbiAgICAgICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG5cclxuICAgICAgICAgICAgICAgICY6Zmlyc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNSU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5uZXdzLWluZm8ge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHotaW5kZXg6IDI7XHJcbiAgICAgICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDUlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMGRlZywgcmdiYSgwLCAwLCAwLCAwLjYpIDM0JSwgcmdiYSgwLCAwLCAwLCAwKSA4MCUpO1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMTAlO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAud2F0ZXJmYWxsLXNlY29uZCB7XHJcbiAgICAgICAgJjpsYXN0LWNoaWxkIHtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDIwJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5uZXdzLWluZm8ge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMiU7XHJcbiAgICAgICAgICAgIGF7XHJcbiAgICAgICAgICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcDpmaXJzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcCB7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjo6YWZ0ZXJ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDFweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByaWdodDogLTI1cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTUlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDIwMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLndhdGVyZmFsbC1maXJzdCB7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgYW5pbWF0aW9uOiB3YXRlcmZhbGwtMSAxLjZzIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG4ud2F0ZXJmYWxsLXNlY29uZCB7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgYW5pbWF0aW9uOiB3YXRlcmZhbGwtMiAycyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuXHJcbkBrZXlmcmFtZXMgd2F0ZXJmYWxsLTEge1xyXG4gICAgZnJvbXtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTExNSUpO1xyXG4gICAgfVxyXG4gICAgdG8ge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwJSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgd2F0ZXJmYWxsLTIge1xyXG4gICAgZnJvbXtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMTE1JSk7XHJcbiAgICB9XHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDAlKTtcclxuICAgIH1cclxufSIsIi5uZXdzLWlubmVyLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5uZXdzLWlubmVyLWNvbnRhaW5lcl9fdGl0bGUge1xuICBwYWRkaW5nOiAwIDUlO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190aXRsZSBoMyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy91bmRlcmxpbmUucG5nXCIpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDQycHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RpdGxlIHAge1xuICBjb2xvcjogI2JmYmZiZjtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xufVxuXG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMCA1JTtcbn1cbi5uZXdzLWlubmVyLWNvbnRhaW5lcl9fdGFicyAubmF2LWl0ZW06Zmlyc3QtY2hpbGQgLm5hdi1saW5rIHtcbiAgYm9yZGVyLXJpZ2h0OiBub25lO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uYXYtaXRlbTpsYXN0LWNoaWxkIC5uYXYtbGluayB7XG4gIG1hcmdpbi1sZWZ0OiAtMnB4O1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uYXYtbGluayB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMXJlbSAxLjVyZW07XG4gIGNvbG9yOiAjMDAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbn1cbi5uZXdzLWlubmVyLWNvbnRhaW5lcl9fdGFicyAubmF2LWxpbms6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uYXYtdGFicyB7XG4gIGJvcmRlci1ib3R0b206IG5vbmU7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uZXdzLWNhcmQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uZXdzLWNhcmQgcCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDE2cHg7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLm5ld3MtY2FyZCBwIHNwYW4ge1xuICBmb250LXNpemU6IDEycHg7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLm5ld3MtY2FyZCBwIHNwYW46Zmlyc3QtY2hpbGQge1xuICBtYXJnaW4tcmlnaHQ6IDUlO1xufVxuLm5ld3MtaW5uZXItY29udGFpbmVyX190YWJzIC5uZXdzLWNhcmQgLm5ld3MtaW5mbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMjtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNSU7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCByZ2JhKDAsIDAsIDAsIDAuNikgMzQlLCByZ2JhKDAsIDAsIDAsIDApIDgwJSk7XG4gIHBhZGRpbmctdG9wOiAxMCU7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQ6bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctdG9wOiAyMCU7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgLm5ld3MtaW5mbyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHBhZGRpbmctdG9wOiAyJTtcbn1cbi5uZXdzLWlubmVyLWNvbnRhaW5lcl9fdGFicyAud2F0ZXJmYWxsLXNlY29uZCAubmV3cy1pbmZvIGE6aG92ZXIge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgLm5ld3MtaW5mbyBwOmZpcnN0LWNoaWxkIHtcbiAgY29sb3I6ICM4Nzg3ODc7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgLm5ld3MtaW5mbyBwIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgLm5ld3MtaW5mbyBwIHNwYW4ge1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgLm5ld3MtaW5mbyBwIHNwYW46OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMXB4O1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAtMjVweDtcbiAgdG9wOiA1NSU7XG59XG4ubmV3cy1pbm5lci1jb250YWluZXJfX3RhYnMgLndhdGVyZmFsbC1zZWNvbmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG59XG5cbi53YXRlcmZhbGwtZmlyc3Qge1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYW5pbWF0aW9uOiB3YXRlcmZhbGwtMSAxLjZzIGVhc2UtaW4tb3V0O1xufVxuXG4ud2F0ZXJmYWxsLXNlY29uZCB7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHdhdGVyZmFsbC0yIDJzIGVhc2UtaW4tb3V0O1xufVxuXG5Aa2V5ZnJhbWVzIHdhdGVyZmFsbC0xIHtcbiAgZnJvbSB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMTUlKTtcbiAgfVxuICB0byB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDAlKTtcbiAgfVxufVxuQGtleWZyYW1lcyB3YXRlcmZhbGwtMiB7XG4gIGZyb20ge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMTUlKTtcbiAgfVxuICB0byB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDAlKTtcbiAgfVxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news-inner/news-inner.component.ts": 
        /*!*********************************************************************************!*\
          !*** ./src/app/modules/home/components/news/news-inner/news-inner.component.ts ***!
          \*********************************************************************************/
        /*! exports provided: NewsInnerComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsInnerComponent", function () { return NewsInnerComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var NewsInnerComponent = /** @class */ (function () {
                function NewsInnerComponent() {
                    this.newsCard1 = "./assets/images/news/news-inner/news1.png";
                    this.newsCard2 = "./assets/images/news/news-inner/news2.png";
                }
                NewsInnerComponent.prototype.ngOnInit = function () {
                };
                return NewsInnerComponent;
            }());
            NewsInnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-news-inner',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./news-inner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news-inner/news-inner.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./news-inner.component.scss */ "./src/app/modules/home/components/news/news-inner/news-inner.component.scss")).default]
                })
            ], NewsInnerComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news.component.scss": 
        /*!******************************************************************!*\
          !*** ./src/app/modules/home/components/news/news.component.scss ***!
          \******************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".news-container {\n  background: #fff;\n  height: 100vh;\n  position: relative;\n  overflow: hidden;\n}\n.news-container .news-header {\n  width: 100%;\n  height: 10%;\n  text-align: center;\n  position: relative;\n}\n.news-container .news-header span {\n  position: relative;\n}\n.news-container .news-header a {\n  color: inherit;\n}\n.news-container .news-header h2 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 30%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 42px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.news-container .news-header-btn {\n  text-align: center;\n}\n.news-container .news-header-btn .nav-item {\n  border: none;\n  border-right: 2px solid #000;\n  border-radius: 0;\n}\n.news-container .news-header-btn .nav-item:last-child {\n  border-right: none;\n}\n.news-container .news-header-btn .news-header-btn__wrapper {\n  display: inline-block;\n  border: 1px solid #000;\n  border-radius: 5px;\n  overflow: hidden;\n  transition: all 0.3s;\n  -webkit-animation: onStage 3s linear;\n          animation: onStage 3s linear;\n}\n.news-container .news-header-btn .news-header-btn__btn-tabs {\n  display: inline-block;\n  padding: 1rem 2rem;\n  color: #000;\n  border-right: none !important;\n  margin-right: -1px;\n  transition: all 0.3s;\n}\n.news-container .news-header-btn .news-header-btn__btn-tabs:last-child {\n  border-right: none;\n}\n.news-container .news-header-btn .news-header-btn__btn-tabs:hover {\n  background: #000;\n  color: #fff;\n  text-decoration: none;\n}\n.news-container .news-content {\n  text-align: center;\n  z-index: 1;\n}\n.news-container .news-content .news-img {\n  text-align: center;\n  z-index: 1;\n}\n.news-container .news-content .news-img img {\n  margin: auto;\n}\n.news-container .news-content .news-img .news-img_target-1 {\n  width: 95%;\n}\n.news-container .news-content .news-img .news-img_target-2 {\n  width: 95%;\n}\n.news-container .news-content .news-img .news-img_target-3 {\n  width: 95%;\n}\n.news-container .news-content .news-img .news-container__article-wrapper {\n  margin: 0 5%;\n  padding: 2% 0;\n  text-align: left;\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);\n  transition: all 0.3s;\n  -webkit-animation: onStage 3s;\n          animation: onStage 3s;\n  background: #fff;\n}\n.news-container .news-content .news-img .news-container__article-wrapper p {\n  padding-top: 5%;\n  padding: 0 10%;\n}\n.news-container .news-content .news-img .news-container__article-wrapper .news-content__read--wrapper {\n  display: inline-block;\n  position: relative;\n  width: 150px;\n}\n.news-container .news-content .news-img .news-container__article-wrapper .news-content__read {\n  padding: 0 10%;\n  color: #000;\n  font-weight: bold;\n}\n.news-container .news-content .news-img .news-container__article-wrapper .news-content__read:after {\n  content: \"\";\n  display: inline-block;\n  width: 20px;\n  height: 1px;\n  position: absolute;\n  bottom: 10px;\n  background: #000;\n  right: 0%;\n}\n.news-container .news-content .news-img .news-container__article-wrapper img {\n  margin-left: 2.5%;\n}\n.news-container .news-content .news-img .news-container__article-wrapper--box-shadow {\n  box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);\n}\n#ball1 {\n  position: absolute;\n  transition: all 0.3s;\n  left: -18%;\n  z-index: 0;\n  -webkit-animation: ball1 4s 0.1s linear;\n          animation: ball1 4s 0.1s linear;\n  transition: all 0.1s;\n  top: 35%;\n}\n#ball2 {\n  position: absolute;\n  top: 0;\n  left: 90%;\n  z-index: 0;\n  transition: all 0.3s;\n  -webkit-animation: ball2 4s 0.3s linear;\n          animation: ball2 4s 0.3s linear;\n  top: 55%;\n}\n#ball3 {\n  position: absolute;\n  top: 0;\n  left: 60%;\n  z-index: 0;\n  transition: all 0.2s;\n  -webkit-animation: ball3 4s 0.2s linear;\n          animation: ball3 4s 0.2s linear;\n  top: 58%;\n}\n@-webkit-keyframes ball1 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 58%;\n  }\n  100% {\n    top: 35%;\n  }\n}\n@keyframes ball1 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 58%;\n  }\n  100% {\n    top: 35%;\n  }\n}\n@-webkit-keyframes ball2 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 58%;\n  }\n  100% {\n    top: 55%;\n  }\n}\n@keyframes ball2 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 58%;\n  }\n  100% {\n    top: 55%;\n  }\n}\n@-webkit-keyframes ball3 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 50%;\n  }\n  100% {\n    top: 58%;\n  }\n}\n@keyframes ball3 {\n  0% {\n    top: -50%;\n  }\n  25% {\n    top: 50%;\n  }\n  50% {\n    top: 100%;\n  }\n  75% {\n    top: 50%;\n  }\n  100% {\n    top: 58%;\n  }\n}\n@-webkit-keyframes onStage {\n  from {\n    transform: translateY(200%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n@keyframes onStage {\n  from {\n    transform: translateY(200%);\n  }\n  to {\n    transform: translateY(0%);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXG1vZHVsZXNcXGhvbWVcXGNvbXBvbmVudHNcXG5ld3NcXG5ld3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL25ld3MvbmV3cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNESjtBREdJO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDRFI7QURHUTtFQUNJLGtCQUFBO0FDRFo7QURJUTtFQUNJLGNBQUE7QUNGWjtBREtRO0VBQ0ksaUJBQUE7RUFDQSw2REFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7RUFDQSwwQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLDJCQUFBO1VBQUEsbUJBQUE7QUNIWjtBRE9JO0VBQ0ksa0JBQUE7QUNMUjtBRE9RO0VBQ0ksWUFBQTtFQUVBLDRCQUFBO0VBQ0EsZ0JBQUE7QUNOWjtBRFFZO0VBQ0ksa0JBQUE7QUNOaEI7QURVUTtFQUNJLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxvQ0FBQTtVQUFBLDRCQUFBO0FDUlo7QURXUTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDVFo7QURXWTtFQUNJLGtCQUFBO0FDVGhCO0FEWVk7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtBQ1ZoQjtBRGVJO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0FDYlI7QURlUTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtBQ2JaO0FEZVk7RUFDSSxZQUFBO0FDYmhCO0FEaUJZO0VBRUksVUFBQTtBQ2hCaEI7QURvQlk7RUFDSSxVQUFBO0FDbEJoQjtBRHNCWTtFQUNJLFVBQUE7QUNwQmhCO0FEeUJZO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLHVDQUFBO0VBQ0Esb0JBQUE7RUFDQSw2QkFBQTtVQUFBLHFCQUFBO0VBQ0EsZ0JBQUE7QUN2QmhCO0FEeUJnQjtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDdkJwQjtBRDBCZ0I7RUFDSSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ3hCcEI7QUQyQmdCO0VBQ0ksY0FBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtBQ3pCcEI7QUQyQm9CO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7QUN6QnhCO0FENkJnQjtFQUNJLGlCQUFBO0FDM0JwQjtBRCtCWTtFQUVJLHVDQUFBO0FDN0JoQjtBRG1DQTtFQUNJLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLHVDQUFBO1VBQUEsK0JBQUE7RUFDQSxvQkFBQTtFQUNBLFFBQUE7QUNoQ0o7QURtQ0E7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0EsdUNBQUE7VUFBQSwrQkFBQTtFQUNBLFFBQUE7QUNoQ0o7QURtQ0E7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLG9CQUFBO0VBQ0EsdUNBQUE7VUFBQSwrQkFBQTtFQUNBLFFBQUE7QUNoQ0o7QURtQ0E7RUFDSTtJQUNJLFNBQUE7RUNoQ047RURtQ0U7SUFDSSxRQUFBO0VDakNOO0VEb0NFO0lBQ0ksU0FBQTtFQ2xDTjtFRHFDRTtJQUNJLFFBQUE7RUNuQ047RURzQ0U7SUFDSSxRQUFBO0VDcENOO0FBQ0Y7QURpQkE7RUFDSTtJQUNJLFNBQUE7RUNoQ047RURtQ0U7SUFDSSxRQUFBO0VDakNOO0VEb0NFO0lBQ0ksU0FBQTtFQ2xDTjtFRHFDRTtJQUNJLFFBQUE7RUNuQ047RURzQ0U7SUFDSSxRQUFBO0VDcENOO0FBQ0Y7QUR1Q0E7RUFDSTtJQUNJLFNBQUE7RUNyQ047RUR3Q0U7SUFDSSxRQUFBO0VDdENOO0VEeUNFO0lBQ0ksU0FBQTtFQ3ZDTjtFRDBDRTtJQUNJLFFBQUE7RUN4Q047RUQyQ0U7SUFDSSxRQUFBO0VDekNOO0FBQ0Y7QURzQkE7RUFDSTtJQUNJLFNBQUE7RUNyQ047RUR3Q0U7SUFDSSxRQUFBO0VDdENOO0VEeUNFO0lBQ0ksU0FBQTtFQ3ZDTjtFRDBDRTtJQUNJLFFBQUE7RUN4Q047RUQyQ0U7SUFDSSxRQUFBO0VDekNOO0FBQ0Y7QUQ0Q0E7RUFDSTtJQUNJLFNBQUE7RUMxQ047RUQ2Q0U7SUFDSSxRQUFBO0VDM0NOO0VEOENFO0lBQ0ksU0FBQTtFQzVDTjtFRCtDRTtJQUNJLFFBQUE7RUM3Q047RURnREU7SUFDSSxRQUFBO0VDOUNOO0FBQ0Y7QUQyQkE7RUFDSTtJQUNJLFNBQUE7RUMxQ047RUQ2Q0U7SUFDSSxRQUFBO0VDM0NOO0VEOENFO0lBQ0ksU0FBQTtFQzVDTjtFRCtDRTtJQUNJLFFBQUE7RUM3Q047RURnREU7SUFDSSxRQUFBO0VDOUNOO0FBQ0Y7QURpREE7RUFDSTtJQUNJLDJCQUFBO0VDL0NOO0VEa0RFO0lBQ0kseUJBQUE7RUNoRE47QUFDRjtBRHlDQTtFQUNJO0lBQ0ksMkJBQUE7RUMvQ047RURrREU7SUFDSSx5QkFBQTtFQ2hETjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvbmV3cy9uZXdzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS1iaWcucG5nJztcclxuXHJcbi5uZXdzLWNvbnRhaW5lciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgLm5ld3MtaGVhZGVyIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICAgICBzcGFuIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaDIge1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdXJsKCR1bmRlcmxpbmUpIG5vLXJlcGVhdDtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiA4MCUgMzAlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDEwMCU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODglO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNDJweDtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVuc2V0O1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgICAgICAgICBhbmltYXRpb246IHRpdGxlIDJzO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubmV3cy1oZWFkZXItYnRuIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgICAgIC5uYXYtaXRlbSB7XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuXHJcbiAgICAgICAgICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkICMwMDA7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcblxyXG4gICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubmV3cy1oZWFkZXItYnRuX193cmFwcGVyIHtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgICAgIGFuaW1hdGlvbjogb25TdGFnZSAzcyBsaW5lYXI7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAubmV3cy1oZWFkZXItYnRuX19idG4tdGFicyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgcGFkZGluZzogMXJlbSAycmVtO1xyXG4gICAgICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogLTFweDtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcztcclxuXHJcbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC5uZXdzLWNvbnRlbnQge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG5cclxuICAgICAgICAubmV3cy1pbWcge1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIHotaW5kZXg6IDE7XHJcblxyXG4gICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLm5ld3MtaW1nX3RhcmdldC0xIHtcclxuXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogOTUlO1xyXG5cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLm5ld3MtaW1nX3RhcmdldC0yIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA5NSU7XHJcblxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAubmV3cy1pbWdfdGFyZ2V0LTMge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgICAgICAubmV3cy1jb250YWluZXJfX2FydGljbGUtd3JhcHBlciB7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDAgNSU7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAyJSAwO1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb246IG9uU3RhZ2UgM3M7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cclxuICAgICAgICAgICAgICAgIHAge1xyXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiA1JTtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDEwJTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubmV3cy1jb250ZW50X19yZWFkLS13cmFwcGVyIHtcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAubmV3cy1jb250ZW50X19yZWFkIHtcclxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDEwJTtcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgJjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMXB4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvdHRvbTogMTBweDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IDAlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyLjUlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAubmV3cy1jb250YWluZXJfX2FydGljbGUtd3JhcHBlci0tYm94LXNoYWRvdyB7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuI2JhbGwxIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICBsZWZ0OiAtMTglO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIGFuaW1hdGlvbjogYmFsbDEgNHMgLjFzIGxpbmVhcjtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuMXM7XHJcbiAgICB0b3A6IDM1JTtcclxufVxyXG5cclxuI2JhbGwyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDkwJTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgYW5pbWF0aW9uOiBiYWxsMiA0cyAuM3MgbGluZWFyO1xyXG4gICAgdG9wOiA1NSU7XHJcbn1cclxuXHJcbiNiYWxsMyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiA2MCU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4ycztcclxuICAgIGFuaW1hdGlvbjogYmFsbDMgNHMgLjJzIGxpbmVhcjtcclxuICAgIHRvcDogNTglO1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJhbGwxIHtcclxuICAgIDAlIHtcclxuICAgICAgICB0b3A6IC01MCU7XHJcbiAgICB9XHJcblxyXG4gICAgMjUlIHtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgIH1cclxuXHJcbiAgICA1MCUge1xyXG4gICAgICAgIHRvcDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICA3NSUge1xyXG4gICAgICAgIHRvcDogNTglO1xyXG4gICAgfVxyXG5cclxuICAgIDEwMCUge1xyXG4gICAgICAgIHRvcDogMzUlO1xyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJhbGwyIHtcclxuICAgIDAlIHtcclxuICAgICAgICB0b3A6IC01MCU7XHJcbiAgICB9XHJcblxyXG4gICAgMjUlIHtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgIH1cclxuXHJcbiAgICA1MCUge1xyXG4gICAgICAgIHRvcDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICA3NSUge1xyXG4gICAgICAgIHRvcDogNTglO1xyXG4gICAgfVxyXG5cclxuICAgIDEwMCUge1xyXG4gICAgICAgIHRvcDogNTUlO1xyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJhbGwzIHtcclxuICAgIDAlIHtcclxuICAgICAgICB0b3A6IC01MCU7XHJcbiAgICB9XHJcblxyXG4gICAgMjUlIHtcclxuICAgICAgICB0b3A6IDUwJTtcclxuICAgIH1cclxuXHJcbiAgICA1MCUge1xyXG4gICAgICAgIHRvcDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICA3NSUge1xyXG4gICAgICAgIHRvcDogNTAlO1xyXG4gICAgfVxyXG5cclxuICAgIDEwMCUge1xyXG4gICAgICAgIHRvcDogNTglO1xyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIG9uU3RhZ2Uge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDIwMCUpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpO1xyXG4gICAgfVxyXG59IiwiLm5ld3MtY29udGFpbmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWhlYWRlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyIHNwYW4ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyIGEge1xuICBjb2xvcjogaW5oZXJpdDtcbn1cbi5uZXdzLWNvbnRhaW5lciAubmV3cy1oZWFkZXIgaDIge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLWJpZy5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAzMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiA0MnB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHRpdGxlIDJzO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWhlYWRlci1idG4ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyLWJ0biAubmF2LWl0ZW0ge1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yaWdodDogMnB4IHNvbGlkICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyLWJ0biAubmF2LWl0ZW06bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1yaWdodDogbm9uZTtcbn1cbi5uZXdzLWNvbnRhaW5lciAubmV3cy1oZWFkZXItYnRuIC5uZXdzLWhlYWRlci1idG5fX3dyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGFuaW1hdGlvbjogb25TdGFnZSAzcyBsaW5lYXI7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyLWJ0biAubmV3cy1oZWFkZXItYnRuX19idG4tdGFicyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMXJlbSAycmVtO1xuICBjb2xvcjogIzAwMDtcbiAgYm9yZGVyLXJpZ2h0OiBub25lICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogLTFweDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtaGVhZGVyLWJ0biAubmV3cy1oZWFkZXItYnRuX19idG4tdGFiczpsYXN0LWNoaWxkIHtcbiAgYm9yZGVyLXJpZ2h0OiBub25lO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWhlYWRlci1idG4gLm5ld3MtaGVhZGVyLWJ0bl9fYnRuLXRhYnM6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHotaW5kZXg6IDE7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHotaW5kZXg6IDE7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgaW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWNvbnRlbnQgLm5ld3MtaW1nIC5uZXdzLWltZ190YXJnZXQtMSB7XG4gIHdpZHRoOiA5NSU7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgLm5ld3MtaW1nX3RhcmdldC0yIHtcbiAgd2lkdGg6IDk1JTtcbn1cbi5uZXdzLWNvbnRhaW5lciAubmV3cy1jb250ZW50IC5uZXdzLWltZyAubmV3cy1pbWdfdGFyZ2V0LTMge1xuICB3aWR0aDogOTUlO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWNvbnRlbnQgLm5ld3MtaW1nIC5uZXdzLWNvbnRhaW5lcl9fYXJ0aWNsZS13cmFwcGVyIHtcbiAgbWFyZ2luOiAwIDUlO1xuICBwYWRkaW5nOiAyJSAwO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBib3gtc2hhZG93OiAwIDAgMjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IG9uU3RhZ2UgM3M7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgLm5ld3MtY29udGFpbmVyX19hcnRpY2xlLXdyYXBwZXIgcCB7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgcGFkZGluZzogMCAxMCU7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgLm5ld3MtY29udGFpbmVyX19hcnRpY2xlLXdyYXBwZXIgLm5ld3MtY29udGVudF9fcmVhZC0td3JhcHBlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMTUwcHg7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgLm5ld3MtY29udGFpbmVyX19hcnRpY2xlLXdyYXBwZXIgLm5ld3MtY29udGVudF9fcmVhZCB7XG4gIHBhZGRpbmc6IDAgMTAlO1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4ubmV3cy1jb250YWluZXIgLm5ld3MtY29udGVudCAubmV3cy1pbWcgLm5ld3MtY29udGFpbmVyX19hcnRpY2xlLXdyYXBwZXIgLm5ld3MtY29udGVudF9fcmVhZDphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMTBweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgcmlnaHQ6IDAlO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWNvbnRlbnQgLm5ld3MtaW1nIC5uZXdzLWNvbnRhaW5lcl9fYXJ0aWNsZS13cmFwcGVyIGltZyB7XG4gIG1hcmdpbi1sZWZ0OiAyLjUlO1xufVxuLm5ld3MtY29udGFpbmVyIC5uZXdzLWNvbnRlbnQgLm5ld3MtaW1nIC5uZXdzLWNvbnRhaW5lcl9fYXJ0aWNsZS13cmFwcGVyLS1ib3gtc2hhZG93IHtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMjBweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbn1cblxuI2JhbGwxIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgbGVmdDogLTE4JTtcbiAgei1pbmRleDogMDtcbiAgYW5pbWF0aW9uOiBiYWxsMSA0cyAwLjFzIGxpbmVhcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMXM7XG4gIHRvcDogMzUlO1xufVxuXG4jYmFsbDIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogOTAlO1xuICB6LWluZGV4OiAwO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYW5pbWF0aW9uOiBiYWxsMiA0cyAwLjNzIGxpbmVhcjtcbiAgdG9wOiA1NSU7XG59XG5cbiNiYWxsMyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiA2MCU7XG4gIHotaW5kZXg6IDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjJzO1xuICBhbmltYXRpb246IGJhbGwzIDRzIDAuMnMgbGluZWFyO1xuICB0b3A6IDU4JTtcbn1cblxuQGtleWZyYW1lcyBiYWxsMSB7XG4gIDAlIHtcbiAgICB0b3A6IC01MCU7XG4gIH1cbiAgMjUlIHtcbiAgICB0b3A6IDUwJTtcbiAgfVxuICA1MCUge1xuICAgIHRvcDogMTAwJTtcbiAgfVxuICA3NSUge1xuICAgIHRvcDogNTglO1xuICB9XG4gIDEwMCUge1xuICAgIHRvcDogMzUlO1xuICB9XG59XG5Aa2V5ZnJhbWVzIGJhbGwyIHtcbiAgMCUge1xuICAgIHRvcDogLTUwJTtcbiAgfVxuICAyNSUge1xuICAgIHRvcDogNTAlO1xuICB9XG4gIDUwJSB7XG4gICAgdG9wOiAxMDAlO1xuICB9XG4gIDc1JSB7XG4gICAgdG9wOiA1OCU7XG4gIH1cbiAgMTAwJSB7XG4gICAgdG9wOiA1NSU7XG4gIH1cbn1cbkBrZXlmcmFtZXMgYmFsbDMge1xuICAwJSB7XG4gICAgdG9wOiAtNTAlO1xuICB9XG4gIDI1JSB7XG4gICAgdG9wOiA1MCU7XG4gIH1cbiAgNTAlIHtcbiAgICB0b3A6IDEwMCU7XG4gIH1cbiAgNzUlIHtcbiAgICB0b3A6IDUwJTtcbiAgfVxuICAxMDAlIHtcbiAgICB0b3A6IDU4JTtcbiAgfVxufVxuQGtleWZyYW1lcyBvblN0YWdlIHtcbiAgZnJvbSB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDIwMCUpO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpO1xuICB9XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/news/news.component.ts": 
        /*!****************************************************************!*\
          !*** ./src/app/modules/home/components/news/news.component.ts ***!
          \****************************************************************/
        /*! exports provided: NewsComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsComponent", function () { return NewsComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            var NewsComponent = /** @class */ (function () {
                function NewsComponent(store) {
                    this.store = store;
                    this.babyImg = './assets/images/news/baby.png';
                    this.boyImg = './assets/images/news/boy.png';
                    this.manImg = './assets/images/news/man.png';
                    this.ball1 = '/assets/images/projects/ball-left.png';
                    this.ball2 = '/assets/images/projects/ball-right-big.png';
                    this.ball3 = '/assets/images/projects/ball-right-sm.png';
                }
                NewsComponent.prototype.ngOnInit = function () {
                };
                return NewsComponent;
            }());
            NewsComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            NewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-news',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./news.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/news/news.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./news.component.scss */ "./src/app/modules/home/components/news/news.component.scss")).default]
                })
            ], NewsComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects-inner/project/project.component.scss": 
        /*!************************************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects-inner/project/project.component.scss ***!
          \************************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".project-container {\n  background: url(\"/assets/images/about/about-inner/ballons.png\"), #fff;\n  background-repeat: no-repeat;\n  background-position-y: 100%;\n  background-position-x: 95%;\n  -webkit-animation: up infinite 10s linear;\n          animation: up infinite 10s linear;\n}\n\n.projects-container__title {\n  margin-bottom: 5%;\n  width: 100%;\n  padding: 10% 10%;\n  text-align: center;\n  color: #ffffff;\n  background: rgba(0, 0, 0, 0.3);\n  padding-top: 15%;\n}\n\n.projects-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 50%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 32px;\n  margin-bottom: 0;\n  z-index: 2;\n}\n\n.projects-container__title .projects-container__title__body {\n  margin-top: 8%;\n}\n\n.projects-container__title .projects-container__title__body h3 {\n  font-weight: bold;\n  background: none;\n  font-size: 14px;\n  display: inline-block;\n}\n\n.projects-container__title .projects-container__title__body p {\n  padding: 0 15%;\n  margin-top: 1rem;\n}\n\n.project-container__content {\n  padding: 0 18%;\n}\n\n.project-container__content h3 {\n  margin-top: 5%;\n}\n\n.info--grey-color {\n  margin-top: 5%;\n  color: #bfbfbf;\n  font-size: 12px;\n  padding: 0 18%;\n}\n\n.image-left {\n  width: 90%;\n}\n\n#ngCarousel {\n  margin: 3% 0;\n  width: 100%;\n}\n\n@-webkit-keyframes up {\n  from {\n    background-position-y: 130%;\n  }\n  to {\n    background-position-y: -130%;\n  }\n}\n\n@keyframes up {\n  from {\n    background-position-y: 130%;\n  }\n  to {\n    background-position-y: -130%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvcHJvamVjdHMvcHJvamVjdHMtaW5uZXIvcHJvamVjdC9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXG1vZHVsZXNcXGhvbWVcXGNvbXBvbmVudHNcXHByb2plY3RzXFxwcm9qZWN0cy1pbm5lclxccHJvamVjdFxccHJvamVjdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvcHJvamVjdHMvcHJvamVjdHMtaW5uZXIvcHJvamVjdC9wcm9qZWN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0kscUVBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5Q0FBQTtVQUFBLGlDQUFBO0FDSEo7O0FES0E7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0VBQ0EsZ0JBQUE7QUNGSjs7QURHSTtFQUVJLGlCQUFBO0VBQ0EseURBQUE7RUFDQSwwQkFBQTtFQUNBLDBCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0FDRlI7O0FEY0k7RUFDSSxjQUFBO0FDWlI7O0FEYVE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0FDWFo7O0FEYVE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7QUNYWjs7QURrQkE7RUFDSSxjQUFBO0FDZko7O0FEZ0JLO0VBQ0csY0FBQTtBQ2RSOztBRGtCQTtFQUNJLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNmSjs7QURpQkE7RUFDSSxVQUFBO0FDZEo7O0FEa0JBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7QUNmSjs7QURrQkE7RUFDSTtJQUNJLDJCQUFBO0VDZk47RURpQkU7SUFDSSw0QkFBQTtFQ2ZOO0FBQ0Y7O0FEU0E7RUFDSTtJQUNJLDJCQUFBO0VDZk47RURpQkU7SUFDSSw0QkFBQTtFQ2ZOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9wcm9qZWN0cy9wcm9qZWN0cy1pbm5lci9wcm9qZWN0L3Byb2plY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkdW5kZXJsaW5lOiAnL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZyc7XHJcbiRiYWxsOiAnL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQtaW5uZXIvYmFsbG9ucy5wbmcnO1xyXG5cclxuXHJcbi5wcm9qZWN0LWNvbnRhaW5lcntcclxuICAgIGJhY2tncm91bmQ6IHVybCgkYmFsbCksICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiA5NSU7XHJcbiAgICBhbmltYXRpb246IHVwIGluZmluaXRlIDEwcyBsaW5lYXI7XHJcbn1cclxuLnByb2plY3RzLWNvbnRhaW5lcl9fdGl0bGV7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1JTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMTAlIDEwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICMwMDAwMDAsICRhbHBoYTogLjMpO1xyXG4gICAgcGFkZGluZy10b3A6IDE1JTtcclxuICAgIGgzIHtcclxuICAgICAgICBcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoJHVuZGVybGluZSkgbm8tcmVwZWF0O1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teDogNTAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODglOyBcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgZm9udC1zaXplOiAzMnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICAgICAgei1pbmRleDogMjtcclxuICAgICAgICAvLyAmOjpiZWZvcmV7XHJcbiAgICAgICAgLy8gICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICAgIC8vICAgICB0b3A6IDA7XHJcbiAgICAgICAgLy8gICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgLy8gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAvLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIC8vICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvLyAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIC8vICAgICBiYWNrZ3JvdW5kOiByZ2JhKCRjb2xvcjogIzAwMDAwMCwgJGFscGhhOiAuMyk7XHJcbiAgICAgICAgLy8gIH1cclxuICAgIH1cclxuICAgIC5wcm9qZWN0cy1jb250YWluZXJfX3RpdGxlX19ib2R5e1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDglO1xyXG4gICAgICAgIGgzIHtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwe1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwIDE1JSA7XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDFyZW07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFxyXG59XHJcblxyXG4ucHJvamVjdC1jb250YWluZXJfX2NvbnRlbnR7XHJcbiAgICBwYWRkaW5nOiAgMCAxOCU7XHJcbiAgICAgaDN7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNSU7XHJcbiAgICAgfVxyXG4gICBcclxufVxyXG4uaW5mby0tZ3JleS1jb2xvcntcclxuICAgIG1hcmdpbi10b3A6IDUlO1xyXG4gICAgY29sb3I6ICNiZmJmYmY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBwYWRkaW5nOiAgMCAxOCU7XHJcbn1cclxuLmltYWdlLWxlZnR7XHJcbiAgICB3aWR0aDogOTAlO1xyXG59XHJcblxyXG5cclxuI25nQ2Fyb3VzZWx7XHJcbiAgICBtYXJnaW46IDMlIDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuQGtleWZyYW1lcyB1cCB7XHJcbiAgICBmcm9te1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMTMwJTtcclxuICAgIH1cclxuICAgIHRve1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogLTEzMCU7XHJcbiAgICB9XHJcbn0iLCIucHJvamVjdC1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9hYm91dC9hYm91dC1pbm5lci9iYWxsb25zLnBuZ1wiKSwgI2ZmZjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDk1JTtcbiAgYW5pbWF0aW9uOiB1cCBpbmZpbml0ZSAxMHMgbGluZWFyO1xufVxuXG4ucHJvamVjdHMtY29udGFpbmVyX190aXRsZSB7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTAlIDEwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjMpO1xuICBwYWRkaW5nLXRvcDogMTUlO1xufVxuLnByb2plY3RzLWNvbnRhaW5lcl9fdGl0bGUgaDMge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogNTAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IDg4JTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDMycHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHotaW5kZXg6IDI7XG59XG4ucHJvamVjdHMtY29udGFpbmVyX190aXRsZSAucHJvamVjdHMtY29udGFpbmVyX190aXRsZV9fYm9keSB7XG4gIG1hcmdpbi10b3A6IDglO1xufVxuLnByb2plY3RzLWNvbnRhaW5lcl9fdGl0bGUgLnByb2plY3RzLWNvbnRhaW5lcl9fdGl0bGVfX2JvZHkgaDMge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4ucHJvamVjdHMtY29udGFpbmVyX190aXRsZSAucHJvamVjdHMtY29udGFpbmVyX190aXRsZV9fYm9keSBwIHtcbiAgcGFkZGluZzogMCAxNSU7XG4gIG1hcmdpbi10b3A6IDFyZW07XG59XG5cbi5wcm9qZWN0LWNvbnRhaW5lcl9fY29udGVudCB7XG4gIHBhZGRpbmc6IDAgMTglO1xufVxuLnByb2plY3QtY29udGFpbmVyX19jb250ZW50IGgzIHtcbiAgbWFyZ2luLXRvcDogNSU7XG59XG5cbi5pbmZvLS1ncmV5LWNvbG9yIHtcbiAgbWFyZ2luLXRvcDogNSU7XG4gIGNvbG9yOiAjYmZiZmJmO1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmc6IDAgMTglO1xufVxuXG4uaW1hZ2UtbGVmdCB7XG4gIHdpZHRoOiA5MCU7XG59XG5cbiNuZ0Nhcm91c2VsIHtcbiAgbWFyZ2luOiAzJSAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuQGtleWZyYW1lcyB1cCB7XG4gIGZyb20ge1xuICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogMTMwJTtcbiAgfVxuICB0byB7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiAtMTMwJTtcbiAgfVxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects-inner/project/project.component.ts": 
        /*!**********************************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects-inner/project/project.component.ts ***!
          \**********************************************************************************************/
        /*! exports provided: ProjectComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectComponent", function () { return ProjectComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ProjectComponent = /** @class */ (function () {
                function ProjectComponent() {
                    this.header = "/assets/images/projects/project/header.png";
                    this.imageRight = "/assets/images/projects/project/imageRight.png";
                    this.styles = {
                        'background': "url(" + this.header + ")",
                        'background-repeat': 'no-repeat',
                        'background-size': 'cover'
                    };
                    this.imgags = [
                        '/assets/images/charity/charity-inner/slide1.png',
                        '/assets/images/charity/charity-inner/slide2.png',
                        '/assets/images/charity/charity-inner/slide3.png'
                    ];
                    this.carouselTileItems = [0];
                    this.carouselTiles = {
                        0: []
                    };
                    this.carouselTile = {
                        grid: { xs: 1, sm: 1, md: 1, lg: 3, all: 0 },
                        slide: 3,
                        speed: 250,
                        point: {
                            visible: false
                        },
                        load: 3,
                        velocity: 0,
                        touch: true,
                        easing: 'cubic-bezier(0, 0, 0.2, 1)'
                    };
                }
                ProjectComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.carouselTileItems.forEach(function (el) {
                        _this.carouselTileLoad(el);
                    });
                };
                ProjectComponent.prototype.carouselTileLoad = function (j) {
                    // console.log(this.carouselTiles[j]);
                    var len = this.carouselTiles[j].length;
                    if (len <= 30) {
                        for (var i = len; i < len + 15; i++) {
                            this.carouselTiles[j].push(this.imgags[Math.floor(Math.random() * this.imgags.length)]);
                        }
                    }
                };
                return ProjectComponent;
            }());
            ProjectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-project',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./project.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/project/project.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./project.component.scss */ "./src/app/modules/home/components/projects/projects-inner/project/project.component.scss")).default]
                })
            ], ProjectComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects-inner/projects-inner.component.scss": 
        /*!***********************************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects-inner/projects-inner.component.scss ***!
          \***********************************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".projects-inner-container {\n  height: 100%;\n  background: #fff;\n}\n\n.projects-inner-container__title {\n  padding: 0 5%;\n}\n\n.projects-inner-container__title h3 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline.png\") no-repeat;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 42px;\n  margin-bottom: 0;\n}\n\n.projects-inner-container__body {\n  padding: 0 5%;\n}\n\n.projects-inner-container__body a {\n  color: inherit;\n}\n\n.projects-inner-container__body a:hover {\n  text-decoration: none;\n}\n\n.projects-inner-container__body img {\n  width: 100%;\n  max-height: 220px;\n}\n\n.projects-inner-container__body p {\n  text-align: center;\n  font-weight: bold;\n}\n\n.projects-inner-container__body .projects-inner-container__body__title {\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvcHJvamVjdHMvcHJvamVjdHMtaW5uZXIvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxwcm9qZWN0c1xccHJvamVjdHMtaW5uZXJcXHByb2plY3RzLWlubmVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9wcm9qZWN0cy9wcm9qZWN0cy1pbm5lci9wcm9qZWN0cy1pbm5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQ0RKOztBREdBO0VBQ0ksYUFBQTtBQ0FKOztBRENJO0VBQ0ksaUJBQUE7RUFDQSx5REFBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0NSOztBREVBO0VBT0ksYUFBQTtBQ0xKOztBRERJO0VBQ0ksY0FBQTtBQ0dSOztBREZRO0VBQ0kscUJBQUE7QUNJWjs7QURBSTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBQ0VSOztBREFJO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQ0VSOztBREVJO0VBQ0ksZ0JBQUE7QUNBUiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL3Byb2plY3RzL3Byb2plY3RzLWlubmVyL3Byb2plY3RzLWlubmVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS5wbmcnO1xyXG5cclxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcntcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fdGl0bGV7XHJcbiAgICBwYWRkaW5nOiAwIDUlO1xyXG4gICAgaDMge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHVybCgkdW5kZXJsaW5lKSBuby1yZXBlYXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODglO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBmb250LXNpemU6IDQycHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgIH1cclxufVxyXG4ucHJvamVjdHMtaW5uZXItY29udGFpbmVyX19ib2R5e1xyXG4gICAgYXtcclxuICAgICAgICBjb2xvcjogaW5oZXJpdDtcclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcGFkZGluZzogMCA1JTtcclxuICAgIGltZ3tcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAyMjBweDtcclxuICAgIH1cclxuICAgIHB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG4gICBcclxuXHJcbiAgICAucHJvamVjdHMtaW5uZXItY29udGFpbmVyX19ib2R5X190aXRsZXtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgfVxyXG59IiwiLnByb2plY3RzLWlubmVyLWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fdGl0bGUge1xuICBwYWRkaW5nOiAwIDUlO1xufVxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fdGl0bGUgaDMge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdW5kZXJsaW5lLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbi15OiA4OCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiA0MnB4O1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4ucHJvamVjdHMtaW5uZXItY29udGFpbmVyX19ib2R5IHtcbiAgcGFkZGluZzogMCA1JTtcbn1cbi5wcm9qZWN0cy1pbm5lci1jb250YWluZXJfX2JvZHkgYSB7XG4gIGNvbG9yOiBpbmhlcml0O1xufVxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fYm9keSBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fYm9keSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LWhlaWdodDogMjIwcHg7XG59XG4ucHJvamVjdHMtaW5uZXItY29udGFpbmVyX19ib2R5IHAge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnByb2plY3RzLWlubmVyLWNvbnRhaW5lcl9fYm9keSAucHJvamVjdHMtaW5uZXItY29udGFpbmVyX19ib2R5X190aXRsZSB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects-inner/projects-inner.component.ts": 
        /*!*********************************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects-inner/projects-inner.component.ts ***!
          \*********************************************************************************************/
        /*! exports provided: ProjectsInnerComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsInnerComponent", function () { return ProjectsInnerComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var ProjectsInnerComponent = /** @class */ (function () {
                function ProjectsInnerComponent() {
                    this.proj1 = '/assets/images/projects/proj1.png';
                    this.proj1Title = 'титул';
                    this.proj2 = '/assets/images/projects/proj2.png';
                    this.proj3 = '/assets/images/projects/proj3.png';
                    this.proj4 = '/assets/images/projects/proj4.png';
                    this.proj5 = '/assets/images/projects/proj5.png';
                }
                ProjectsInnerComponent.prototype.ngOnInit = function () {
                };
                return ProjectsInnerComponent;
            }());
            ProjectsInnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-projects-inner',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./projects-inner.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects-inner/projects-inner.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./projects-inner.component.scss */ "./src/app/modules/home/components/projects/projects-inner/projects-inner.component.scss")).default]
                })
            ], ProjectsInnerComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects.component.scss": 
        /*!**************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects.component.scss ***!
          \**************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".glide {\n  position: relative;\n  width: 100%;\n  box-sizing: border-box;\n}\n.glide * {\n  box-sizing: inherit;\n}\n.glide__track {\n  overflow: hidden;\n}\n.glide__slides {\n  position: relative;\n  width: 100%;\n  list-style: none;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  transform-style: preserve-3d;\n  touch-action: pan-Y;\n  overflow: hidden;\n  padding: 0;\n  white-space: nowrap;\n  display: flex;\n  flex-wrap: nowrap;\n  will-change: transform;\n}\n.glide__slides--dragging {\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide__slide {\n  width: 100%;\n  height: 100%;\n  flex-shrink: 0;\n  white-space: normal;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  -webkit-touch-callout: none;\n  -webkit-tap-highlight-color: transparent;\n}\n.glide__slide a {\n  -webkit-user-select: none;\n          user-select: none;\n  -webkit-user-drag: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n}\n.glide__arrows {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide__bullets {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide--rtl {\n  direction: rtl;\n}\n.project-container {\n  height: 100vh;\n  width: 100%;\n  background: url(\"/assets/images/projects/ball-left.png\") -27% 108% no-repeat, url(\"/assets/images/projects/ball-right-big.png\") 93% 91% no-repeat, url(\"/assets/images/projects/ball-right-sm.png\") 72% 72% no-repeat, #fff;\n}\n.projects-header {\n  width: 100%;\n  height: 20%;\n  text-transform: uppercase;\n  text-align: center;\n  position: relative;\n}\n.projects-header span {\n  position: relative;\n}\n.projects-header a {\n  color: inherit;\n}\n.projects-header h2 {\n  font-weight: bold;\n  background: url(\"/assets/images/underline-big.png\") no-repeat;\n  background-size: 80% 30%;\n  background-position-x: 100%;\n  background-position-y: 88%;\n  display: inline-block;\n  font-size: 28px;\n  margin-bottom: 0;\n  text-transform: unset;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.glide-projects {\n  height: 47%;\n  transition: all 0.3s;\n  -webkit-animation: slider 1s linear;\n          animation: slider 1s linear;\n}\n@-webkit-keyframes slider {\n  from {\n    transform: translateX(-150%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n@keyframes slider {\n  from {\n    transform: translateX(-150%);\n  }\n  to {\n    transform: translateX(0%);\n  }\n}\n.projects-footer {\n  width: 100%;\n  height: 33%;\n  position: relative;\n}\n.projects-footer a {\n  display: inline-block;\n  position: absolute;\n  bottom: 23%;\n  left: 38%;\n  color: inherit;\n  padding: 0.5rem 1.5rem;\n  border: 1px solid #000;\n  border-radius: 7px;\n  transition: all 0.3s;\n  -webkit-animation: btn 1s linear;\n          animation: btn 1s linear;\n}\n.projects-footer a:hover {\n  text-decoration: none;\n  transform: scale(1.1);\n}\n@-webkit-keyframes btn {\n  from {\n    transform: scale(0);\n  }\n  to {\n    transform: scale(1);\n  }\n}\n@keyframes btn {\n  from {\n    transform: scale(0);\n  }\n  to {\n    transform: scale(1);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvcHJvamVjdHMvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL25vZGVfbW9kdWxlc1xcQGdsaWRlanNcXGdsaWRlXFxzcmNcXGFzc2V0c1xcc2Fzc1xcZ2xpZGUuY29yZS5zY3NzIiwic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9wcm9qZWN0cy9wcm9qZWN0cy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvcHJvamVjdHMvQzpcXE9TUGFuZWxcXGRvbWFpbnNcXGFfbHV4X19jYXJpdGFzXFxjYXJpdGFzL3NyY1xcYXBwXFxtb2R1bGVzXFxob21lXFxjb21wb25lbnRzXFxwcm9qZWN0c1xccHJvamVjdHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFNRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQ05GO0FEUUU7RUFDRSxtQkFBQTtBQ05KO0FEU0U7RUFDRSxnQkFBQTtBQ1BKO0FEVUU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ1JKO0FEVUk7RUFDRSx5QkFBQTtLQUFBLHNCQUFBO01BQUEscUJBQUE7VUFBQSxpQkFBQTtBQ1JOO0FEWUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0tBQUEsc0JBQUE7TUFBQSxxQkFBQTtVQUFBLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSx3Q0FBQTtBQ1ZKO0FEWUk7RUFDRSx5QkFBQTtVQUFBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0FDVk47QURjRTtFQUNFLDJCQUFBO0VBQ0EseUJBQUE7S0FBQSxzQkFBQTtNQUFBLHFCQUFBO1VBQUEsaUJBQUE7QUNaSjtBRGVFO0VBQ0UsMkJBQUE7RUFDQSx5QkFBQTtLQUFBLHNCQUFBO01BQUEscUJBQUE7VUFBQSxpQkFBQTtBQ2JKO0FEZ0JFO0VBQ0UsY0FBQTtBQ2RKO0FDN0NBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSwyTkFBQTtBRGdESjtBQzNDQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FEOENKO0FDNUNJO0VBQ0ksa0JBQUE7QUQ4Q1I7QUM1Q0k7RUFDSSxjQUFBO0FEOENSO0FDM0NJO0VBQ0ksaUJBQUE7RUFDQSw2REFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7RUFDQSwwQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLDJCQUFBO1VBQUEsbUJBQUE7QUQ2Q1I7QUM5QkE7RUFDSSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0FEaUNKO0FDOUJBO0VBQ0k7SUFDSSw0QkFBQTtFRGlDTjtFQzdCRTtJQUNJLHlCQUFBO0VEK0JOO0FBQ0Y7QUN2Q0E7RUFDSTtJQUNJLDRCQUFBO0VEaUNOO0VDN0JFO0lBQ0kseUJBQUE7RUQrQk47QUFDRjtBQzNCQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUQ2Qko7QUMzQkk7RUFDSSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQ0FBQTtVQUFBLHdCQUFBO0FENkJSO0FDM0JRO0VBQ0kscUJBQUE7RUFDQSxxQkFBQTtBRDZCWjtBQ3ZCQTtFQUNJO0lBQ0ksbUJBQUE7RUQwQk47RUN0QkU7SUFDSSxtQkFBQTtFRHdCTjtBQUNGO0FDaENBO0VBQ0k7SUFDSSxtQkFBQTtFRDBCTjtFQ3RCRTtJQUNJLG1CQUFBO0VEd0JOO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2hvbWUvY29tcG9uZW50cy9wcm9qZWN0cy9wcm9qZWN0cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCJ2YXJpYWJsZXNcIjtcclxuXHJcbi4jeyRnbGlkZS1jbGFzc30ge1xyXG4gICR0aGlzOiAmO1xyXG5cclxuICAkc2U6ICRnbGlkZS1lbGVtZW50LXNlcGFyYXRvcjtcclxuICAkc206ICRnbGlkZS1tb2RpZmllci1zZXBhcmF0b3I7XHJcblxyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG5cclxuICAqIHtcclxuICAgIGJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgfVxyXG5cclxuICAmI3skc2V9dHJhY2sge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB9XHJcblxyXG4gICYjeyRzZX1zbGlkZXMge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcclxuICAgIHRvdWNoLWFjdGlvbjogcGFuLVk7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICB3aWxsLWNoYW5nZTogdHJhbnNmb3JtO1xyXG5cclxuICAgICYjeyRnbGlkZS1tb2RpZmllci1zZXBhcmF0b3J9ZHJhZ2dpbmcge1xyXG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gICYjeyRzZX1zbGlkZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xyXG4gICAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcclxuXHJcbiAgICBhIHtcclxuICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgIC13ZWJraXQtdXNlci1kcmFnOiBub25lO1xyXG4gICAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAmI3skc2V9YXJyb3dzIHtcclxuICAgIC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gIH1cclxuXHJcbiAgJiN7JHNlfWJ1bGxldHMge1xyXG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAmI3skc219cnRsIHtcclxuICAgIGRpcmVjdGlvbjogcnRsO1xyXG4gIH1cclxufVxyXG4iLCIuZ2xpZGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuLmdsaWRlICoge1xuICBib3gtc2l6aW5nOiBpbmhlcml0O1xufVxuLmdsaWRlX190cmFjayB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZ2xpZGVfX3NsaWRlcyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcbiAgdG91Y2gtYWN0aW9uOiBwYW4tWTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcGFkZGluZzogMDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIHdpbGwtY2hhbmdlOiB0cmFuc2Zvcm07XG59XG4uZ2xpZGVfX3NsaWRlcy0tZHJhZ2dpbmcge1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbn1cbi5nbGlkZV9fc2xpZGUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBmbGV4LXNocmluazogMDtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5nbGlkZV9fc2xpZGUgYSB7XG4gIHVzZXItc2VsZWN0OiBub25lO1xuICAtd2Via2l0LXVzZXItZHJhZzogbm9uZTtcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xufVxuLmdsaWRlX19hcnJvd3Mge1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG4gIHVzZXItc2VsZWN0OiBub25lO1xufVxuLmdsaWRlX19idWxsZXRzIHtcbiAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbn1cbi5nbGlkZS0tcnRsIHtcbiAgZGlyZWN0aW9uOiBydGw7XG59XG5cbi5wcm9qZWN0LWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwdmg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9wcm9qZWN0cy9iYWxsLWxlZnQucG5nXCIpIC0yNyUgMTA4JSBuby1yZXBlYXQsIHVybChcIi9hc3NldHMvaW1hZ2VzL3Byb2plY3RzL2JhbGwtcmlnaHQtYmlnLnBuZ1wiKSA5MyUgOTElIG5vLXJlcGVhdCwgdXJsKFwiL2Fzc2V0cy9pbWFnZXMvcHJvamVjdHMvYmFsbC1yaWdodC1zbS5wbmdcIikgNzIlIDcyJSBuby1yZXBlYXQsICNmZmY7XG59XG5cbi5wcm9qZWN0cy1oZWFkZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMCU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByb2plY3RzLWhlYWRlciBzcGFuIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByb2plY3RzLWhlYWRlciBhIHtcbiAgY29sb3I6IGluaGVyaXQ7XG59XG4ucHJvamVjdHMtaGVhZGVyIGgyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS1iaWcucG5nXCIpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiA4MCUgMzAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uLXg6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb24teTogODglO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVuc2V0O1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcbiAgYW5pbWF0aW9uOiB0aXRsZSAycztcbn1cblxuLmdsaWRlLXByb2plY3RzIHtcbiAgaGVpZ2h0OiA0NyU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IHNsaWRlciAxcyBsaW5lYXI7XG59XG5cbkBrZXlmcmFtZXMgc2xpZGVyIHtcbiAgZnJvbSB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xNTAlKTtcbiAgfVxuICB0byB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDAlKTtcbiAgfVxufVxuLnByb2plY3RzLWZvb3RlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDMzJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnByb2plY3RzLWZvb3RlciBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMjMlO1xuICBsZWZ0OiAzOCU7XG4gIGNvbG9yOiBpbmhlcml0O1xuICBwYWRkaW5nOiAwLjVyZW0gMS41cmVtO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xuICBib3JkZXItcmFkaXVzOiA3cHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xuICBhbmltYXRpb246IGJ0biAxcyBsaW5lYXI7XG59XG4ucHJvamVjdHMtZm9vdGVyIGE6aG92ZXIge1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIHRyYW5zZm9ybTogc2NhbGUoMS4xKTtcbn1cblxuQGtleWZyYW1lcyBidG4ge1xuICBmcm9tIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB9XG59IiwiQGltcG9ydCBcIm5vZGVfbW9kdWxlcy9AZ2xpZGVqcy9nbGlkZS9zcmMvYXNzZXRzL3Nhc3MvZ2xpZGUuY29yZVwiO1xyXG5cclxuJGJhbGxfMTogJy9hc3NldHMvaW1hZ2VzL3Byb2plY3RzL2JhbGwtbGVmdC5wbmcnO1xyXG4kYmFsbF8yOiAnL2Fzc2V0cy9pbWFnZXMvcHJvamVjdHMvYmFsbC1yaWdodC1iaWcucG5nJztcclxuJGJhbGxfMzogJy9hc3NldHMvaW1hZ2VzL3Byb2plY3RzL2JhbGwtcmlnaHQtc20ucG5nJztcclxuJHVuZGVybGluZTogJy9hc3NldHMvaW1hZ2VzL3VuZGVybGluZS1iaWcucG5nJztcclxuXHJcblxyXG4ucHJvamVjdC1jb250YWluZXIge1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCRiYWxsXzEpIC0yNyUgMTA4JSBuby1yZXBlYXQsIHVybCgkYmFsbF8yKSA5MyUgOTElIG5vLXJlcGVhdCwgdXJsKCRiYWxsXzMpIDcyJSA3MiUgbm8tcmVwZWF0LCAjZmZmO1xyXG59XHJcblxyXG5cclxuXHJcbi5wcm9qZWN0cy1oZWFkZXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gICAgc3BhbiB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG4gICAgYXtcclxuICAgICAgICBjb2xvcjogaW5oZXJpdDtcclxuICAgIH1cclxuXHJcbiAgICBoMiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKCR1bmRlcmxpbmUpIG5vLXJlcGVhdDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IDgwJSAzMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbi14OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb24teTogODglO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBmb250LXNpemU6IDI4cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdW5zZXQ7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4zcztcclxuICAgICAgICBhbmltYXRpb246IHRpdGxlIDJzO1xyXG4gICAgICAgIC8vICY6OmFmdGVyIHtcclxuICAgICAgICAvLyAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAvLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIC8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgLy8gICAgIHdpZHRoOiA5NiU7XHJcbiAgICAgICAgLy8gICAgIGhlaWdodDogMTBweDtcclxuICAgICAgICAvLyAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICAgIC8vICAgICByaWdodDogLTE3cHg7XHJcbiAgICAgICAgLy8gICAgIGJvdHRvbTogMnB4O1xyXG4gICAgICAgIC8vICAgICB6LWluZGV4OiAtMTtcclxuICAgICAgICAvLyB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5nbGlkZS1wcm9qZWN0cyB7XHJcbiAgICBoZWlnaHQ6IDQ3JTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XHJcbiAgICBhbmltYXRpb246IHNsaWRlciAxcyBsaW5lYXI7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgc2xpZGVyIHtcclxuICAgIGZyb20ge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTUwJSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHRvIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCUpO1xyXG5cclxuICAgIH1cclxufVxyXG5cclxuLnByb2plY3RzLWZvb3RlciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzMlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgIGEge1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYm90dG9tOiAyMyU7XHJcbiAgICAgICAgbGVmdDogMzglO1xyXG4gICAgICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgICAgIHBhZGRpbmc6IC41cmVtIDEuNXJlbTtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG4gICAgICAgIGFuaW1hdGlvbjogYnRuIDFzIGxpbmVhcjtcclxuXHJcbiAgICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpO1xyXG4gICAgICAgICAgICA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGJ0biB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuXHJcbiAgICB9XHJcbn0iXX0= */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/projects/projects.component.ts": 
        /*!************************************************************************!*\
          !*** ./src/app/modules/home/components/projects/projects.component.ts ***!
          \************************************************************************/
        /*! exports provided: ProjectsComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsComponent", function () { return ProjectsComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            /* harmony import */ var _glidejs_glide__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @glidejs/glide */ "./node_modules/@glidejs/glide/dist/glide.esm.js");
            var ProjectsComponent = /** @class */ (function () {
                function ProjectsComponent(store) {
                    this.store = store;
                    this.proj1 = '/assets/images/projects/proj1.png';
                    this.proj2 = '/assets/images/projects/proj2.png';
                    this.proj3 = '/assets/images/projects/proj3.png';
                    this.proj4 = '/assets/images/projects/proj4.png';
                    this.proj5 = '/assets/images/projects/proj5.png';
                }
                ProjectsComponent.prototype.ngOnInit = function () {
                    new _glidejs_glide__WEBPACK_IMPORTED_MODULE_3__["default"]('.glide-projects', {
                        type: 'carousel',
                        startAt: 0,
                        perView: 3
                    }).mount();
                };
                return ProjectsComponent;
            }());
            ProjectsComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            ProjectsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-projects',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./projects.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/projects/projects.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./projects.component.scss */ "./src/app/modules/home/components/projects/projects.component.scss")).default]
                })
            ], ProjectsComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/welcome/welcome.component.scss": 
        /*!************************************************************************!*\
          !*** ./src/app/modules/home/components/welcome/welcome.component.scss ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".glide {\n  position: relative;\n  width: 100%;\n  box-sizing: border-box;\n}\n.glide * {\n  box-sizing: inherit;\n}\n.glide__track {\n  overflow: hidden;\n}\n.glide__slides {\n  position: relative;\n  width: 100%;\n  list-style: none;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  transform-style: preserve-3d;\n  touch-action: pan-Y;\n  overflow: hidden;\n  padding: 0;\n  white-space: nowrap;\n  display: flex;\n  flex-wrap: nowrap;\n  will-change: transform;\n}\n.glide__slides--dragging {\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide__slide {\n  width: 100%;\n  height: 100%;\n  flex-shrink: 0;\n  white-space: normal;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n  -webkit-touch-callout: none;\n  -webkit-tap-highlight-color: transparent;\n}\n.glide__slide a {\n  -webkit-user-select: none;\n          user-select: none;\n  -webkit-user-drag: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n}\n.glide__arrows {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide__bullets {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n.glide--rtl {\n  direction: rtl;\n}\n.glide {\n  position: relative;\n  overflow: hidden;\n}\n.glide .glide__track {\n  height: 100vh;\n}\n.glide img {\n  height: 100vh;\n  width: 100vw;\n}\n.logo-welcome--absolute {\n  position: absolute;\n  top: 5%;\n  left: 5%;\n  z-index: 99;\n}\n.welcome__h2 {\n  position: absolute;\n  top: 25%;\n  left: 5%;\n  z-index: 99;\n  color: #fff;\n  width: 60%;\n  font-size: 60px;\n  transition: all 0.3s;\n  -webkit-animation: title 2s;\n          animation: title 2s;\n}\n.button-container {\n  position: absolute;\n  top: 45%;\n  left: 5%;\n  z-index: 99;\n  color: #fff;\n  width: 40%;\n  font-size: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvd2VsY29tZS9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvbm9kZV9tb2R1bGVzXFxAZ2xpZGVqc1xcZ2xpZGVcXHNyY1xcYXNzZXRzXFxzYXNzXFxnbGlkZS5jb3JlLnNjc3MiLCJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL3dlbGNvbWUvd2VsY29tZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbW9kdWxlcy9ob21lL2NvbXBvbmVudHMvd2VsY29tZS9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXG1vZHVsZXNcXGhvbWVcXGNvbXBvbmVudHNcXHdlbGNvbWVcXHdlbGNvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFNRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQ05GO0FEUUU7RUFDRSxtQkFBQTtBQ05KO0FEU0U7RUFDRSxnQkFBQTtBQ1BKO0FEVUU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7RUFDQSw0QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ1JKO0FEVUk7RUFDRSx5QkFBQTtLQUFBLHNCQUFBO01BQUEscUJBQUE7VUFBQSxpQkFBQTtBQ1JOO0FEWUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0tBQUEsc0JBQUE7TUFBQSxxQkFBQTtVQUFBLGlCQUFBO0VBQ0EsMkJBQUE7RUFDQSx3Q0FBQTtBQ1ZKO0FEWUk7RUFDRSx5QkFBQTtVQUFBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0FDVk47QURjRTtFQUNFLDJCQUFBO0VBQ0EseUJBQUE7S0FBQSxzQkFBQTtNQUFBLHFCQUFBO1VBQUEsaUJBQUE7QUNaSjtBRGVFO0VBQ0UsMkJBQUE7RUFDQSx5QkFBQTtLQUFBLHNCQUFBO01BQUEscUJBQUE7VUFBQSxpQkFBQTtBQ2JKO0FEZ0JFO0VBQ0UsY0FBQTtBQ2RKO0FDbERBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBRHFESjtBQ3BESTtFQUNJLGFBQUE7QURzRFI7QUNwREk7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBRHNEUjtBQ2xEQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0FEcURKO0FDbERBO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLDJCQUFBO1VBQUEsbUJBQUE7QURxREo7QUNsREE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBRHFESiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvaG9tZS9jb21wb25lbnRzL3dlbGNvbWUvd2VsY29tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCJ2YXJpYWJsZXNcIjtcclxuXHJcbi4jeyRnbGlkZS1jbGFzc30ge1xyXG4gICR0aGlzOiAmO1xyXG5cclxuICAkc2U6ICRnbGlkZS1lbGVtZW50LXNlcGFyYXRvcjtcclxuICAkc206ICRnbGlkZS1tb2RpZmllci1zZXBhcmF0b3I7XHJcblxyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG5cclxuICAqIHtcclxuICAgIGJveC1zaXppbmc6IGluaGVyaXQ7XHJcbiAgfVxyXG5cclxuICAmI3skc2V9dHJhY2sge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB9XHJcblxyXG4gICYjeyRzZX1zbGlkZXMge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcclxuICAgIHRvdWNoLWFjdGlvbjogcGFuLVk7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICB3aWxsLWNoYW5nZTogdHJhbnNmb3JtO1xyXG5cclxuICAgICYjeyRnbGlkZS1tb2RpZmllci1zZXBhcmF0b3J9ZHJhZ2dpbmcge1xyXG4gICAgICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gICYjeyRzZX1zbGlkZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xyXG4gICAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcclxuXHJcbiAgICBhIHtcclxuICAgICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICAgIC13ZWJraXQtdXNlci1kcmFnOiBub25lO1xyXG4gICAgICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4gICAgICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAmI3skc2V9YXJyb3dzIHtcclxuICAgIC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcclxuICAgIHVzZXItc2VsZWN0OiBub25lO1xyXG4gIH1cclxuXHJcbiAgJiN7JHNlfWJ1bGxldHMge1xyXG4gICAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xyXG4gICAgdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgfVxyXG5cclxuICAmI3skc219cnRsIHtcclxuICAgIGRpcmVjdGlvbjogcnRsO1xyXG4gIH1cclxufVxyXG4iLCIuZ2xpZGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuLmdsaWRlICoge1xuICBib3gtc2l6aW5nOiBpbmhlcml0O1xufVxuLmdsaWRlX190cmFjayB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uZ2xpZGVfX3NsaWRlcyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgdHJhbnNmb3JtLXN0eWxlOiBwcmVzZXJ2ZS0zZDtcbiAgdG91Y2gtYWN0aW9uOiBwYW4tWTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcGFkZGluZzogMDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIHdpbGwtY2hhbmdlOiB0cmFuc2Zvcm07XG59XG4uZ2xpZGVfX3NsaWRlcy0tZHJhZ2dpbmcge1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbn1cbi5nbGlkZV9fc2xpZGUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBmbGV4LXNocmluazogMDtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC13ZWJraXQtdG91Y2gtY2FsbG91dDogbm9uZTtcbiAgLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cbi5nbGlkZV9fc2xpZGUgYSB7XG4gIHVzZXItc2VsZWN0OiBub25lO1xuICAtd2Via2l0LXVzZXItZHJhZzogbm9uZTtcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xufVxuLmdsaWRlX19hcnJvd3Mge1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG4gIHVzZXItc2VsZWN0OiBub25lO1xufVxuLmdsaWRlX19idWxsZXRzIHtcbiAgLXdlYmtpdC10b3VjaC1jYWxsb3V0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbn1cbi5nbGlkZS0tcnRsIHtcbiAgZGlyZWN0aW9uOiBydGw7XG59XG5cbi5nbGlkZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5nbGlkZSAuZ2xpZGVfX3RyYWNrIHtcbiAgaGVpZ2h0OiAxMDB2aDtcbn1cbi5nbGlkZSBpbWcge1xuICBoZWlnaHQ6IDEwMHZoO1xuICB3aWR0aDogMTAwdnc7XG59XG5cbi5sb2dvLXdlbGNvbWUtLWFic29sdXRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUlO1xuICBsZWZ0OiA1JTtcbiAgei1pbmRleDogOTk7XG59XG5cbi53ZWxjb21lX19oMiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAyNSU7XG4gIGxlZnQ6IDUlO1xuICB6LWluZGV4OiA5OTtcbiAgY29sb3I6ICNmZmY7XG4gIHdpZHRoOiA2MCU7XG4gIGZvbnQtc2l6ZTogNjBweDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG4gIGFuaW1hdGlvbjogdGl0bGUgMnM7XG59XG5cbi5idXR0b24tY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDQ1JTtcbiAgbGVmdDogNSU7XG4gIHotaW5kZXg6IDk5O1xuICBjb2xvcjogI2ZmZjtcbiAgd2lkdGg6IDQwJTtcbiAgZm9udC1zaXplOiA2MHB4O1xufSIsIkBpbXBvcnQgXCJub2RlX21vZHVsZXMvQGdsaWRlanMvZ2xpZGUvc3JjL2Fzc2V0cy9zYXNzL2dsaWRlLmNvcmVcIjtcclxuXHJcblxyXG4uZ2xpZGUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIC5nbGlkZV9fdHJhY2sge1xyXG4gICAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICB9XHJcbiAgICBpbWcge1xyXG4gICAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICAgICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubG9nby13ZWxjb21lLS1hYnNvbHV0ZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDUlO1xyXG4gICAgbGVmdDogNSU7XHJcbiAgICB6LWluZGV4OiA5OTtcclxufVxyXG5cclxuLndlbGNvbWVfX2gyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMjUlO1xyXG4gICAgbGVmdDogNSU7XHJcbiAgICB6LWluZGV4OiA5OTtcclxuICAgIGNvbG9yOiAjZmZmOyAgXHJcbiAgICB3aWR0aDogNjAlO1xyXG4gICAgZm9udC1zaXplOiA2MHB4O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4zcztcclxuICAgIGFuaW1hdGlvbjogdGl0bGUgMnM7XHJcbn1cclxuXHJcbi5idXR0b24tY29udGFpbmVye1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA0NSU7XHJcbiAgICBsZWZ0OiA1JTtcclxuICAgIHotaW5kZXg6IDk5O1xyXG4gICAgY29sb3I6ICNmZmY7ICBcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgICBmb250LXNpemU6IDYwcHg7XHJcbiAgICAuYnV0dG9uLWNvbnRhaW5lcl9fYSB7XHJcbiAgICAgICBcclxuICAgIH1cclxufSJdfQ== */");
            /***/ 
        }),
        /***/ "./src/app/modules/home/components/welcome/welcome.component.ts": 
        /*!**********************************************************************!*\
          !*** ./src/app/modules/home/components/welcome/welcome.component.ts ***!
          \**********************************************************************/
        /*! exports provided: WelcomeComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function () { return WelcomeComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _ngxs_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngxs/store */ "./node_modules/@ngxs/store/fesm2015/ngxs-store.js");
            /* harmony import */ var _glidejs_glide__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @glidejs/glide */ "./node_modules/@glidejs/glide/dist/glide.esm.js");
            var WelcomeComponent = /** @class */ (function () {
                function WelcomeComponent(store) {
                    this.store = store;
                    this.logoWelcome = "/assets/images/index/logo-slider.png";
                    this.slide1 = "/assets/images/index/slider1.png";
                    this.slide2 = "/assets/images/index/slider1.png";
                    this.slide3 = "/assets/images/index/slider1.png";
                    this.slide4 = "/assets/images/index/slider1.png";
                }
                WelcomeComponent.prototype.ngOnInit = function () {
                    new _glidejs_glide__WEBPACK_IMPORTED_MODULE_3__["default"]('.glide').mount({
                        type: 'carousel',
                        startAt: 0,
                        perView: 1,
                        peek: {
                            before: 0,
                            after: 0
                        }
                    });
                };
                return WelcomeComponent;
            }());
            WelcomeComponent.ctorParameters = function () { return [
                { type: _ngxs_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
            ]; };
            WelcomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-welcome',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./welcome.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/modules/home/components/welcome/welcome.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./welcome.component.scss */ "./src/app/modules/home/components/welcome/welcome.component.scss")).default]
                })
            ], WelcomeComponent);
            /***/ 
        }),
        /***/ "./src/app/modules/home/home.modules.ts": 
        /*!**********************************************!*\
          !*** ./src/app/modules/home/home.modules.ts ***!
          \**********************************************/
        /*! exports provided: HomeModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function () { return HomeModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var HomeModule = /** @class */ (function () {
                function HomeModule() {
                }
                return HomeModule;
            }());
            HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [],
                    imports: [],
                    providers: [],
                    bootstrap: []
                })
            ], HomeModule);
            /***/ 
        }),
        /***/ "./src/app/shared/components/error404/error404.component.scss": 
        /*!********************************************************************!*\
          !*** ./src/app/shared/components/error404/error404.component.scss ***!
          \********************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2Vycm9yNDA0L2Vycm9yNDA0LmNvbXBvbmVudC5zY3NzIn0= */");
            /***/ 
        }),
        /***/ "./src/app/shared/components/error404/error404.component.ts": 
        /*!******************************************************************!*\
          !*** ./src/app/shared/components/error404/error404.component.ts ***!
          \******************************************************************/
        /*! exports provided: Error404Component */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Error404Component", function () { return Error404Component; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var Error404Component = /** @class */ (function () {
                function Error404Component() {
                }
                Error404Component.prototype.ngOnInit = function () {
                };
                return Error404Component;
            }());
            Error404Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-error404',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./error404.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/error404/error404.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./error404.component.scss */ "./src/app/shared/components/error404/error404.component.scss")).default]
                })
            ], Error404Component);
            /***/ 
        }),
        /***/ "./src/app/shared/components/navigation/navigation.component.scss": 
        /*!************************************************************************!*\
          !*** ./src/app/shared/components/navigation/navigation.component.scss ***!
          \************************************************************************/
        /*! exports provided: default */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony default export */ __webpack_exports__["default"] = (".navigation-wrapper {\n  font-family: \"circleExtraBold\";\n  position: relative;\n  height: 100vh;\n}\n.navigation-wrapper .navigation-wrapper__logo {\n  position: fixed;\n  padding-left: 2.5%;\n  padding-top: 2%;\n}\n.navigation-wrapper .navigation-wrapper__logo img {\n  height: 60px;\n  width: 65px;\n  margin-top: 11%;\n  margin-bottom: 22%;\n}\n.navigation-wrapper .navigation-wrapper__list {\n  position: fixed;\n  top: 20%;\n  width: 13%;\n}\n.navigation-wrapper .navigation-wrapper__list a {\n  color: inherit;\n  height: 8vh;\n  display: inline-block;\n  padding-left: 15%;\n  text-transform: uppercase;\n  width: 100%;\n}\n.navigation-wrapper .navigation-wrapper__list a:hover {\n  text-decoration: none;\n}\n.navigation-wrapper .navigation-wrapper__socials-container {\n  display: flex;\n  justify-content: space-around;\n  position: fixed;\n  width: 13%;\n  margin: auto;\n  bottom: 10%;\n  padding: 0 1%;\n}\n.navigation-wrapper .navigation-wrapper__socials-container img {\n  cursor: pointer;\n  -webkit-filter: opacity(0.5);\n          filter: opacity(0.5);\n  transition: all 0.5s;\n}\n.navigation-wrapper .navigation-wrapper__socials-container img:hover {\n  -webkit-filter: opacity(1);\n          filter: opacity(1);\n}\n.navigation-wrapper .navigation-wrapper__socials-container img:nth-child(1) {\n  width: 16px;\n  height: 15px;\n  -webkit-animation: socials 1s linear;\n          animation: socials 1s linear;\n}\n.navigation-wrapper .navigation-wrapper__socials-container img:nth-child(2) {\n  width: 8px;\n  height: 15px;\n  -webkit-animation: socials 1.5s linear;\n          animation: socials 1.5s linear;\n}\n.navigation-wrapper .navigation-wrapper__socials-container img:nth-child(3) {\n  width: 16px;\n  height: 15px;\n  -webkit-animation: socials 2s linear;\n          animation: socials 2s linear;\n}\n.navigation-wrapper .navigation-wrapper__socials-container img:nth-child(4) {\n  width: 21px;\n  height: 15px;\n  -webkit-animation: socials 3s linear;\n          animation: socials 3s linear;\n}\n@-webkit-keyframes socials {\n  from {\n    transform: scale(0);\n  }\n  to {\n    transform: scale(1);\n  }\n}\n@keyframes socials {\n  from {\n    transform: scale(0);\n  }\n  to {\n    transform: scale(1);\n  }\n}\n.menuLang-absolute {\n  display: inline-block;\n  position: fixed;\n  z-index: 2;\n  top: 15px;\n  right: 3%;\n  width: 11%;\n  height: 5%;\n  color: #4e4e4e;\n  font-weight: bold;\n}\n.menuLang-absolute a {\n  margin: auto;\n  display: inline-block;\n  color: #d4002d;\n  margin-right: 8px;\n}\n.menuLang-absolute a:hover {\n  text-decoration: none;\n}\n.menuLang-absolute #menu-toggle {\n  display: inline-block;\n  position: absolute;\n  width: 30%;\n  height: 100%;\n  top: 3%;\n  right: 3%;\n}\n.menuLang-absolute #menu-toggle div {\n  display: inline-block;\n  width: 33px;\n  height: 1px;\n  position: fixed;\n  background: #d4002d;\n  top: 27px;\n  right: 4%;\n  transition: all 0.5s ease !important;\n}\n.menuLang-absolute #menu-toggle div::before {\n  content: \"\";\n  display: inline-block;\n  transition: all 0.5s ease !important;\n  width: 33px;\n  height: 1px;\n  position: fixed;\n  background: #d4002d;\n  top: 19px;\n  right: 4%;\n}\n.menuLang-absolute #menu-toggle div::after {\n  content: \"\";\n  display: inline-block;\n  transition: all 0.5s ease !important;\n  width: 33px;\n  height: 1px;\n  position: fixed;\n  background: #d4002d;\n  top: 35px;\n  right: 4%;\n}\n.menu-close div {\n  background: transparent !important;\n}\n.menu-close div::before {\n  transform: rotate(45deg) !important;\n  top: 27px !important;\n}\n.menu-close div::after {\n  transform: rotate(-45deg) !important;\n  top: 27px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9DOlxcT1NQYW5lbFxcZG9tYWluc1xcYV9sdXhfX2Nhcml0YXNcXGNhcml0YXMvc3JjXFxhcHBcXHNoYXJlZFxcY29tcG9uZW50c1xcbmF2aWdhdGlvblxcbmF2aWdhdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksOEJBQUE7RUFrQkEsa0JBQUE7RUFDQSxhQUFBO0FDaEJKO0FEQUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDRVI7QURBUTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDRVo7QURPSTtFQUNJLGVBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtBQ0xSO0FET1E7RUFDSSxjQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUNMWjtBRE9ZO0VBQ0kscUJBQUE7QUNMaEI7QURXSTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FDVFI7QURXUTtFQUNJLGVBQUE7RUFDQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0Esb0JBQUE7QUNUWjtBRFdZO0VBQ0ksMEJBQUE7VUFBQSxrQkFBQTtBQ1RoQjtBRFlZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtVQUFBLDRCQUFBO0FDVmhCO0FEYVk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7QUNYaEI7QURjWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esb0NBQUE7VUFBQSw0QkFBQTtBQ1poQjtBRGVZO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxvQ0FBQTtVQUFBLDRCQUFBO0FDYmhCO0FEdUJBO0VBQ0k7SUFDSSxtQkFBQTtFQ3JCTjtFRHdCRTtJQUNJLG1CQUFBO0VDdEJOO0FBQ0Y7QURlQTtFQUNJO0lBQ0ksbUJBQUE7RUNyQk47RUR3QkU7SUFDSSxtQkFBQTtFQ3RCTjtBQUNGO0FEMEJBO0VBQ0kscUJBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUN4Qko7QUR5Qkk7RUFDSSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUN2QlI7QUR3QlE7RUFDSSxxQkFBQTtBQ3RCWjtBRDBCSTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLE9BQUE7RUFDSixTQUFBO0FDeEJKO0FEeUJRO0VBQ0kscUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0Esb0NBQUE7QUN2Qlo7QUR3Qlk7RUFDSSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxvQ0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLFNBQUE7QUN0QmhCO0FEeUJZO0VBQ0ksV0FBQTtFQUNBLHFCQUFBO0VBQ0Esb0NBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0FDdkJoQjtBRGlDSTtFQUNHLGtDQUFBO0FDOUJQO0FEK0JRO0VBQ0ksbUNBQUE7RUFDQSxvQkFBQTtBQzdCWjtBRCtCUTtFQUNJLG9DQUFBO0VBQ0Esb0JBQUE7QUM3QloiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2aWdhdGlvbi13cmFwcGVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiBcImNpcmNsZUV4dHJhQm9sZFwiO1xyXG5cclxuICAgIC8vIGJveC1zaGFkb3c6IDAgMCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgIC5uYXZpZ2F0aW9uLXdyYXBwZXJfX2xvZ28ge1xyXG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDIuNSU7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDIlO1xyXG5cclxuICAgICAgICBpbWcge1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA2NXB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMSU7XHJcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDIyJTtcclxuXHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG5cclxuICAgIC5uYXZpZ2F0aW9uLXdyYXBwZXJfX2xpc3Qge1xyXG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICAgICB0b3A6IDIwJTtcclxuICAgICAgICB3aWR0aDogMTMlO1xyXG5cclxuICAgICAgICBhIHtcclxuICAgICAgICAgICAgY29sb3I6IGluaGVyaXQ7XHJcbiAgICAgICAgICAgIGhlaWdodDogOHZoO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTUlO1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAubmF2aWdhdGlvbi13cmFwcGVyX19zb2NpYWxzLWNvbnRhaW5lciB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgd2lkdGg6IDEzJTtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgYm90dG9tOiAxMCU7XHJcbiAgICAgICAgcGFkZGluZzogMCAxJTtcclxuXHJcbiAgICAgICAgaW1nIHtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICBmaWx0ZXI6IG9wYWNpdHkoMC41KTtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC41cztcclxuXHJcbiAgICAgICAgICAgICY6aG92ZXIge1xyXG4gICAgICAgICAgICAgICAgZmlsdGVyOiBvcGFjaXR5KDEpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOm50aC1jaGlsZCgxKSB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTZweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTVweDtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGlvbjogc29jaWFscyAxcyBsaW5lYXI7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6bnRoLWNoaWxkKDIpIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA4cHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb246IHNvY2lhbHMgMS41cyBsaW5lYXI7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICY6bnRoLWNoaWxkKDMpIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxNnB4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uOiBzb2NpYWxzIDJzIGxpbmVhcjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgJjpudGgtY2hpbGQoNCkge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDIxcHg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb246IHNvY2lhbHMgM3MgbGluZWFyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC53cmFwcGVyLW1pc3Npb24ge31cclxuXHJcbn1cclxuXHJcblxyXG5Aa2V5ZnJhbWVzIHNvY2lhbHMge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi5tZW51TGFuZy1hYnNvbHV0ZSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB6LWluZGV4OiAyO1xyXG4gICAgdG9wOiAxNXB4O1xyXG4gICAgcmlnaHQ6IDMlO1xyXG4gICAgd2lkdGg6IDExJTtcclxuICAgIGhlaWdodDogNSU7XHJcbiAgICBjb2xvcjogIzRlNGU0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgYSB7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBjb2xvcjogI2Q0MDAyZDtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgICNtZW51LXRvZ2dsZSB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB3aWR0aDogMzAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB0b3A6IDMlO1xyXG4gICAgcmlnaHQ6IDMlO1xyXG4gICAgICAgIGRpdiB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgd2lkdGg6IDMzcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogMXB4O1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNkNDAwMmQ7XHJcbiAgICAgICAgICAgIHRvcDogMjdweDtcclxuICAgICAgICAgICAgcmlnaHQ6IDQlO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjVzIGVhc2UgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgJjo6YmVmb3JlIHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjVzIGVhc2UgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzM3B4O1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxcHg7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZDQwMDJkO1xyXG4gICAgICAgICAgICAgICAgdG9wOiAxOXB4O1xyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IDQlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAmOjphZnRlciB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC41cyBlYXNlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMzNweDtcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMXB4O1xyXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2Q0MDAyZDtcclxuICAgICAgICAgICAgICAgIHRvcDogMzVweDtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiA0JTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgIFxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLm1lbnUtY2xvc2V7XHJcbiAgICBkaXZ7XHJcbiAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG4gICAgICAgICY6OmJlZm9yZXtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIHRvcDogMjdweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICAmOjphZnRlcntcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKSAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICB0b3A6IDI3cHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG5cclxuXHJcbiIsIi5uYXZpZ2F0aW9uLXdyYXBwZXIge1xuICBmb250LWZhbWlseTogXCJjaXJjbGVFeHRyYUJvbGRcIjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBoZWlnaHQ6IDEwMHZoO1xufVxuLm5hdmlnYXRpb24td3JhcHBlciAubmF2aWdhdGlvbi13cmFwcGVyX19sb2dvIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBwYWRkaW5nLWxlZnQ6IDIuNSU7XG4gIHBhZGRpbmctdG9wOiAyJTtcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fbG9nbyBpbWcge1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiA2NXB4O1xuICBtYXJnaW4tdG9wOiAxMSU7XG4gIG1hcmdpbi1ib3R0b206IDIyJTtcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fbGlzdCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAyMCU7XG4gIHdpZHRoOiAxMyU7XG59XG4ubmF2aWdhdGlvbi13cmFwcGVyIC5uYXZpZ2F0aW9uLXdyYXBwZXJfX2xpc3QgYSB7XG4gIGNvbG9yOiBpbmhlcml0O1xuICBoZWlnaHQ6IDh2aDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nLWxlZnQ6IDE1JTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgd2lkdGg6IDEwMCU7XG59XG4ubmF2aWdhdGlvbi13cmFwcGVyIC5uYXZpZ2F0aW9uLXdyYXBwZXJfX2xpc3QgYTpob3ZlciB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fc29jaWFscy1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTMlO1xuICBtYXJnaW46IGF1dG87XG4gIGJvdHRvbTogMTAlO1xuICBwYWRkaW5nOiAwIDElO1xufVxuLm5hdmlnYXRpb24td3JhcHBlciAubmF2aWdhdGlvbi13cmFwcGVyX19zb2NpYWxzLWNvbnRhaW5lciBpbWcge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZpbHRlcjogb3BhY2l0eSgwLjUpO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fc29jaWFscy1jb250YWluZXIgaW1nOmhvdmVyIHtcbiAgZmlsdGVyOiBvcGFjaXR5KDEpO1xufVxuLm5hdmlnYXRpb24td3JhcHBlciAubmF2aWdhdGlvbi13cmFwcGVyX19zb2NpYWxzLWNvbnRhaW5lciBpbWc6bnRoLWNoaWxkKDEpIHtcbiAgd2lkdGg6IDE2cHg7XG4gIGhlaWdodDogMTVweDtcbiAgYW5pbWF0aW9uOiBzb2NpYWxzIDFzIGxpbmVhcjtcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fc29jaWFscy1jb250YWluZXIgaW1nOm50aC1jaGlsZCgyKSB7XG4gIHdpZHRoOiA4cHg7XG4gIGhlaWdodDogMTVweDtcbiAgYW5pbWF0aW9uOiBzb2NpYWxzIDEuNXMgbGluZWFyO1xufVxuLm5hdmlnYXRpb24td3JhcHBlciAubmF2aWdhdGlvbi13cmFwcGVyX19zb2NpYWxzLWNvbnRhaW5lciBpbWc6bnRoLWNoaWxkKDMpIHtcbiAgd2lkdGg6IDE2cHg7XG4gIGhlaWdodDogMTVweDtcbiAgYW5pbWF0aW9uOiBzb2NpYWxzIDJzIGxpbmVhcjtcbn1cbi5uYXZpZ2F0aW9uLXdyYXBwZXIgLm5hdmlnYXRpb24td3JhcHBlcl9fc29jaWFscy1jb250YWluZXIgaW1nOm50aC1jaGlsZCg0KSB7XG4gIHdpZHRoOiAyMXB4O1xuICBoZWlnaHQ6IDE1cHg7XG4gIGFuaW1hdGlvbjogc29jaWFscyAzcyBsaW5lYXI7XG59XG5Aa2V5ZnJhbWVzIHNvY2lhbHMge1xuICBmcm9tIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xuICB9XG4gIHRvIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB9XG59XG4ubWVudUxhbmctYWJzb2x1dGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMjtcbiAgdG9wOiAxNXB4O1xuICByaWdodDogMyU7XG4gIHdpZHRoOiAxMSU7XG4gIGhlaWdodDogNSU7XG4gIGNvbG9yOiAjNGU0ZTRlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5tZW51TGFuZy1hYnNvbHV0ZSBhIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGNvbG9yOiAjZDQwMDJkO1xuICBtYXJnaW4tcmlnaHQ6IDhweDtcbn1cbi5tZW51TGFuZy1hYnNvbHV0ZSBhOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLm1lbnVMYW5nLWFic29sdXRlICNtZW51LXRvZ2dsZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMzAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRvcDogMyU7XG4gIHJpZ2h0OiAzJTtcbn1cbi5tZW51TGFuZy1hYnNvbHV0ZSAjbWVudS10b2dnbGUgZGl2IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMzNweDtcbiAgaGVpZ2h0OiAxcHg7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYmFja2dyb3VuZDogI2Q0MDAyZDtcbiAgdG9wOiAyN3B4O1xuICByaWdodDogNCU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UgIWltcG9ydGFudDtcbn1cbi5tZW51TGFuZy1hYnNvbHV0ZSAjbWVudS10b2dnbGUgZGl2OjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UgIWltcG9ydGFudDtcbiAgd2lkdGg6IDMzcHg7XG4gIGhlaWdodDogMXB4O1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJhY2tncm91bmQ6ICNkNDAwMmQ7XG4gIHRvcDogMTlweDtcbiAgcmlnaHQ6IDQlO1xufVxuLm1lbnVMYW5nLWFic29sdXRlICNtZW51LXRvZ2dsZSBkaXY6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAzM3B4O1xuICBoZWlnaHQ6IDFweDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBiYWNrZ3JvdW5kOiAjZDQwMDJkO1xuICB0b3A6IDM1cHg7XG4gIHJpZ2h0OiA0JTtcbn1cblxuLm1lbnUtY2xvc2UgZGl2IHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cbi5tZW51LWNsb3NlIGRpdjo6YmVmb3JlIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpICFpbXBvcnRhbnQ7XG4gIHRvcDogMjdweCAhaW1wb3J0YW50O1xufVxuLm1lbnUtY2xvc2UgZGl2OjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZykgIWltcG9ydGFudDtcbiAgdG9wOiAyN3B4ICFpbXBvcnRhbnQ7XG59Il19 */");
            /***/ 
        }),
        /***/ "./src/app/shared/components/navigation/navigation.component.ts": 
        /*!**********************************************************************!*\
          !*** ./src/app/shared/components/navigation/navigation.component.ts ***!
          \**********************************************************************/
        /*! exports provided: NavigationComponent */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function () { return NavigationComponent; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var NavigationComponent = /** @class */ (function () {
                //
                function NavigationComponent() {
                    this.logoSrc = "/assets/images/caritas-lgo.png";
                    //Social SVG links
                    this.instagram = "/assets/images/instagramLogo.png";
                    this.facebook = "/assets/images/facebookLogo.png";
                    this.twitter = "/assets/images/twitterLogo.png";
                    this.youtube = "/assets/images/youtubeLogo.png";
                }
                NavigationComponent.prototype.ngOnInit = function () {
                    var tog = window.document.getElementById("menu-toggle");
                    var nav = window.document.getElementsByClassName("global__container__menu")[0];
                    var content = window.document.getElementsByClassName("global__container__content")[0];
                    tog.addEventListener('click', function (e) {
                        tog.classList.toggle("menu-close");
                        nav.classList.toggle("hide-nav");
                        content.classList.toggle("hide-menu");
                    });
                };
                return NavigationComponent;
            }());
            NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
                    selector: 'app-navigation',
                    template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navigation.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/navigation/navigation.component.html")).default,
                    styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navigation.component.scss */ "./src/app/shared/components/navigation/navigation.component.scss")).default]
                })
            ], NavigationComponent);
            /***/ 
        }),
        /***/ "./src/app/shared/shared.modules.ts": 
        /*!******************************************!*\
          !*** ./src/app/shared/shared.modules.ts ***!
          \******************************************/
        /*! exports provided: SharedModule */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function () { return SharedModule; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var SharedModule = /** @class */ (function () {
                function SharedModule() {
                }
                return SharedModule;
            }());
            SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
                    declarations: [],
                    imports: [],
                    providers: [],
                    bootstrap: []
                })
            ], SharedModule);
            /***/ 
        }),
        /***/ "./src/app/wheel.directive.ts": 
        /*!************************************!*\
          !*** ./src/app/wheel.directive.ts ***!
          \************************************/
        /*! exports provided: InputMagicWheelDirective */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputMagicWheelDirective", function () { return InputMagicWheelDirective; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            var WheelType;
            (function (WheelType) {
                WheelType["INTEGER"] = "integer";
                WheelType["DECIMAL"] = "decimal";
            })(WheelType || (WheelType = {}));
            var WheelOperator;
            (function (WheelOperator) {
                WheelOperator[WheelOperator["INCREASE"] = 0] = "INCREASE";
                WheelOperator[WheelOperator["DECREASE"] = 1] = "DECREASE";
            })(WheelOperator || (WheelOperator = {}));
            var InputMagicWheelDirective = /** @class */ (function () {
                function InputMagicWheelDirective(el, router) {
                    var _a;
                    var _this = this;
                    this.el = el;
                    this.router = router;
                    this.wheelOn = WheelType.INTEGER;
                    this.wheelDecimalPlaces = 2;
                    this.wheelUpNumber = 1;
                    this.wheelDownNumber = 1;
                    this.ngModelChange = new _angular_core__WEBPACK_IMPORTED_MODULE_2__["EventEmitter"]();
                    this.operators = (_a = {},
                        _a[WheelOperator.INCREASE] = function (a, b) {
                            _this.handleParse(a) + _this.handleParse(b);
                            var arrayUp = ["/contacts", "/archive", "/charity", "/news", "/projects", "/map", "/about", "/"];
                            var arrayCurrent = arrayUp.indexOf(location.pathname);
                            var arrayStorage = arrayCurrent + 1;
                            _this.router.navigateByUrl((arrayUp[arrayStorage]));
                            console.log(location.pathname);
                            console.log(arrayCurrent);
                            console.log(arrayStorage);
                        },
                        _a[WheelOperator.DECREASE] = function (a, b) {
                            _this.handleParse(a) - _this.handleParse(b);
                            var arrayDown = ["/", "/about", "/map", "/projects", "/news", "/charity", "/archive", "/contacts"];
                            var arrayCurrent = arrayDown.indexOf(location.pathname);
                            var arrayStorage = arrayCurrent + 1;
                            _this.router.navigateByUrl((arrayDown[arrayStorage]));
                            console.log(location.pathname);
                            console.log(arrayCurrent);
                            console.log(arrayStorage);
                        },
                        _a);
                    //el.nativeElement.value = this.handleParse(el.nativeElement.value);
                    el.nativeElement.step = this.wheelUpNumber;
                }
                InputMagicWheelDirective.prototype.onMouseWheel = function (event) {
                    var nativeValue = this.handleParse(this.el.nativeElement.value);
                    if (event.wheelDelta > 0) {
                        this.el.nativeElement.value = this.handleOperation(nativeValue, WheelOperator.INCREASE);
                    }
                    else {
                        this.el.nativeElement.value = this.handleOperation(nativeValue, WheelOperator.DECREASE);
                    }
                    //propagate ngModel changes
                    this.ngModelChange.emit(this.el.nativeElement.value);
                    return false;
                };
                InputMagicWheelDirective.prototype.handleOperation = function (value, operator) {
                    return this.handleParse(this.operators[operator](value, this.getRangeNumber(operator)));
                };
                InputMagicWheelDirective.prototype.getRangeNumber = function (operator) {
                    if (operator === WheelOperator.INCREASE) {
                        return this.wheelUpNumber;
                    }
                    else if (operator === WheelOperator.DECREASE) {
                        return this.wheelDownNumber;
                    }
                };
                InputMagicWheelDirective.prototype.handleParse = function (value) {
                    if (this.wheelOn === WheelType.INTEGER) {
                        return parseInt(value, 10);
                    }
                    else if (this.wheelOn === WheelType.DECIMAL) {
                        return +parseFloat(value).toFixed(this.wheelDecimalPlaces);
                    }
                };
                InputMagicWheelDirective.prototype.ngOnInit = function () {
                    var routerDown = 0;
                    var array = window.document.querySelectorAll(" #hidden-target a");
                    console.log(array[0]);
                };
                return InputMagicWheelDirective;
            }());
            InputMagicWheelDirective.ctorParameters = function () { return [
                { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] },
                { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
            ]; };
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])()
            ], InputMagicWheelDirective.prototype, "wheelOn", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])()
            ], InputMagicWheelDirective.prototype, "wheelDecimalPlaces", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])()
            ], InputMagicWheelDirective.prototype, "wheelUpNumber", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])()
            ], InputMagicWheelDirective.prototype, "wheelDownNumber", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Output"])()
            ], InputMagicWheelDirective.prototype, "ngModelChange", void 0);
            tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostListener"])("mousewheel", ["$event"])
            ], InputMagicWheelDirective.prototype, "onMouseWheel", null);
            InputMagicWheelDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Directive"])({
                    selector: "[wheelOn]"
                })
            ], InputMagicWheelDirective);
            /***/ 
        }),
        /***/ "./src/environments/environment.ts": 
        /*!*****************************************!*\
          !*** ./src/environments/environment.ts ***!
          \*****************************************/
        /*! exports provided: environment */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function () { return environment; });
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            // This file can be replaced during build by using the `fileReplacements` array.
            // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
            // The list of file replacements can be found in `angular.json`.
            var environment = {
                production: false
            };
            /*
             * For easier debugging in development mode, you can import the following file
             * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
             *
             * This import should be commented out in production mode because it will have a negative impact
             * on performance if an error is thrown.
             */
            // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
            /***/ 
        }),
        /***/ "./src/main.ts": 
        /*!*********************!*\
          !*** ./src/main.ts ***!
          \*********************/
        /*! no exports provided */
        /***/ (function (module, __webpack_exports__, __webpack_require__) {
            "use strict";
            __webpack_require__.r(__webpack_exports__);
            /* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
            /* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
            /* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
            /* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
            /* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
            /* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/ __webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_5__);
            if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
                Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
            }
            Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
                .catch(function (err) { return console.error(err); });
            /***/ 
        }),
        /***/ 0: 
        /*!***************************!*\
          !*** multi ./src/main.ts ***!
          \***************************/
        /*! no static exports found */
        /***/ (function (module, exports, __webpack_require__) {
            module.exports = __webpack_require__(/*! C:\OSPanel\domains\a_lux__caritas\caritas\src\main.ts */ "./src/main.ts");
            /***/ 
        })
    }, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
//# sourceMappingURL=main-es5.js.map
//# sourceMappingURL=main-es5.js.map