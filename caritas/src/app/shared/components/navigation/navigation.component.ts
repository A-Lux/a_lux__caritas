import { Component, OnInit } from '@angular/core';

declare function require(name:string);

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})


export class NavigationComponent implements OnInit {
  logoSrc = "/assets/images/caritas-lgo.png";
  //Social SVG links
  instagram = "/assets/images/instagramLogo.png";
  facebook = "/assets/images/facebookLogo.png";
  twitter = "/assets/images/twitterLogo.png";
  youtube = "/assets/images/youtubeLogo.png";
  //
  constructor() { }

  ngOnInit() {
     var tog =window.document.getElementById("menu-toggle");
     var nav = window.document.getElementsByClassName("global__container__menu")[0];
     var content = window.document.getElementsByClassName("global__container__content")[0];
     tog.addEventListener('click', function(e) {
      tog.classList.toggle("menu-close");
      nav.classList.toggle("hide-nav")  
      content.classList.toggle("hide-menu")

    
    })
    


   
 
   }
}