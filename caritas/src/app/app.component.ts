import { Component, HostListener, Inject, NgModule, OnInit, Directive } from '@angular/core';
import { slideInAnimation } from "src/app/animations";
// import { HighlightDirective } from "src/app/wheel.directive";
import { RouterOutlet } from '@angular/router';
import { fromEvent } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    slideInAnimation,
    // HighlightDirective
  ]
})
export class AppComponent {
  title = 'caritas';
  ngOnInit() {

  }
  ngAfterContentInit() {

    $('.fotorama').fotorama();

    
  }
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  

}
@Directive({selector: 'button[counting]'})
class CountClicks {
  numberOfClicks = 0;

  @HostListener('wheel', ['$event.target'])
  onClick(btn) {
    console.log('button', btn, 'number of clicks:', this.numberOfClicks++);
 }
}



