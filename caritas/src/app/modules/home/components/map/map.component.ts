import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  
  mapPng = './assets/images/map/map.png';
  mapSubscr = './assets/images/map/subscribtion.png';

  constructor(private store: Store) { }

  ngOnInit() {
  }

}
