import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsInnerComponent } from './contacts-inner.component';

describe('ContactsInnerComponent', () => {
  let component: ContactsInnerComponent;
  let fixture: ComponentFixture<ContactsInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
