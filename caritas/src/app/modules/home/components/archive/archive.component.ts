import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent implements OnInit {
  bear= "./assets/images/archive/bear.png";
  archivePhoto = './assets/images/archive/archive-photo.png';

  constructor(private store: Store) { }

  ngOnInit() {
  }

}
