import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import Glide from '@glidejs/glide';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  proj1 = '/assets/images/projects/proj1.png';
  proj2 = '/assets/images/projects/proj2.png';
  proj3 = '/assets/images/projects/proj3.png';
  proj4 = '/assets/images/projects/proj4.png';
  proj5 = '/assets/images/projects/proj5.png';


 

  constructor(private store: Store) { }

  ngOnInit() {
    new Glide('.glide-projects', {
      type: 'carousel',
      startAt: 0,
      perView:3
    }).mount()
  }

}
