import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects-inner',
  templateUrl: './projects-inner.component.html',
  styleUrls: ['./projects-inner.component.scss']
})

export class ProjectsInnerComponent implements OnInit {

  proj1 = '/assets/images/projects/proj1.png';
  proj1Title = 'титул';
  proj2 = '/assets/images/projects/proj2.png';
  proj3 = '/assets/images/projects/proj3.png';
  proj4 = '/assets/images/projects/proj4.png';
  proj5 = '/assets/images/projects/proj5.png';

  constructor() { }

  ngOnInit() {
  }

}
