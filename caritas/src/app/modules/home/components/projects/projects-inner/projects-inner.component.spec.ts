import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsInnerComponent } from './projects-inner.component';

describe('ProjectsInnerComponent', () => {
  let component: ProjectsInnerComponent;
  let fixture: ComponentFixture<ProjectsInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
