import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import Glide from '@glidejs/glide';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
  
  logoWelcome = "/assets/images/index/logo-slider.png";

  slide1 = "/assets/images/index/slider1.png";
  slide2 = "/assets/images/index/slider1.png";
  slide3 = "/assets/images/index/slider1.png";
  slide4 = "/assets/images/index/slider1.png";


  constructor(private store: Store) { }

  ngOnInit() {
    

    new Glide('.glide').mount({
      type: 'carousel',
      startAt: 0,
      perView: 1,
      peek: {
        before:  0,
        after: 0
      }
    })


  }


 


}
