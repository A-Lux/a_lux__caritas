import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';


@Component({
  selector: 'app-charity-inner',
  templateUrl: './charity-inner.component.html',
  styleUrls: ['./charity-inner.component.scss']
})
export class CharityInnerComponent implements OnInit {


  family = "/assets/images/charity/charity-inner/family.png";
  


  imgags = [
    '/assets/images/charity/charity-inner/slide1.png',
    '/assets/images/charity/charity-inner/slide2.png',
    '/assets/images/charity/charity-inner/slide3.png'
  ];
  public carouselTileItems: Array<any> = [0];
  public carouselTiles = {
    0: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 3, all: 0 },
    slide: 1,
    speed: 250,
    loop: true,
    load: 1,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };


  constructor() { }

  ngOnInit() {
    
    this.carouselTileItems.forEach(el => {
      this.carouselTileLoad(el);
    });
  }
  public carouselTileLoad(j) {
    // console.log(this.carouselTiles[j]);
    const len = this.carouselTiles[j].length;
    if (len <= 30) {
      for (let i = len; i < len + 15; i++) {
        this.carouselTiles[j].push(
          this.imgags[Math.floor(Math.random() * this.imgags.length)]
        );
      }
    }
  }
}
export class NguCarouselStore {
  items: 3;

}