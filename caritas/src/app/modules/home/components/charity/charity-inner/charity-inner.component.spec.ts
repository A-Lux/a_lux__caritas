import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharityInnerComponent } from './charity-inner.component';

describe('CharityInnerComponent', () => {
  let component: CharityInnerComponent;
  let fixture: ComponentFixture<CharityInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharityInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharityInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
