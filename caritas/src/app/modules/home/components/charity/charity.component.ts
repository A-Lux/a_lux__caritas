import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-charity',
  templateUrl: './charity.component.html',
  styleUrls: ['./charity.component.scss']
})
export class CharityComponent implements OnInit {
  charityPhone = '/assets/images/charity/charity-phone.png';

  constructor(private store: Store) { }

  ngOnInit() {
  }

}
