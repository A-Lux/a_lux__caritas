import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
declare var $: any;


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  

  slide1 = './assets/images/about/about-slide1.png';
  slide2 = './assets/images/about/about-slide2.png';
  slide3 = './assets/images/about/about-slide3.png';
  slide4 = './assets/images/about/about-slide4.png';
  slide5 = './assets/images/about/about-slide5.png';
  slide6 = './assets/images/about/about-slide6.png';
  slide7 = './assets/images/about/about-slide7.png';

  constructor(private store: Store) { }

  ngOnInit() {
    
  }
  ngAfterContentInit() {
    $('.fotorama').fotorama();
  }
}
