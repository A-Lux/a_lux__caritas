import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-inner',
  templateUrl: './about-inner.component.html',
  styleUrls: ['./about-inner.component.scss']
})
export class AboutInnerComponent implements OnInit {

  aboutLeft = "./assets/images/about/about-inner/about-inner-left.png";
  aboutRight = "./assets/images/about/about-inner/about-inner-right.png";
  ballons = "./assets/images/about/about-inner/ballons.png";
  vision = "./assets/images/about/about-inner/vision.png"

  constructor() { }

  ngOnInit() {
  }

}
