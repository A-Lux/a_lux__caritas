import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  babyImg = './assets/images/news/baby.png' ;
  boyImg = './assets/images/news/boy.png' ;
  manImg = './assets/images/news/man.png' ;


  ball1 = '/assets/images/projects/ball-left.png';
  ball2 = '/assets/images/projects/ball-right-big.png';
  ball3 = '/assets/images/projects/ball-right-sm.png';
  constructor(private store: Store) { }

  ngOnInit() {
  }

}
