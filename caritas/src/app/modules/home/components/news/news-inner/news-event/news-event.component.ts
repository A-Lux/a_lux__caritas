import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-news-event',
  templateUrl: './news-event.component.html',
  styleUrls: ['./news-event.component.scss']
})
export class NewsEventComponent implements OnInit {
  header = "/assets/images/news/news-event/title.png";
  imageRight = "/assets/images/projects/project/imageRight.png";


  styles = {
    'background': `url(${this.header})`,
    'background-repeat': 'no-repeat',
    'background-size': 'cover'
  }



  imgags = [
    '/assets/images/charity/charity-inner/slide1.png',
    '/assets/images/charity/charity-inner/slide2.png',
    '/assets/images/charity/charity-inner/slide3.png'
  ];
  public carouselTileItems: Array<any> = [0];
  public carouselTiles = {
    0: []
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
    slide: 3,
    speed: 250,
    point: {
      visible: false
    },
    load: 3,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)'
  };
  constructor() { }

  ngOnInit() {
    this.carouselTileItems.forEach(el => {
      this.carouselTileLoad(el);
    });
  }

  public carouselTileLoad(j) {
    // console.log(this.carouselTiles[j]);
    const len = this.carouselTiles[j].length;
    if (len <= 30) {
      for (let i = len; i < len + 15; i++) {
        this.carouselTiles[j].push(
          this.imgags[Math.floor(Math.random() * this.imgags.length)]
        );
      }
    }
  }

}
