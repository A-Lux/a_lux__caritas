import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-inner',
  templateUrl: './news-inner.component.html',
  styleUrls: ['./news-inner.component.scss']
})
export class NewsInnerComponent implements OnInit {

  newsCard1 = "./assets/images/news/news-inner/news1.png";
  newsCard2 = "./assets/images/news/news-inner/news2.png";

  constructor() { }

  ngOnInit() {
  }

}
