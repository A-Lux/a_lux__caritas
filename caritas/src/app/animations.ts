import {
    animation, trigger, animateChild, group,
    transition, animate, style, query
} from '@angular/animations';


export const slideInAnimation = trigger('routeAnimations', [

    /**
     * HOME ANIMATION
     */

    //  1-2 block
    transition('ContactsPage => WelcomePage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),

    transition('WelcomePage => AboutPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('AboutPage => WelcomePage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),

    //  2-3 block
    transition('AboutPage => MapPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('MapPage => AboutPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),


    //  3-4 block
    transition('MapPage => ProjectsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('ProjectsPage => MapPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),


    //  4-5 block
    transition('ProjectsPage => NewsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('NewsPage => ProjectsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),


    //  5-6 block
    transition('NewsPage => CharityrPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('CharityrPage => NewsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),

    //  6-7 block
    transition('CharityrPage => ArchiverPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('ArchiverPage => CharityrPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' }))
        ]),
        query(':enter', animateChild()),
    ]),

    //  7-8 block

    transition('ArchiverPage => ContactsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),
    transition('ContactsPage => ArchiverPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '100%' })),
        ]),
        query(':enter', animateChild()),
    ]),

    /// 1-8 connection
 transition('WelcomePage =>  ContactsPage', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10
            })
        ]),
        query(':enter', [
            style({ top: '-100%' }),
            animate('400ms ease-in-out', style({ top: '0%' }))
        ]),
        query(':leave', [
            style({ top: '0%' }),
            animate('400ms ease-in-out', style({ top: '-100%' }))
        ]),

        query(':enter', animateChild()),
    ]),

    //About-inner
    transition('* => innerPageAbout', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageAbout => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),

    ]),

    //project-inner
    transition('* => innerPageProject', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageProject => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    //charity-inner
    transition('* => innerPageCharity', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageCharity => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    //contacts-inner
    transition('* => innerPageContacts', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageContacts => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    //contacts-inner
    transition('* => innerPageNews', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageNews => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ left: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ left: '0%' }))
        ]),
        query(':leave', [
            style({ left: '0%' }),
            animate('400ms ease-in-out', style({ left: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    ///////////////////////////
    //news-event
    transition('* => innerPageNews-event', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ right: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ right: '0%' }))
        ]),
        query(':leave', [
            style({ right: '0%' }),
            animate('400ms ease-in-out', style({ right: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageNews-event => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ right: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ right: '0%' }))
        ]),
        query(':leave', [
            style({ right: '0%' }),
            animate('400ms ease-in-out', style({ right: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    //news-event
    transition('* => innerPageProject-id', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ right: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ right: '0%' }))
        ]),
        query(':leave', [
            style({ right: '0%' }),
            animate('400ms ease-in-out', style({ right: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),

    transition('innerPageProject-id => *', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
            style({
                position: 'absolute',
                top: 0,
                right: 0,
                width: '100%',
                height: "100vh",
                zIndex: 10,
            })
        ]),
        query(':enter', [
            style({ right: '-100%', zIndex: 11 }),
            animate('400ms ease-in-out', style({ right: '0%' }))
        ]),
        query(':leave', [
            style({ right: '0%' }),
            animate('400ms ease-in-out', style({ right: '100%' })),
        ]),

        query(':enter', animateChild()),
    ]),


]);