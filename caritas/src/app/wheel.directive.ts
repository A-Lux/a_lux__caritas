
import { RouterModule, Routes, Router } from '@angular/router';

import {
    Directive,
    EventEmitter,
    ElementRef,
    HostListener,
    Input,
    Output,
  } from "@angular/core";
  
  import { PathLocationComponent } from "./path-location.component"

  
 


  enum WheelType {
    INTEGER = "integer",
    DECIMAL = "decimal"
  }
  
  enum WheelOperator {
    INCREASE,
    DECREASE
  }
  
  @Directive({
    selector: "[wheelOn]"
  })

  export class InputMagicWheelDirective {
    @Input() wheelOn: string = WheelType.INTEGER;
    @Input() wheelDecimalPlaces: number = 2;
    @Input() wheelUpNumber: number = 1;
    @Input() wheelDownNumber: number = 1;
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

    public operators: any = {
      
      [WheelOperator.INCREASE]: (a: boolean, b: boolean): void =>{
       
        this.handleParse(a) + this.handleParse(b);
        const arrayUp = ["/contacts","/archive","/charity","/news","/projects","/map","/about","/"];
        let arrayCurrent =  arrayUp.indexOf(location.pathname)
        const arrayStorage = arrayCurrent + 1 ;
        this.router.navigateByUrl((arrayUp[arrayStorage]));
        console.log(location.pathname)
        console.log(arrayCurrent)
        console.log(arrayStorage)
      },
      [WheelOperator.DECREASE]: (a: boolean, b: boolean): void =>{
        
        this.handleParse(a) - this.handleParse(b);
        const arrayDown = ["/","/about","/map","/projects","/news","/charity","/archive","/contacts"];
        let arrayCurrent =  arrayDown.indexOf(location.pathname)
        const arrayStorage = arrayCurrent + 1 ;
        this.router.navigateByUrl((arrayDown[arrayStorage]));
        console.log(location.pathname)
        console.log(arrayCurrent)
        console.log(arrayStorage)
      }
    };
  
    @HostListener("mousewheel", ["$event"])
    onMouseWheel(event) {
      const nativeValue: number = this.handleParse(this.el.nativeElement.value);
  
      if (event.wheelDelta > 0) {
        this.el.nativeElement.value = this.handleOperation(
          nativeValue,
          WheelOperator.INCREASE
        );
      } else {
        this.el.nativeElement.value = this.handleOperation(
          nativeValue,
          WheelOperator.DECREASE
        );
      }
      //propagate ngModel changes
      this.ngModelChange.emit(this.el.nativeElement.value);
      return false;
    }
  
    handleOperation(value: number, operator: WheelOperator): number {
      return this.handleParse(
        this.operators[operator](value, this.getRangeNumber(operator))
      );
    }
  
    getRangeNumber(operator: WheelOperator): number {
      if (operator === WheelOperator.INCREASE) {
        return this.wheelUpNumber;
      } else if (operator === WheelOperator.DECREASE) {
        return this.wheelDownNumber;
      }
    }
  
    handleParse(value: any): number {
      if (this.wheelOn === WheelType.INTEGER) {
        return parseInt(value, 10);
      } else if (this.wheelOn === WheelType.DECIMAL) {
        return +parseFloat(value).toFixed(this.wheelDecimalPlaces);
      }
    }
  
    constructor(private el: ElementRef, private router: Router) {
      //el.nativeElement.value = this.handleParse(el.nativeElement.value);
      el.nativeElement.step = this.wheelUpNumber;
      
    }
  
    ngOnInit() {
      var routerDown = 0;
      var array = window.document.querySelectorAll(" #hidden-target a");
      console.log(array[0])
     
    }
  }

  