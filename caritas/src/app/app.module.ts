import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './modules/home/home.modules';
import { SharedModule } from './shared/shared.modules';
import { WelcomeComponent } from './modules/home/components/welcome/welcome.component';
import { FormsModule } from '@angular/forms';
import { AboutComponent } from './modules/home/components/about/about.component';
import { AboutInnerComponent } from './modules/home/components/about/about-inner/about-inner.component';
import { MapComponent } from './modules/home/components/map/map.component';
import { ProjectsComponent } from './modules/home/components/projects/projects.component';
import { NewsComponent } from './modules/home/components/news/news.component';
import { CharityComponent } from './modules/home/components/charity/charity.component';
import { ArchiveComponent } from './modules/home/components/archive/archive.component';
import { ContactsComponent } from './modules/home/components/contacts/contacts.component';
import { NavigationComponent } from './shared/components/navigation/navigation.component';
import { RouterModule, Routes, Router } from '@angular/router';
import { Error404Component } from './shared/components/error404/error404.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ProjectsInnerComponent } from './modules/home/components/projects/projects-inner/projects-inner.component';
import { CharityInnerComponent } from './modules/home/components/charity/charity-inner/charity-inner.component';
import { NewsInnerComponent } from './modules/home/components/news/news-inner/news-inner.component';
import { ContactsInnerComponent } from './modules/home/components/contacts/contacts-inner/contacts-inner.component';
import { NguCarouselModule } from '@ngu/carousel';
import { ProjectComponent } from './modules/home/components/projects/projects-inner/project/project.component';
import { NewsEventComponent } from './modules/home/components/news/news-inner/news-event/news-event.component';
import { InputMagicWheelDirective } from './wheel.directive';

//NGXS 
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule, RouterStateSerializer } from '@ngxs/router-plugin';
import { Params, RouterStateSnapshot } from '@angular/router';

const appRoutes: Routes = [

  {
    path: '',
    component: WelcomeComponent,
    data: { animation: 'WelcomePage' }
  },
  {
    path: 'about',
    component: AboutComponent,
    data: { animation: 'AboutPage' }
  },
  {
    path: 'map',
    component: MapComponent,
    data: { animation: 'MapPage' }
  },
  {
    path: 'projects',
    component: ProjectsComponent,
    data: { animation: 'ProjectsPage' }
  },
  {
    path: 'news',
    component: NewsComponent,
    data: { animation: 'NewsPage' }
  },
  {
    path: 'charity',
    component: CharityComponent,
    data: { animation: 'CharityrPage' }
  },
  {
    path: 'archive',
    component: ArchiveComponent,
    data: { animation: 'ArchiverPage' }
  },
  {
    path: 'contacts',
    component: ContactsComponent,
    data: { animation: 'ContactsPage' }
  },

  //inner pages
  {
    path: 'about/about-inner',
    component: AboutInnerComponent,
    data: { animation: 'innerPageAbout' }
  },
  {
    path: 'projects/projects-inner',
    component: ProjectsInnerComponent,
    data: { animation: 'innerPageProject' }
  },
  {
    path: 'projects/projects-inner/project',
    component: ProjectComponent,
    data: { animation: 'innerPageProject-id' }
  },
  {
    path: 'charity/charity-inner',
    component: CharityInnerComponent,
    data: { animation: 'innerPageCharity' }
  },
  {
    path: 'contacts/contacts-inner',
    component: ContactsInnerComponent,
    data: { animation: 'innerPageContacts' }
  },
  {
    path: 'news/news-inner',
    component: NewsInnerComponent,
    data: { animation: 'innerPageNews' }
  },
  {
    path: 'news/news-inner/news-event',
    component: NewsEventComponent,
    data: { animation: 'innerPageNews-event' }
  },

  { path: '404', component: Error404Component, data: { animation: 'ContactsPage' } },
  { path: '**', redirectTo: "404", data: { animation: 'FilterPage' } }

];

export interface RouterStateParams {
  url: string;
  params: Params;
  queryParams: Params;
}

// Map the router snapshot to { url, params, queryParams }
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateParams> {
  serialize(routerState: RouterStateSnapshot): RouterStateParams {
    const {
      url,
      root: { queryParams }
    } = routerState;

    let { root: route } = routerState;
    while (route.firstChild) {
      route = route.firstChild;
    }

    const { params } = route;

    return { url, params, queryParams };
  }
}
@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    AboutComponent,
    MapComponent,
    ProjectsComponent,
    NewsComponent,
    CharityComponent,
    ArchiveComponent,
    ContactsComponent,
    NavigationComponent,
    Error404Component,
    AboutInnerComponent,
    ProjectsInnerComponent,
    CharityInnerComponent,
    NewsInnerComponent,
    ContactsInnerComponent,
    ProjectComponent,
    NewsEventComponent,
    InputMagicWheelDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    SharedModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    SlickCarouselModule,
    NguCarouselModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    NgxsModule.forRoot([

    ]),
    NgxsRouterPluginModule.forRoot()
  ],
  providers: [{ provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }],
  bootstrap: [AppComponent]
})

export class AppModule {

}
