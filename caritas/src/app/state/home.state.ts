import { State, Action, StateContext, Selector } from '@ngxs/store';
import { homeState } from './../modules/home/home-routing.module';
import { ScrollUp, ScrollDown} from './../action/scroll.action';


export class ScrollStateModel {
    scroll: homeState[]
}


@State<ScrollStateModel>({
    name: "scroll",
    defaults: {
        scroll: []
    }
})

export class ScrollState {
    @Selector()

    static getScroll(state: ScrollStateModel) {
        return state.scroll
    }

  

    @Action(ScrollDown)
    ScrollDown({getState, patchState}: StateContext<ScrollStateModel>, {payload}: ScrollDown) {
        const state = getState();
        patchState({
            scroll: [... state.scroll, payload]
        })
    }

    @Action(ScrollUp)
    scrollUp({getState, patchState}: StateContext<ScrollStateModel>, {payload}: ScrollUp) {
       
        patchState({
            // scroll: getState().scroll.filter( a => a.url != payload)
        })
    }
}